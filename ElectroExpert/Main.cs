﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert
{
    public partial class Main : MetroFramework.Forms.MetroForm
    {
        public Main()
        {
            InitializeComponent();
        }

       

        private void button3_Click(object sender, EventArgs e)
        {
            GlavnaForma frm = new GlavnaForma();
            frm.ShowDialog();

        }

        private void mtDashboard_Click(object sender, EventArgs e)
        {
            GlavnaForma frm = new GlavnaForma();
            frm.ShowDialog();
        }

        private void mtIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
