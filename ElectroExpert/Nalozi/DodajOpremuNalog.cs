﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Nalozi
{
    public partial class DodajOpremuNalog : Form
    {
        WebApiHelper zaduzenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOprema");
        private int idRadnika;
        List<object> destList = new List<object>();
        Form f = Application.OpenForms["DodajNalog"];

        public DodajOpremuNalog(int idRadnika)
        {
            InitializeComponent();
            dgZaduzenaOprema.AutoGenerateColumns = false;
            
            if (idRadnika != 0)
                this.idRadnika = idRadnika;
        }

        private void DodajOpremuNalog_Load(object sender, EventArgs e)
        {
            BindZaduzenaOprema();
        }

        private void BindZaduzenaOprema()
        {
            HttpResponseMessage zaduzenaresponse = zaduzenaOpremaService.GetActionResponse("GetZaduzenaOpremaByRadnik", idRadnika.ToString());
            if (zaduzenaresponse.IsSuccessStatusCode)
            {
                dgZaduzenaOprema.DataSource = zaduzenaresponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
                
            }
            
        }

     

    }
        
    
}
