﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Nalozi
{
    public partial class DodajNalog : MetroFramework.Forms.MetroForm
    {
        WebApiHelper radnikService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        WebApiHelper  nalogService = new WebApiHelper("http://localhost:63632/", "api/Nalog");
        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        private List<VoziloDTO> vozila { get; set; }
        List<RadnikDTO> dt1;
        private int n;
        List<object> destList = new List<object>();
        public DataGridView dgUgradjenaOpremaa = new DataGridView();
        public DodajNalog()
        {
            InitializeComponent();
            dgRadniciVozilo.AutoGenerateColumns = false;
            dgRadniciNalog.AutoGenerateColumns = false;
        }

        private void DodajNalog_Load(object sender, EventArgs e)
        {
            BindVozila();
        }

        private void BindVozila()
        {
            HttpResponseMessage response = voziloService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                
                vozila.Insert(0, new Vozilo());
                cbxVozila.DataSource = vozila;
                cbxVozila.DisplayMember = "Naziv";
                cbxVozila.ValueMember = "Id";

            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {


            if (cbxVozila.SelectedIndex == 0)
            {
                errorProvider1.SetError(cbxVozila, Globall.GetMessage("empty"));
                return;
                
            }
            else
            {
                errorProvider1.SetError(cbxVozila, "");

                int n = dgRadniciNalog.Rows.Add();
                dgRadniciNalog.Rows[n].Cells[0].Value = Convert.ToInt32(dgRadniciVozilo.Rows[dgRadniciVozilo.CurrentRow.Index].Cells[0].Value);
                dgRadniciNalog.Rows[n].Cells[1].Value = cbxVozila.Text;
                dgRadniciNalog.Rows[n].Cells[2].Value = Convert.ToString(dgRadniciVozilo.Rows[dgRadniciVozilo.CurrentRow.Index].Cells[1].Value);
                dgRadniciNalog.Rows[n].Cells[3].Value = Convert.ToString(dgRadniciVozilo.Rows[dgRadniciVozilo.CurrentRow.Index].Cells[2].Value);
                dgRadniciNalog.Rows[n].Cells[4].Value = Convert.ToString(dgRadniciVozilo.Rows[dgRadniciVozilo.CurrentRow.Index].Cells[3].Value);
                btnDodaj.Enabled = false;
            }

  

         

            
        }

        private void BindRadniciDva(int v)
        {
            HttpResponseMessage response = radnikService.GetActionResponse("GetRadniciAndVozilo",v.ToString());

            if (response.IsSuccessStatusCode)
            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                dgRadniciNalog.DataSource = vozila;
                dgRadniciNalog.ClearSelection();
            }
        }
    

        private void btnDodajOpremu_Click(object sender, EventArgs e)
        {
            int idRadnika = Convert.ToInt32(dgRadniciNalog.Rows[dgRadniciNalog.CurrentRow.Index].Cells[0].Value);

            DodajOpremuNalog dodajOpremuForm = new DodajOpremuNalog(idRadnika);
            dodajOpremuForm.ShowDialog();
        }





        private void btnOtvoriNalog_Click(object sender, EventArgs e)
        {

            if (this.ValidateChildren())
            {
                Nalog n = new Nalog();

                n.BrojNaloga = Convert.ToInt32(txtBrojNaloga.Text);
                n.DatumOtvorenja = DateTime.Now;
                n.Pretplatnik = txtPretplatnik.Text;
                n.KorisnikId = Globall.prijavljeniKorisnik.Id;
                n.AdresaInstalacije = txtAdresa.Text;
                n.VoziloId = Convert.ToInt32(dgRadniciNalog.Rows[dgRadniciNalog.CurrentRow.Index].Cells[0].Value);
                n.Zavrsen = false;

                HttpResponseMessage response = nalogService.PostResponse(n);


                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("nalog_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void ClearInput()
        {
            txtAdresa.Text = txtBrojNaloga.Text = txtBrojUgovora.Text = txtPretplatnik.Text="" ;            
            dgRadniciNalog.Refresh();
        }

        private void cbxVozila_SelectionChangeCommitted(object sender, EventArgs e)
        {
            BindRadnici(Convert.ToInt32(cbxVozila.SelectedValue));
        }

        private void BindRadnici(int v)
        {
            HttpResponseMessage response = radnikService.GetActionResponse("GetRadniciByVozilo", v.ToString());

            if (response.IsSuccessStatusCode)            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                dgRadniciVozilo.DataSource = vozila;
                dgRadniciVozilo.ClearSelection();
            }
        }


        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            int row = dgRadniciNalog.CurrentCell.RowIndex;
            dgRadniciNalog.Rows.RemoveAt(row);
            btnDodaj.Enabled = true;
        }








        #region Validacija
        private void txtBrojNaloga_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtBrojNaloga.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtBrojNaloga, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtBrojNaloga.Text, "^[0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtBrojNaloga, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtBrojNaloga, "");
            }
        }

        private void txtBrojUgovora_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtBrojUgovora.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtBrojUgovora, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtBrojUgovora.Text, "^[0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtBrojUgovora, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtBrojUgovora, "");
            }
        }

        private void txtPretplatnik_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPretplatnik.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtPretplatnik, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtPretplatnik.Text, "^[a-zA-Z\\s]+$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtPretplatnik, Globall.GetMessage("let_only"));
            }

            else
            {

                errorProvider1.SetError(txtPretplatnik, "");
            }
        }

        private void txtAdresa_Validating(object sender, CancelEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtAdresa.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtAdresa, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtAdresa.Text, "^[a-zA-Z0-9 _.-]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtAdresa, Globall.GetMessage("letnum_only"));
            }

            else
            {

                errorProvider1.SetError(txtAdresa, "");
            }
        }

        private void cbxVozila_Validating(object sender, CancelEventArgs e)
        {
            if (cbxVozila.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxVozila, Globall.GetMessage("empty"));
            }
            else
            {
                errorProvider1.SetError(cbxVozila, "");
            }
        }

        private void dgRadniciNalog_Validating(object sender, CancelEventArgs e)
        {
            if (dgRadniciNalog.Rows.Count == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(dgRadniciNalog, Globall.GetMessage("empty"));
            }
            else
            {
                errorProvider1.SetError(dgRadniciNalog, "");
            }
        }

        #endregion




     
    }
}



       



    
    

