﻿namespace ElectroExpert.Nalozi
{
    partial class DetaljiNaloga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblRadnik2 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblAdresaInst = new System.Windows.Forms.Label();
            this.lblPretplatnik = new System.Windows.Forms.Label();
            this.lblRadnik1 = new System.Windows.Forms.Label();
            this.lblDatumOtv = new System.Windows.Forms.Label();
            this.lblBrNaloga = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnDodajUslugu = new System.Windows.Forms.ToolStripMenuItem();
            this.btnIzbrisi = new System.Windows.Forms.ToolStripMenuItem();
            this.dgUsluge = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VrstaUslugeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjereId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VrstaUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SifraUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cijena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uslugaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.btnDodajOpremu = new System.Windows.Forms.ToolStripMenuItem();
            this.btnIzbrisiOpremu = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnZakljuci = new System.Windows.Forms.Button();
            this.rtxtOpis = new System.Windows.Forms.RichTextBox();
            this.dgUgradjenaOprema = new System.Windows.Forms.DataGridView();
            this.ArtikalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KolicinaUgradjenog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UgradjenabindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsluge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUgradjenaOprema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UgradjenabindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Broj naloga:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Datum otvaranja:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(78, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Radnici:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblRadnik2);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.lblAdresaInst);
            this.groupBox1.Controls.Add(this.lblPretplatnik);
            this.groupBox1.Controls.Add(this.lblRadnik1);
            this.groupBox1.Controls.Add(this.lblDatumOtv);
            this.groupBox1.Controls.Add(this.lblBrNaloga);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(864, 174);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalji naloga";
            // 
            // lblRadnik2
            // 
            this.lblRadnik2.AutoSize = true;
            this.lblRadnik2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRadnik2.Location = new System.Drawing.Point(176, 139);
            this.lblRadnik2.Name = "lblRadnik2";
            this.lblRadnik2.Size = new System.Drawing.Size(53, 20);
            this.lblRadnik2.TabIndex = 25;
            this.lblRadnik2.Text = "label7";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(656, 107);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(53, 20);
            this.lblStatus.TabIndex = 24;
            this.lblStatus.Text = "label7";
            // 
            // lblAdresaInst
            // 
            this.lblAdresaInst.AutoSize = true;
            this.lblAdresaInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdresaInst.Location = new System.Drawing.Point(656, 70);
            this.lblAdresaInst.Name = "lblAdresaInst";
            this.lblAdresaInst.Size = new System.Drawing.Size(53, 20);
            this.lblAdresaInst.TabIndex = 23;
            this.lblAdresaInst.Text = "label7";
            // 
            // lblPretplatnik
            // 
            this.lblPretplatnik.AutoSize = true;
            this.lblPretplatnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPretplatnik.Location = new System.Drawing.Point(656, 28);
            this.lblPretplatnik.Name = "lblPretplatnik";
            this.lblPretplatnik.Size = new System.Drawing.Size(53, 20);
            this.lblPretplatnik.TabIndex = 22;
            this.lblPretplatnik.Text = "label7";
            // 
            // lblRadnik1
            // 
            this.lblRadnik1.AutoSize = true;
            this.lblRadnik1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRadnik1.Location = new System.Drawing.Point(176, 107);
            this.lblRadnik1.Name = "lblRadnik1";
            this.lblRadnik1.Size = new System.Drawing.Size(53, 20);
            this.lblRadnik1.TabIndex = 21;
            this.lblRadnik1.Text = "label7";
            // 
            // lblDatumOtv
            // 
            this.lblDatumOtv.AutoSize = true;
            this.lblDatumOtv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatumOtv.Location = new System.Drawing.Point(176, 70);
            this.lblDatumOtv.Name = "lblDatumOtv";
            this.lblDatumOtv.Size = new System.Drawing.Size(53, 20);
            this.lblDatumOtv.TabIndex = 20;
            this.lblDatumOtv.Text = "label7";
            // 
            // lblBrNaloga
            // 
            this.lblBrNaloga.AutoSize = true;
            this.lblBrNaloga.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrNaloga.Location = new System.Drawing.Point(176, 28);
            this.lblBrNaloga.Name = "lblBrNaloga";
            this.lblBrNaloga.Size = new System.Drawing.Size(53, 20);
            this.lblBrNaloga.TabIndex = 19;
            this.lblBrNaloga.Text = "label7";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(565, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Status:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(529, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Pretplatnik:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(468, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Adresa instalacije:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.menuStrip1);
            this.groupBox2.Controls.Add(this.dgUsluge);
            this.groupBox2.Location = new System.Drawing.Point(12, 243);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(864, 239);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Usluge";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodajUslugu,
            this.btnIzbrisi});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(3, 18);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(858, 56);
            this.menuStrip1.TabIndex = 35;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnDodajUslugu
            // 
            this.btnDodajUslugu.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodajUslugu.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodajUslugu.Name = "btnDodajUslugu";
            this.btnDodajUslugu.Size = new System.Drawing.Size(110, 52);
            this.btnDodajUslugu.Text = "Dodaj";
            this.btnDodajUslugu.Click += new System.EventHandler(this.btnDodajUslugu_Click_1);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Image = global::ElectroExpert.Properties.Resources.if_edit_delete_118920;
            this.btnIzbrisi.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(108, 52);
            this.btnIzbrisi.Text = "Izbriši";
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // dgUsluge
            // 
            this.dgUsluge.AllowUserToAddRows = false;
            this.dgUsluge.AutoGenerateColumns = false;
            this.dgUsluge.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUsluge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsluge.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.VrstaUslugeId,
            this.JedMjereId,
            this.VrstaUsluge,
            this.SifraUsluge,
            this.NazivUsluge,
            this.JedMjere,
            this.Cijena});
            this.dgUsluge.DataSource = this.uslugaBindingSource;
            this.dgUsluge.Location = new System.Drawing.Point(3, 94);
            this.dgUsluge.Name = "dgUsluge";
            this.dgUsluge.ReadOnly = true;
            this.dgUsluge.RowTemplate.Height = 24;
            this.dgUsluge.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUsluge.Size = new System.Drawing.Size(858, 162);
            this.dgUsluge.TabIndex = 23;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "UslugaId";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // VrstaUslugeId
            // 
            this.VrstaUslugeId.DataPropertyName = "VrstaUslugeId";
            this.VrstaUslugeId.HeaderText = "VrstaUslugeId";
            this.VrstaUslugeId.Name = "VrstaUslugeId";
            this.VrstaUslugeId.ReadOnly = true;
            this.VrstaUslugeId.Visible = false;
            // 
            // JedMjereId
            // 
            this.JedMjereId.DataPropertyName = "JedMjereId";
            this.JedMjereId.HeaderText = "JedMjereId";
            this.JedMjereId.Name = "JedMjereId";
            this.JedMjereId.ReadOnly = true;
            this.JedMjereId.Visible = false;
            // 
            // VrstaUsluge
            // 
            this.VrstaUsluge.DataPropertyName = "VrstaUsluge";
            this.VrstaUsluge.HeaderText = "Vrsta usluge";
            this.VrstaUsluge.Name = "VrstaUsluge";
            this.VrstaUsluge.ReadOnly = true;
            // 
            // SifraUsluge
            // 
            this.SifraUsluge.DataPropertyName = "SifraUsluge";
            this.SifraUsluge.HeaderText = "Sifra";
            this.SifraUsluge.Name = "SifraUsluge";
            this.SifraUsluge.ReadOnly = true;
            // 
            // NazivUsluge
            // 
            this.NazivUsluge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NazivUsluge.DataPropertyName = "NazivUsluge";
            this.NazivUsluge.HeaderText = "Naziv";
            this.NazivUsluge.Name = "NazivUsluge";
            this.NazivUsluge.ReadOnly = true;
            this.NazivUsluge.Width = 72;
            // 
            // JedMjere
            // 
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jedinica mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            // 
            // Cijena
            // 
            this.Cijena.DataPropertyName = "Cijena";
            this.Cijena.HeaderText = "Cijena";
            this.Cijena.Name = "Cijena";
            this.Cijena.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.menuStrip2);
            this.groupBox3.Controls.Add(this.btnPrint);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnZakljuci);
            this.groupBox3.Controls.Add(this.rtxtOpis);
            this.groupBox3.Controls.Add(this.dgUgradjenaOprema);
            this.groupBox3.Location = new System.Drawing.Point(15, 505);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(864, 403);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ugrađena oprema po nalogu";
            // 
            // menuStrip2
            // 
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodajOpremu,
            this.btnIzbrisiOpremu});
            this.menuStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip2.Location = new System.Drawing.Point(3, 18);
            this.menuStrip2.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(858, 56);
            this.menuStrip2.TabIndex = 36;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // btnDodajOpremu
            // 
            this.btnDodajOpremu.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodajOpremu.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodajOpremu.Name = "btnDodajOpremu";
            this.btnDodajOpremu.Size = new System.Drawing.Size(110, 52);
            this.btnDodajOpremu.Text = "Dodaj";
            this.btnDodajOpremu.Click += new System.EventHandler(this.btnDodajOpremu_Click_1);
            // 
            // btnIzbrisiOpremu
            // 
            this.btnIzbrisiOpremu.Image = global::ElectroExpert.Properties.Resources.if_edit_delete_118920;
            this.btnIzbrisiOpremu.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnIzbrisiOpremu.Name = "btnIzbrisiOpremu";
            this.btnIzbrisiOpremu.Size = new System.Drawing.Size(108, 52);
            this.btnIzbrisiOpremu.Text = "Izbriši";
            this.btnIzbrisiOpremu.Click += new System.EventHandler(this.btnIzbrisiOpremu_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrint.Enabled = false;
            this.btnPrint.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPrint.Location = new System.Drawing.Point(570, 331);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(136, 45);
            this.btnPrint.TabIndex = 29;
            this.btnPrint.Text = "Štampaj";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 255);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 26;
            this.label7.Text = "Napomena:";
            // 
            // btnZakljuci
            // 
            this.btnZakljuci.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnZakljuci.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnZakljuci.Location = new System.Drawing.Point(725, 331);
            this.btnZakljuci.Name = "btnZakljuci";
            this.btnZakljuci.Size = new System.Drawing.Size(136, 45);
            this.btnZakljuci.TabIndex = 19;
            this.btnZakljuci.Text = "Zaključi";
            this.btnZakljuci.UseVisualStyleBackColor = false;
            this.btnZakljuci.Click += new System.EventHandler(this.btnZakljuci_Click);
            // 
            // rtxtOpis
            // 
            this.rtxtOpis.BackColor = System.Drawing.SystemColors.HighlightText;
            this.rtxtOpis.Location = new System.Drawing.Point(6, 278);
            this.rtxtOpis.Name = "rtxtOpis";
            this.rtxtOpis.Size = new System.Drawing.Size(482, 98);
            this.rtxtOpis.TabIndex = 20;
            this.rtxtOpis.Text = "";
            // 
            // dgUgradjenaOprema
            // 
            this.dgUgradjenaOprema.AllowUserToAddRows = false;
            this.dgUgradjenaOprema.AutoGenerateColumns = false;
            this.dgUgradjenaOprema.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUgradjenaOprema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUgradjenaOprema.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ArtikalId,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn5,
            this.KolicinaUgradjenog});
            this.dgUgradjenaOprema.DataSource = this.UgradjenabindingSource;
            this.dgUgradjenaOprema.Location = new System.Drawing.Point(6, 97);
            this.dgUgradjenaOprema.Name = "dgUgradjenaOprema";
            this.dgUgradjenaOprema.ReadOnly = true;
            this.dgUgradjenaOprema.RowTemplate.Height = 24;
            this.dgUgradjenaOprema.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUgradjenaOprema.Size = new System.Drawing.Size(852, 142);
            this.dgUgradjenaOprema.TabIndex = 28;
            // 
            // ArtikalId
            // 
            this.ArtikalId.DataPropertyName = "ArtikalId";
            this.ArtikalId.HeaderText = "ArtikalId";
            this.ArtikalId.Name = "ArtikalId";
            this.ArtikalId.ReadOnly = true;
            this.ArtikalId.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SifraArtikla";
            this.dataGridViewTextBoxColumn2.HeaderText = "Šifra artikla";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "NazivArtikla";
            this.dataGridViewTextBoxColumn3.HeaderText = "Naziv artikla";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "JedinicaMjere";
            this.dataGridViewTextBoxColumn5.HeaderText = "Jedinica mjere";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // KolicinaUgradjenog
            // 
            this.KolicinaUgradjenog.DataPropertyName = "KolicinaUgradjenog";
            this.KolicinaUgradjenog.HeaderText = "Količina ugrađenog";
            this.KolicinaUgradjenog.Name = "KolicinaUgradjenog";
            this.KolicinaUgradjenog.ReadOnly = true;
            // 
            // DetaljiNaloga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 928);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "DetaljiNaloga";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Detalji naloga";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.DetaljiNaloga_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsluge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUgradjenaOprema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UgradjenabindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRadnik2;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblAdresaInst;
        private System.Windows.Forms.Label lblPretplatnik;
        private System.Windows.Forms.Label lblRadnik1;
        private System.Windows.Forms.Label lblDatumOtv;
        private System.Windows.Forms.Label lblBrNaloga;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnZakljuci;
        private System.Windows.Forms.DataGridView dgUgradjenaOprema;
        public System.Windows.Forms.DataGridView dgUsluge;
        private System.Windows.Forms.BindingSource uslugaBindingSource;
        private System.Windows.Forms.BindingSource UgradjenabindingSource;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox rtxtOpis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn VrstaUslugeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjereId;
        private System.Windows.Forms.DataGridViewTextBoxColumn VrstaUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn SifraUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cijena;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnDodajUslugu;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem btnDodajOpremu;
        private System.Windows.Forms.ToolStripMenuItem btnIzbrisi;
        private System.Windows.Forms.ToolStripMenuItem btnIzbrisiOpremu;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArtikalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn KolicinaUgradjenog;
    }
}