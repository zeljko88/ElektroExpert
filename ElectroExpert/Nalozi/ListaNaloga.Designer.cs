﻿namespace ElectroExpert.Nalozi
{
    partial class ListaNaloga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaNaloga));
            this.txtSifra = new System.Windows.Forms.TextBox();
            this.dgListaNaloga = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrojNaloga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vozilo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumOtvaranja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pretplatnik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuAddEditDelete = new System.Windows.Forms.MenuStrip();
            this.btnDodaj = new System.Windows.Forms.ToolStripMenuItem();
            this.urediToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izbirišiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaNaloga)).BeginInit();
            this.menuAddEditDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSifra
            // 
            this.txtSifra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSifra.Location = new System.Drawing.Point(16, 164);
            this.txtSifra.Name = "txtSifra";
            this.txtSifra.Size = new System.Drawing.Size(167, 30);
            this.txtSifra.TabIndex = 10;
            this.txtSifra.TextChanged += new System.EventHandler(this.txtSifra_TextChanged);
            // 
            // dgListaNaloga
            // 
            this.dgListaNaloga.AllowUserToAddRows = false;
            this.dgListaNaloga.AllowUserToDeleteRows = false;
            this.dgListaNaloga.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgListaNaloga.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgListaNaloga.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.BrojNaloga,
            this.Vozilo,
            this.DatumOtvaranja,
            this.Pretplatnik,
            this.Status});
            this.dgListaNaloga.Location = new System.Drawing.Point(16, 225);
            this.dgListaNaloga.Name = "dgListaNaloga";
            this.dgListaNaloga.ReadOnly = true;
            this.dgListaNaloga.RowTemplate.Height = 24;
            this.dgListaNaloga.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgListaNaloga.Size = new System.Drawing.Size(1020, 430);
            this.dgListaNaloga.TabIndex = 12;
            this.dgListaNaloga.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgListaNaloga_CellContentDoubleClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // BrojNaloga
            // 
            this.BrojNaloga.DataPropertyName = "BrojNaloga";
            this.BrojNaloga.HeaderText = "Broj naloga";
            this.BrojNaloga.Name = "BrojNaloga";
            this.BrojNaloga.ReadOnly = true;
            // 
            // Vozilo
            // 
            this.Vozilo.DataPropertyName = "Vozilo";
            this.Vozilo.HeaderText = "Vozilo";
            this.Vozilo.Name = "Vozilo";
            this.Vozilo.ReadOnly = true;
            // 
            // DatumOtvaranja
            // 
            this.DatumOtvaranja.DataPropertyName = "DatumOtvaranja";
            this.DatumOtvaranja.HeaderText = "Datum Otvaranja";
            this.DatumOtvaranja.Name = "DatumOtvaranja";
            this.DatumOtvaranja.ReadOnly = true;
            // 
            // Pretplatnik
            // 
            this.Pretplatnik.DataPropertyName = "Pretplatnik";
            this.Pretplatnik.HeaderText = "Pretplatnik";
            this.Pretplatnik.Name = "Pretplatnik";
            this.Pretplatnik.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Broj naloga:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "(dvostruki klik na nalog za detalje)";
            // 
            // menuAddEditDelete
            // 
            this.menuAddEditDelete.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuAddEditDelete.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodaj,
            this.urediToolStripMenuItem,
            this.izbirišiToolStripMenuItem});
            this.menuAddEditDelete.Location = new System.Drawing.Point(20, 60);
            this.menuAddEditDelete.Name = "menuAddEditDelete";
            this.menuAddEditDelete.Size = new System.Drawing.Size(1005, 56);
            this.menuAddEditDelete.TabIndex = 17;
            this.menuAddEditDelete.Text = "menuStrip1";
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDodaj.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(110, 52);
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click_1);
            // 
            // urediToolStripMenuItem
            // 
            this.urediToolStripMenuItem.Image = global::ElectroExpert.Properties.Resources.if_edit_find_replace_118921__1_;
            this.urediToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.urediToolStripMenuItem.Name = "urediToolStripMenuItem";
            this.urediToolStripMenuItem.Size = new System.Drawing.Size(105, 52);
            this.urediToolStripMenuItem.Text = "Uredi";
            // 
            // izbirišiToolStripMenuItem
            // 
            this.izbirišiToolStripMenuItem.Image = global::ElectroExpert.Properties.Resources.if_edit_delete_118920;
            this.izbirišiToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.izbirišiToolStripMenuItem.Name = "izbirišiToolStripMenuItem";
            this.izbirišiToolStripMenuItem.Size = new System.Drawing.Size(108, 52);
            this.izbirišiToolStripMenuItem.Text = "Izbriši";
            this.izbirišiToolStripMenuItem.Click += new System.EventHandler(this.izbirišiToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = global::ElectroExpert.Properties.Resources.if_system_search_118797;
            this.pictureBox1.Location = new System.Drawing.Point(189, 164);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 46);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ListaNaloga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 664);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgListaNaloga);
            this.Controls.Add(this.txtSifra);
            this.Controls.Add(this.menuAddEditDelete);
            this.MainMenuStrip = this.menuAddEditDelete;
            this.MaximizeBox = false;
            this.Name = "ListaNaloga";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lista naloga";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ListaNaloga_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaNaloga)).EndInit();
            this.menuAddEditDelete.ResumeLayout(false);
            this.menuAddEditDelete.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSifra;
        private System.Windows.Forms.DataGridView dgListaNaloga;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuAddEditDelete;
        private System.Windows.Forms.ToolStripMenuItem btnDodaj;
        private System.Windows.Forms.ToolStripMenuItem urediToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izbirišiToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojNaloga;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vozilo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumOtvaranja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pretplatnik;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}