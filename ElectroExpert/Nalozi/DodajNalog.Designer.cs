﻿namespace ElectroExpert.Nalozi
{
    partial class DodajNalog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBrojUgovora = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtPretplatnik = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBrojNaloga = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgRadniciVozilo = new System.Windows.Forms.DataGridView();
            this.VoziloId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SefVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrTelefona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsSef = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbxVozila = new System.Windows.Forms.ComboBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnIzbrisi = new System.Windows.Forms.ToolStripMenuItem();
            this.dgRadniciNalog = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOtvoriNalog = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRadniciVozilo)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRadniciNalog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtBrojUgovora);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAdresa);
            this.groupBox1.Controls.Add(this.txtPretplatnik);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtBrojNaloga);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(796, 167);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nalog info";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Broj ugovora:";
            // 
            // txtBrojUgovora
            // 
            this.txtBrojUgovora.Location = new System.Drawing.Point(13, 123);
            this.txtBrojUgovora.Name = "txtBrojUgovora";
            this.txtBrojUgovora.Size = new System.Drawing.Size(162, 30);
            this.txtBrojUgovora.TabIndex = 9;
            this.txtBrojUgovora.Validating += new System.ComponentModel.CancelEventHandler(this.txtBrojUgovora_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(418, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(166, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Adresa instalacije:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(418, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Pretplatnik:";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(422, 123);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(320, 30);
            this.txtAdresa.TabIndex = 6;
            this.txtAdresa.Validating += new System.ComponentModel.CancelEventHandler(this.txtAdresa_Validating);
            // 
            // txtPretplatnik
            // 
            this.txtPretplatnik.Location = new System.Drawing.Point(422, 60);
            this.txtPretplatnik.Name = "txtPretplatnik";
            this.txtPretplatnik.Size = new System.Drawing.Size(320, 30);
            this.txtPretplatnik.TabIndex = 5;
            this.txtPretplatnik.Validating += new System.ComponentModel.CancelEventHandler(this.txtPretplatnik_Validating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Broj naloga:";
            // 
            // txtBrojNaloga
            // 
            this.txtBrojNaloga.Location = new System.Drawing.Point(13, 60);
            this.txtBrojNaloga.Name = "txtBrojNaloga";
            this.txtBrojNaloga.Size = new System.Drawing.Size(230, 30);
            this.txtBrojNaloga.TabIndex = 0;
            this.txtBrojNaloga.Validating += new System.ComponentModel.CancelEventHandler(this.txtBrojNaloga_Validating);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgRadniciVozilo);
            this.groupBox2.Controls.Add(this.cbxVozila);
            this.groupBox2.Controls.Add(this.btnDodaj);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(797, 233);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Odabir radnika";
            // 
            // dgRadniciVozilo
            // 
            this.dgRadniciVozilo.AllowUserToAddRows = false;
            this.dgRadniciVozilo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgRadniciVozilo.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgRadniciVozilo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRadniciVozilo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoziloId,
            this.SefVozila,
            this.BrTelefona,
            this.Email,
            this.IsSef});
            this.dgRadniciVozilo.Location = new System.Drawing.Point(6, 104);
            this.dgRadniciVozilo.Name = "dgRadniciVozilo";
            this.dgRadniciVozilo.ReadOnly = true;
            this.dgRadniciVozilo.RowTemplate.Height = 24;
            this.dgRadniciVozilo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgRadniciVozilo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRadniciVozilo.Size = new System.Drawing.Size(756, 117);
            this.dgRadniciVozilo.TabIndex = 14;
            // 
            // VoziloId
            // 
            this.VoziloId.DataPropertyName = "VoziloId";
            this.VoziloId.HeaderText = "Id";
            this.VoziloId.Name = "VoziloId";
            this.VoziloId.ReadOnly = true;
            this.VoziloId.Visible = false;
            // 
            // SefVozila
            // 
            this.SefVozila.DataPropertyName = "SefVozila";
            this.SefVozila.FillWeight = 111.9289F;
            this.SefVozila.HeaderText = "Ime i prezime";
            this.SefVozila.Name = "SefVozila";
            this.SefVozila.ReadOnly = true;
            // 
            // BrTelefona
            // 
            this.BrTelefona.DataPropertyName = "BrTelefona";
            this.BrTelefona.FillWeight = 111.9289F;
            this.BrTelefona.HeaderText = "Telefon";
            this.BrTelefona.Name = "BrTelefona";
            this.BrTelefona.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // IsSef
            // 
            this.IsSef.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IsSef.DataPropertyName = "IsSef";
            this.IsSef.FillWeight = 76.14214F;
            this.IsSef.HeaderText = "Šef";
            this.IsSef.Name = "IsSef";
            this.IsSef.ReadOnly = true;
            this.IsSef.Width = 50;
            // 
            // cbxVozila
            // 
            this.cbxVozila.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxVozila.Location = new System.Drawing.Point(82, 49);
            this.cbxVozila.Name = "cbxVozila";
            this.cbxVozila.Size = new System.Drawing.Size(216, 33);
            this.cbxVozila.TabIndex = 13;
            this.cbxVozila.SelectionChangeCommitted += new System.EventHandler(this.cbxVozila_SelectionChangeCommitted);
            this.cbxVozila.Validating += new System.ComponentModel.CancelEventHandler(this.cbxVozila_Validating);
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDodaj.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDodaj.Location = new System.Drawing.Point(626, 39);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(136, 40);
            this.btnDodaj.TabIndex = 3;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = false;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Vozilo:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.menuStrip1);
            this.groupBox4.Controls.Add(this.dgRadniciNalog);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(19, 521);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(790, 202);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Radnici";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnIzbrisi});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(3, 23);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 56);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Image = global::ElectroExpert.Properties.Resources.if_edit_delete_118920;
            this.btnIzbrisi.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(112, 52);
            this.btnIzbrisi.Text = "Izbiriši";
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // dgRadniciNalog
            // 
            this.dgRadniciNalog.AllowUserToAddRows = false;
            this.dgRadniciNalog.AllowUserToResizeColumns = false;
            this.dgRadniciNalog.AllowUserToResizeRows = false;
            this.dgRadniciNalog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgRadniciNalog.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgRadniciNalog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRadniciNalog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.NazivVozila,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgRadniciNalog.Location = new System.Drawing.Point(6, 99);
            this.dgRadniciNalog.Name = "dgRadniciNalog";
            this.dgRadniciNalog.ReadOnly = true;
            this.dgRadniciNalog.RowTemplate.Height = 24;
            this.dgRadniciNalog.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgRadniciNalog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRadniciNalog.Size = new System.Drawing.Size(749, 101);
            this.dgRadniciNalog.TabIndex = 15;
            this.dgRadniciNalog.Validating += new System.ComponentModel.CancelEventHandler(this.dgRadniciNalog_Validating);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VoziloId";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // NazivVozila
            // 
            this.NazivVozila.DataPropertyName = "NazivVozila";
            this.NazivVozila.HeaderText = "Vozilo";
            this.NazivVozila.Name = "NazivVozila";
            this.NazivVozila.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SefVozila";
            this.dataGridViewTextBoxColumn2.FillWeight = 111.9289F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Šef vozila";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BrTelefona";
            this.dataGridViewTextBoxColumn3.FillWeight = 111.9289F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Telefon";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Email";
            this.dataGridViewTextBoxColumn4.HeaderText = "Email";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // btnOtvoriNalog
            // 
            this.btnOtvoriNalog.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnOtvoriNalog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOtvoriNalog.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnOtvoriNalog.Location = new System.Drawing.Point(661, 753);
            this.btnOtvoriNalog.Name = "btnOtvoriNalog";
            this.btnOtvoriNalog.Size = new System.Drawing.Size(136, 49);
            this.btnOtvoriNalog.TabIndex = 7;
            this.btnOtvoriNalog.Text = "Otvori nalog";
            this.btnOtvoriNalog.UseVisualStyleBackColor = false;
            this.btnOtvoriNalog.Click += new System.EventHandler(this.btnOtvoriNalog_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // DodajNalog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(820, 814);
            this.Controls.Add(this.btnOtvoriNalog);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.MaximizeBox = false;
            this.Name = "DodajNalog";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Novi nalog";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.DodajNalog_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRadniciVozilo)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRadniciNalog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBrojNaloga;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.TextBox txtPretplatnik;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBrojUgovora;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnOtvoriNalog;
        private System.Windows.Forms.ComboBox cbxVozila;
        private System.Windows.Forms.DataGridView dgRadniciVozilo;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoziloId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SefVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrTelefona;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsSef;
        private System.Windows.Forms.DataGridView dgRadniciNalog;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnIzbrisi;
    }
}