﻿namespace ElectroExpert.Nalozi
{
    partial class DetaljiZavrsenogNaloga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgUgradjenaOprema = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedinicaMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDatumZatvaranja = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblRadnik2 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblAdresaInst = new System.Windows.Forms.Label();
            this.lblPretplatnik = new System.Windows.Forms.Label();
            this.lblRadnik1 = new System.Windows.Forms.Label();
            this.lblDatumOtv = new System.Windows.Forms.Label();
            this.lblBrNaloga = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgOdradjeneUsluge = new System.Windows.Forms.DataGridView();
            this.SifraUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VrstaUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cijena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rtxtNapomena = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUgradjenaOprema)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOdradjeneUsluge)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgUgradjenaOprema);
            this.groupBox3.Location = new System.Drawing.Point(29, 561);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(857, 220);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ugrađena oprema po nalogu";
            // 
            // dgUgradjenaOprema
            // 
            this.dgUgradjenaOprema.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUgradjenaOprema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUgradjenaOprema.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.JedinicaMjere,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgUgradjenaOprema.Location = new System.Drawing.Point(13, 27);
            this.dgUgradjenaOprema.Name = "dgUgradjenaOprema";
            this.dgUgradjenaOprema.ReadOnly = true;
            this.dgUgradjenaOprema.RowTemplate.Height = 24;
            this.dgUgradjenaOprema.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUgradjenaOprema.Size = new System.Drawing.Size(838, 187);
            this.dgUgradjenaOprema.TabIndex = 28;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SifraArtikla";
            this.dataGridViewTextBoxColumn2.HeaderText = "Šifra artikla";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "NazivArtikla";
            this.dataGridViewTextBoxColumn3.HeaderText = "Naziv artikla";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // JedinicaMjere
            // 
            this.JedinicaMjere.DataPropertyName = "JedinicaMjere";
            this.JedinicaMjere.HeaderText = "Jed. mjere";
            this.JedinicaMjere.Name = "JedinicaMjere";
            this.JedinicaMjere.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "KolicinaUgradjenog";
            this.dataGridViewTextBoxColumn4.HeaderText = "Količina ugrađenog";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "DatumUgradnje";
            this.dataGridViewTextBoxColumn5.HeaderText = "Datum ugradnje";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblDatumZatvaranja);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblRadnik2);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.lblAdresaInst);
            this.groupBox1.Controls.Add(this.lblPretplatnik);
            this.groupBox1.Controls.Add(this.lblRadnik1);
            this.groupBox1.Controls.Add(this.lblDatumOtv);
            this.groupBox1.Controls.Add(this.lblBrNaloga);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(29, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(851, 203);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalji naloga";
            // 
            // lblDatumZatvaranja
            // 
            this.lblDatumZatvaranja.AutoSize = true;
            this.lblDatumZatvaranja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatumZatvaranja.Location = new System.Drawing.Point(634, 83);
            this.lblDatumZatvaranja.Name = "lblDatumZatvaranja";
            this.lblDatumZatvaranja.Size = new System.Drawing.Size(53, 20);
            this.lblDatumZatvaranja.TabIndex = 27;
            this.lblDatumZatvaranja.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(464, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "Datum završetka:";
            // 
            // lblRadnik2
            // 
            this.lblRadnik2.AutoSize = true;
            this.lblRadnik2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRadnik2.Location = new System.Drawing.Point(634, 153);
            this.lblRadnik2.Name = "lblRadnik2";
            this.lblRadnik2.Size = new System.Drawing.Size(53, 20);
            this.lblRadnik2.TabIndex = 25;
            this.lblRadnik2.Text = "label7";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(197, 153);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(53, 20);
            this.lblStatus.TabIndex = 24;
            this.lblStatus.Text = "label7";
            // 
            // lblAdresaInst
            // 
            this.lblAdresaInst.AutoSize = true;
            this.lblAdresaInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdresaInst.Location = new System.Drawing.Point(197, 116);
            this.lblAdresaInst.Name = "lblAdresaInst";
            this.lblAdresaInst.Size = new System.Drawing.Size(53, 20);
            this.lblAdresaInst.TabIndex = 23;
            this.lblAdresaInst.Text = "label7";
            // 
            // lblPretplatnik
            // 
            this.lblPretplatnik.AutoSize = true;
            this.lblPretplatnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPretplatnik.Location = new System.Drawing.Point(197, 46);
            this.lblPretplatnik.Name = "lblPretplatnik";
            this.lblPretplatnik.Size = new System.Drawing.Size(53, 20);
            this.lblPretplatnik.TabIndex = 22;
            this.lblPretplatnik.Text = "label7";
            // 
            // lblRadnik1
            // 
            this.lblRadnik1.AutoSize = true;
            this.lblRadnik1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRadnik1.Location = new System.Drawing.Point(634, 116);
            this.lblRadnik1.Name = "lblRadnik1";
            this.lblRadnik1.Size = new System.Drawing.Size(53, 20);
            this.lblRadnik1.TabIndex = 21;
            this.lblRadnik1.Text = "label7";
            // 
            // lblDatumOtv
            // 
            this.lblDatumOtv.AutoSize = true;
            this.lblDatumOtv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatumOtv.Location = new System.Drawing.Point(634, 46);
            this.lblDatumOtv.Name = "lblDatumOtv";
            this.lblDatumOtv.Size = new System.Drawing.Size(53, 20);
            this.lblDatumOtv.TabIndex = 20;
            this.lblDatumOtv.Text = "label7";
            // 
            // lblBrNaloga
            // 
            this.lblBrNaloga.AutoSize = true;
            this.lblBrNaloga.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrNaloga.Location = new System.Drawing.Point(197, 83);
            this.lblBrNaloga.Name = "lblBrNaloga";
            this.lblBrNaloga.Size = new System.Drawing.Size(53, 20);
            this.lblBrNaloga.TabIndex = 19;
            this.lblBrNaloga.Text = "label7";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(106, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Status:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(70, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Pretplatnik:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Adresa instalacije:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(539, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Radnici:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(64, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Broj naloga:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(464, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Datum otvaranja:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgOdradjeneUsluge);
            this.groupBox2.Location = new System.Drawing.Point(29, 370);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(857, 185);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Odrađene usluge po nalogu";
            // 
            // dgOdradjeneUsluge
            // 
            this.dgOdradjeneUsluge.AllowUserToAddRows = false;
            this.dgOdradjeneUsluge.AllowUserToDeleteRows = false;
            this.dgOdradjeneUsluge.AllowUserToResizeColumns = false;
            this.dgOdradjeneUsluge.AllowUserToResizeRows = false;
            this.dgOdradjeneUsluge.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgOdradjeneUsluge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOdradjeneUsluge.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SifraUsluge,
            this.VrstaUsluge,
            this.NazivUsluge,
            this.JedMjere,
            this.Kolicina,
            this.Cijena});
            this.dgOdradjeneUsluge.Location = new System.Drawing.Point(13, 22);
            this.dgOdradjeneUsluge.MultiSelect = false;
            this.dgOdradjeneUsluge.Name = "dgOdradjeneUsluge";
            this.dgOdradjeneUsluge.ReadOnly = true;
            this.dgOdradjeneUsluge.RowTemplate.Height = 24;
            this.dgOdradjeneUsluge.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOdradjeneUsluge.Size = new System.Drawing.Size(838, 157);
            this.dgOdradjeneUsluge.TabIndex = 0;
            // 
            // SifraUsluge
            // 
            this.SifraUsluge.DataPropertyName = "SifraUsluge";
            this.SifraUsluge.HeaderText = "Šifra";
            this.SifraUsluge.Name = "SifraUsluge";
            this.SifraUsluge.ReadOnly = true;
            // 
            // VrstaUsluge
            // 
            this.VrstaUsluge.DataPropertyName = "VrstaUsluge";
            this.VrstaUsluge.HeaderText = "Vrsta";
            this.VrstaUsluge.Name = "VrstaUsluge";
            this.VrstaUsluge.ReadOnly = true;
            // 
            // NazivUsluge
            // 
            this.NazivUsluge.DataPropertyName = "NazivUsluge";
            this.NazivUsluge.HeaderText = "Naziv";
            this.NazivUsluge.Name = "NazivUsluge";
            this.NazivUsluge.ReadOnly = true;
            // 
            // JedMjere
            // 
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jed. mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            // 
            // Kolicina
            // 
            this.Kolicina.DataPropertyName = "Kolicina";
            this.Kolicina.HeaderText = "Kolicina";
            this.Kolicina.Name = "Kolicina";
            this.Kolicina.ReadOnly = true;
            // 
            // Cijena
            // 
            this.Cijena.DataPropertyName = "Cijena";
            this.Cijena.HeaderText = "Cijena";
            this.Cijena.Name = "Cijena";
            this.Cijena.ReadOnly = true;
            // 
            // rtxtNapomena
            // 
            this.rtxtNapomena.Location = new System.Drawing.Point(36, 294);
            this.rtxtNapomena.Name = "rtxtNapomena";
            this.rtxtNapomena.ReadOnly = true;
            this.rtxtNapomena.Size = new System.Drawing.Size(844, 70);
            this.rtxtNapomena.TabIndex = 24;
            this.rtxtNapomena.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(32, 269);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 28;
            this.label7.Text = "Napomena:";
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrint.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPrint.Location = new System.Drawing.Point(750, 787);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(136, 45);
            this.btnPrint.TabIndex = 30;
            this.btnPrint.Text = "Štampaj";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // DetaljiZavrsenogNaloga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 851);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rtxtNapomena);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "DetaljiZavrsenogNaloga";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Detalji završenog naloga";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.DetaljiZavrsenogNaloga_Load);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUgradjenaOprema)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgOdradjeneUsluge)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgUgradjenaOprema;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDatumZatvaranja;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblRadnik2;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblAdresaInst;
        private System.Windows.Forms.Label lblPretplatnik;
        private System.Windows.Forms.Label lblRadnik1;
        private System.Windows.Forms.Label lblDatumOtv;
        private System.Windows.Forms.Label lblBrNaloga;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgOdradjeneUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn SifraUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn VrstaUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kolicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cijena;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedinicaMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.RichTextBox rtxtNapomena;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPrint;
    }
}