﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Nalozi
{
    public partial class ListaNaloga : MetroFramework.Forms.MetroForm
    {

        WebApiHelper nalogService = new WebApiHelper("http://localhost:63632/", "api/Nalog");
        private List<NalogDTO> listaNaloga { get; set; }
        public ListaNaloga()
        {
            InitializeComponent();
            dgListaNaloga.AutoGenerateColumns = false;
        }


        private void ListaNaloga_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            HttpResponseMessage response = nalogService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                listaNaloga = response.Content.ReadAsAsync<List<NalogDTO>>().Result;

                dgListaNaloga.DataSource = listaNaloga; 

            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajNalog frm = new DodajNalog();
            frm.FormClosed += frm_FormClosed;
            frm.ShowDialog();
        }

        void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            BindGrid();
        }

        private void dgListaNaloga_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = dgListaNaloga.CurrentCell.RowIndex;

            string status = dgListaNaloga.Rows[rowindex].Cells[5].Value.ToString();

            if (status=="Otvoren")
            {
                DetaljiNaloga frm = new DetaljiNaloga(listaNaloga[e.RowIndex]);
                frm.FormClosed += frm_FormClosed;
                frm.ShowDialog();
            }
            else
            {
                DetaljiZavrsenogNaloga frm = new DetaljiZavrsenogNaloga(listaNaloga[e.RowIndex]);
                frm.FormClosed += frm_FormClosed;
                frm.ShowDialog();

            }


        }

        private void btnDodaj_Click_1(object sender, EventArgs e)
        {
            DodajNalog frm = new DodajNalog();
            frm.FormClosed += frm_FormClosed;
            frm.ShowDialog();
        }

        private void txtSifra_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtSifra, "");
            }



            TextBox ak = sender as TextBox;

            string ar = ak.Text as string;

            if (ak.Text == "")
            {
                HttpResponseMessage responseNalog = nalogService.GetResponse();
                if (responseNalog.IsSuccessStatusCode)
                {
                    dgListaNaloga.DataSource = responseNalog.Content.ReadAsAsync<List<NalogDTO>>().Result;

                   

                }

            }

            List<NalogDTO> nalozi = new List<NalogDTO>();
            List<NalogDTO> naloziLista = new List<NalogDTO>();

            HttpResponseMessage response = nalogService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                nalozi = response.Content.ReadAsAsync<List<NalogDTO>>().Result;

            }

            foreach (NalogDTO item in nalozi)
            {
                if (Regex.IsMatch(item.BrojNaloga.ToString(), ar, RegexOptions.IgnoreCase))
                {
                    naloziLista.Add(item);
                }


            }

            dgListaNaloga.DataSource = naloziLista;
        }

        private void izbirišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            if(Convert.ToString(dgListaNaloga.Rows[dgListaNaloga.CurrentRow.Index].Cells[5].Value) =="Završen")
            {
                MessageBox.Show("Ne može te izbrisati završene naloge !", "Info !", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return;
            }
            HttpResponseMessage response = nalogService.GetActionResponse("DeleteNalog",Convert.ToString(dgListaNaloga.Rows[dgListaNaloga.CurrentRow.Index].Cells[0].Value));
                {
                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show("Uspješno ste izbrisali nalog !", "Info !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BindGrid();
                }
            }

        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {

        }
    }
    }

