﻿using ElectroExpert.Izvjestaji;
using ElectroExpert.Usluge;
using ElectroExpert.ZaduzenjeRobe;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Nalozi
{
    public partial class DetaljiNaloga : MetroFramework.Forms.MetroForm
    {
        WebApiHelper zaduzenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOprema");
        WebApiHelper zaduzenaStavkeService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOpremaStavke");
        WebApiHelper ugradjenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/UgradjenaOprema");
        WebApiHelper nalogService = new WebApiHelper("http://localhost:63632/", "api/Nalog");
        WebApiHelper radnikService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        private NalogDTO nalog { get; set; }
        public List<UgradjenaOpremaDTO> vecUgradjena { get; set; }

        public DetaljiNaloga(NalogDTO nalog)
        {
            InitializeComponent();
            dgUsluge.AutoGenerateColumns = false;
            dgUgradjenaOprema.AutoGenerateColumns = false;



            if (nalog != null)
                this.nalog = nalog;
        }

        private void DetaljiNaloga_Load(object sender, EventArgs e)
        {
            BindForm();
        }

        private void BindForm()
        {
            lblBrNaloga.Text = nalog.BrojNaloga.ToString();
            lblDatumOtv.Text = nalog.DatumOtvaranja.ToString();
            lblPretplatnik.Text = nalog.Pretplatnik;
            lblAdresaInst.Text = nalog.AdresaInstalacije;
            lblStatus.Text = nalog.Status;

           

            HttpResponseMessage radnikResponse = radnikService.GetActionResponse("GetRadniciByVozilo", nalog.VoziloId.ToString());
            if (radnikResponse.IsSuccessStatusCode)
            {
                List<VoziloDTO> vozila = radnikResponse.Content.ReadAsAsync<List<VoziloDTO>>().Result;

                int i = vozila.Count();

                if (i == 1)
                {
                    lblRadnik1.Text = vozila[0].SefVozila;
                    lblRadnik2.Visible = false;
                }
                else
                {

                    lblRadnik1.Text = vozila[0].SefVozila;
                    lblRadnik2.Text = vozila[1].SefVozila;
                }



            

            }

        }

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    //if (txtKolicina.Text == "")
        //    //{
        //    //    lblKolicina.Visible = true;

        //    //}
        //    //else
        //    //{
        //        int id = Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value);
        //        UgradjenaOprema ugr = new UgradjenaOprema();
        //        ugr.ArtikalId = id;
        //        ugr.VoziloId = nalog.VoziloId;
        //       /*ugr.Kolicina = Convert.ToInt32(txtKolicina.Text);*/
        //         ugr.NalogId = nalog.Id;
        //        ugr.DatumUgradnje = DateTime.Now;

        //        HttpResponseMessage response = ugradjenaOpremaService.PostResponse(ugr);


        //        if (response.IsSuccessStatusCode)
        //        {
        //            MessageBox.Show(Globall.GetMessage("ugr_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            //ClearInput();
        //            BindForm();
        //        }
        //        else
        //        {
        //            MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }


        //    }

        private void btnDodajUslugu_Click(object sender, EventArgs e)
        {
            using (ListaUsluga frm = new ListaUsluga() { Usluga = new UslugaDTO() })
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    uslugaBindingSource.Add(frm.Usluga);

                }

            }

        }

        private void btnDodajOpremu_Click(object sender, EventArgs e)
        {
            using (ListaZaduzene frm = new ListaZaduzene(nalog) { Ugradjena = new UgradjenaOpremaDTO() })
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    UgradjenabindingSource.Add(frm.Ugradjena);

                }

            }
        }

        private void btnZakljuci_Click(object sender, EventArgs e)
        {
            int idUsluge = Convert.ToInt32(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[0].Value);
            nalog.DatumZavrsetka = DateTime.Now.ToLongDateString();
            var uslugaNalog = nalog;
            uslugaNalog.Opis = rtxtOpis.Text;
            uslugaNalog.Usluge = new List<Usluga>();
            List<Usluga> listaUsluga = new List<Usluga>();

            for (int i = 0; i < dgUsluge.RowCount; i++)
            {                
                Usluga u = new Usluga();
                u.Id = Convert.ToInt32(dgUsluge.Rows[i].Cells["Id"].Value);
                u.VrstaUslugeId = Convert.ToInt32(dgUsluge.Rows[i].Cells["VrstaUslugeId"].Value);
                u.Sifra = Convert.ToInt32(dgUsluge.Rows[i].Cells["SifraUsluge"].Value);
                u.Naziv = Convert.ToString(dgUsluge.Rows[i].Cells["NazivUsluge"].Value);
                u.Cijena = Convert.ToInt32(dgUsluge.Rows[i].Cells["Cijena"].Value);
                u.JedinicaMjereId = Convert.ToInt32(dgUsluge.Rows[i].Cells["JedMjereId"].Value);
                uslugaNalog.Usluge.Add(u);
            }

            HttpResponseMessage responseUslugaNalog = nalogService.PostActionResponse("Updateuj", uslugaNalog);
            if (responseUslugaNalog.IsSuccessStatusCode)
            {
                int id = Convert.ToInt32(dgUgradjenaOprema.Rows[dgUgradjenaOprema.CurrentRow.Index].Cells[0].Value);
                UgradjenaOprema ugradjena = new UgradjenaOprema();

                //ugradjena.VoziloId = nalog.VoziloId;
                ugradjena.NalogId = nalog.Id;
                ugradjena.DatumUgradnje = DateTime.Now;
                

                if (ugradjena.UgradjenaOpremaStavke == null)
                {
                    //It's null - create it
                    ugradjena.UgradjenaOpremaStavke = new List<UgradjenaOpremaStavke>();
                    for (int i = 0; i < dgUgradjenaOprema.RowCount; i++)
                    {
                        UgradjenaOpremaStavke s = new UgradjenaOpremaStavke();
                        s.Kolicina = Convert.ToInt32(dgUgradjenaOprema.Rows[i].Cells[4].Value);
                        s.ArtikalId = Convert.ToInt32(dgUgradjenaOprema.Rows[i].Cells[0].Value);
                        ugradjena.UgradjenaOpremaStavke.Add(s);
                    }


                    HttpResponseMessage response = ugradjenaOpremaService.PostResponse(ugradjena);

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show(Globall.GetMessage("nalogClosed_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //  ClearInput();
                        btnPrint.Enabled = true;
                        btnZakljuci.Enabled = false;
                        BindForm();
                    }
                    else
                    {
                        MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            //private void ClearInput()
            //{
            //    txtKolicina.Text = txtSifra.Text = "";
            //    cmbVrsta.SelectedValue = null;

            //}

            
        }

        private void btnDodajUslugu_Click_1(object sender, EventArgs e)
        {
            using (ListaUsluga frm = new ListaUsluga() { Usluga = new UslugaDTO() })
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    uslugaBindingSource.Add(frm.Usluga);

                }

            }
        }

        private void btnDodajOpremu_Click_1(object sender, EventArgs e)
        {

            using (ListaZaduzene frm = new ListaZaduzene(nalog,vecUgradjena) { Ugradjena = new UgradjenaOpremaDTO() })
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    if (frm.ValidateChildren())
                    {
                        int kol;
                        foreach (DataGridViewRow row in dgUgradjenaOprema.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[0].Value) == frm.Ugradjena.ArtikalId)
                            {
                                kol = Convert.ToInt32(row.Cells["KolicinaUgradjenog"].Value);
                                kol += frm.Ugradjena.KolicinaUgradjenog;
                                row.Cells["KolicinaUgradjenog"].Value = kol;
                                return;
                            }
                        }
                        UgradjenabindingSource.Add(frm.Ugradjena);
                        dgUgradjenaOprema.ClearSelection();
                    }
                }
            }

            
        }

        private void btnIzbrisiOpremu_Click(object sender, EventArgs e)
        {
            if (dgUgradjenaOprema.SelectedRows.Count > 0)
            {
                int row = dgUgradjenaOprema.CurrentCell.RowIndex;
                dgUgradjenaOprema.Rows.RemoveAt(row);
            }
        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            if (dgUsluge.SelectedRows.Count > 0)
            {
                int row = dgUsluge.CurrentCell.RowIndex;
                dgUsluge.Rows.RemoveAt(row);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DetaljiNalogaReport frm = new DetaljiNalogaReport(nalog);
            frm.ShowDialog();
        }
    }

}