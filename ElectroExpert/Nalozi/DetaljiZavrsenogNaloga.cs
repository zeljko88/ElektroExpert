﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Nalozi
{
    public partial class DetaljiZavrsenogNaloga : MetroFramework.Forms.MetroForm
    {

        WebApiHelper ugradjenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/UgradjenaOprema");
        WebApiHelper nalogService = new WebApiHelper("http://localhost:63632/", "api/Nalog");
        WebApiHelper uslugaService = new WebApiHelper("http://localhost:63632/", "api/Usluga");
        WebApiHelper radnikService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        private NalogDTO nalog { get; set; }
        public DetaljiZavrsenogNaloga(NalogDTO nalog)
        {
            InitializeComponent();
            dgUgradjenaOprema.AutoGenerateColumns = false;
            if (nalog != null)
                this.nalog = nalog;
        }

        private void DetaljiZavrsenogNaloga_Load(object sender, EventArgs e)
        {
            BindForm();
        }

        private void BindForm()
        {
            lblBrNaloga.Text = nalog.BrojNaloga.ToString();
            lblDatumOtv.Text = nalog.DatumOtvaranja.ToString();
            lblDatumZatvaranja.Text = nalog.DatumZavrsetka.ToString();
            lblPretplatnik.Text = nalog.Pretplatnik;
            lblAdresaInst.Text = nalog.AdresaInstalacije;         
            lblStatus.Text = nalog.Status = false ? "Otvoren" : "Završen";
            rtxtNapomena.Text = nalog.Opis;

            HttpResponseMessage ugradjenaResponse = ugradjenaOpremaService.GetActionResponse("GetUgradjenaOpremaByNalog", nalog.Id.ToString());
            if (ugradjenaResponse.IsSuccessStatusCode)
            {
                dgUgradjenaOprema.DataSource = ugradjenaResponse.Content.ReadAsAsync<List<UgradjenaOpremaDTO>>().Result;

            }


            HttpResponseMessage uslugaResponse = uslugaService.GetActionResponse("GetUslugeByNalogId", nalog.Id.ToString());
            if (uslugaResponse.IsSuccessStatusCode)
            {
                dgOdradjeneUsluge.DataSource = uslugaResponse.Content.ReadAsAsync<List<UslugaNalogDTO>>().Result;

            }

            HttpResponseMessage radnikResponse = radnikService.GetActionResponse("GetRadniciByVozilo", nalog.VoziloId.ToString());
            if (radnikResponse.IsSuccessStatusCode)
            {
                List<VoziloDTO> vozila = radnikResponse.Content.ReadAsAsync<List<VoziloDTO>>().Result;

                int i = vozila.Count();

                if (i == 1)
                {
                    lblRadnik1.Text = vozila[0].SefVozila;
                    lblRadnik2.Visible = false;
                }
                else
                {

                    lblRadnik1.Text = vozila[0].SefVozila;
                    lblRadnik2.Text = vozila[1].SefVozila;
                }
            }

            



        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DetaljiNalogaReport frm = new DetaljiNalogaReport(nalog);
            frm.ShowDialog();
        }
    }
}
