﻿using ElectroExpert.Izvjestaji;
using ElectroExpert.Skladiste;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Artikli
{
    public partial class ListaArtikala : MetroFramework.Forms.MetroForm
    {
        WebApiHelper ulazStavkeService = new WebApiHelper("http://localhost:63632/", "api/UlazStavke");
        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");
        WebApiHelper vrstaService = new WebApiHelper("http://localhost:63632/", "api/Vrsta");
        WebApiHelper stanjeSkladistaService = new WebApiHelper("http://localhost:63632/", "api/StanjeSkladista");
        public ListaArtikala()
        {
            InitializeComponent();
            dgOpremaSkladiste.AutoGenerateColumns = false;
        }

        private void ListaArtikala_Load(object sender, EventArgs e)
        {
            BindGrid();
            //BindVrsta();
        }


        private void BindGrid()
        {



            HttpResponseMessage response = stanjeSkladistaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                dgOpremaSkladiste.DataSource = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

            }


        }

        //private void BindVrsta()
        //{
        //    HttpResponseMessage response = vrstaService.GetResponse();
        //    if (response.IsSuccessStatusCode)
        //    {


        //        List<Vrsta> vrste = response.Content.ReadAsAsync<List<Vrsta>>().Result;
        //        vrste.Insert(0, new Vrsta());
        //        cmbVrsta.DataSource = vrste;
        //        cmbVrsta.DisplayMember = "Naziv";
        //        cmbVrsta.ValueMember = "Id";

        //    }

        //}

        private void txtSifra_TextChanged(object sender, EventArgs e)
        {

            if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*$"))
            {
               
                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtSifra, "");
            }

            TextBox ak = sender as TextBox;
            string ar = ak.Text as string;
            if (ak.Text == "")
            {
                HttpResponseMessage responsedva = stanjeSkladistaService.GetResponse();
                if (responsedva.IsSuccessStatusCode)
                {
                    dgOpremaSkladiste.DataSource = responsedva.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

                }
            }

            List<StanjeSkladistaDTO> artikli = new List<StanjeSkladistaDTO>();
            List<StanjeSkladistaDTO> listaArtikala = new List<StanjeSkladistaDTO>();

            HttpResponseMessage response = stanjeSkladistaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                artikli = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

            }

            foreach (StanjeSkladistaDTO item in artikli)
            {
                if (Regex.IsMatch(item.Sifra, ar, RegexOptions.IgnoreCase))
                {
                    listaArtikala.Add(item);
                }
            }

            dgOpremaSkladiste.DataSource = listaArtikala;
        }





        private void BindGrid(string id)
        {

            HttpResponseMessage response = stanjeSkladistaService.GetActionResponse("GetArtikalByVrsta", id);

            if (response.IsSuccessStatusCode)
            {
                dgOpremaSkladiste.DataSource = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;


            }
        }



        //private void cmbVrsta_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (Convert.ToInt32(cmbVrsta.SelectedIndex) > 0)
        //    {
        //        BindGrid(cmbVrsta.SelectedIndex.ToString());

        //    }
        //    else

        //        BindGrid();
        //}

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            PrijemRobe frm = new PrijemRobe();
            frm.ShowDialog();
        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            StanjeSkladistaReport frm = new StanjeSkladistaReport();
            frm.ShowDialog();
        }

        private void izbirišiToolStripMenuItem_Click(object sender, EventArgs e)
        {


            var confirmResult = MessageBox.Show("Jeste li sigurni da želite obrisati artikal na stanju ?", "Upozorenje !", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (confirmResult == DialogResult.Yes)
            {
                int id = Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[0].Value);


                HttpResponseMessage httpResponse = stanjeSkladistaService.GetActionResponse("DeleteArtikal", id.ToString());
                if (httpResponse.IsSuccessStatusCode)
                {
                    dgOpremaSkladiste.DataSource = httpResponse.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

                }
            }
            else
            {
                return;
            }


        }

        private void urediToolStripMenuItem_Click(object sender, EventArgs e)
        {

           
            UrediStanje frm = new UrediStanje(Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[0].Value));
            frm.FormClosing += new FormClosingEventHandler(this.UrediStanje_FormClosing);
            frm.Show();
        }

        private void UrediStanje_FormClosing(object sender, FormClosingEventArgs e)
        {
            BindGrid();
        }

    }
}