﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Artikli
{
    public partial class ArtikliBaza : MetroFramework.Forms.MetroForm
    {
        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");
        public ArtikliBaza()
        {
            InitializeComponent();
            dgArtikliBaza.AutoGenerateColumns = false;
        }

        private void ArtikliBaza_Load(object sender, EventArgs e)
        {
            BindArtikliBaza();
        }

        private void BindArtikliBaza()
        {
            HttpResponseMessage response = artikliService.GetActionResponse("GetArtikliBaza");
            if (response.IsSuccessStatusCode)
            {
                dgArtikliBaza.DataSource = response.Content.ReadAsAsync<List<ArtikalPrijemDTO>>().Result;

            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajArtikal frm = new DodajArtikal();
            frm.ShowDialog();
        }

        private void urediToolStripMenuItem_Click(object sender, EventArgs e)
        {


                UrediArtikal frm = new UrediArtikal(Convert.ToInt32(dgArtikliBaza.Rows[dgArtikliBaza.CurrentRow.Index].Cells[0].Value));
                frm.FormClosing += new FormClosingEventHandler(this.UrediArtikal_FormClosing);
                frm.Show();
            

          
        }

        private void UrediArtikal_FormClosing(object sender, FormClosingEventArgs e)
        {
            BindArtikliBaza();
        }
    }
}
