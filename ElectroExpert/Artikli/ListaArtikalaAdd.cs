﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Artikli
{
    public partial class ListaArtikalaAdd : MetroFramework.Forms.MetroForm
    {

        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");

        public ArtikalPrijemDTO Artikal { get; set; }
        public ListaArtikalaAdd()
        {
            InitializeComponent();
            dgArtikliBaza.AutoGenerateColumns = false;
            //this.AutoValidate = AutoValidate.Disable;
        }

        private void ListaArtikalaAdd_Load(object sender, EventArgs e)
        {

            BindArtikliBaza();

        }

        private void BindArtikliBaza()
        {
            HttpResponseMessage response = artikliService.GetActionResponse("GetArtikliBaza");
            if (response.IsSuccessStatusCode)
            {
                dgArtikliBaza.DataSource = response.Content.ReadAsAsync<List<ArtikalPrijemDTO>>().Result;

            }
        }

        private void btnDodajArtikal_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {     
                Artikal.Id = Convert.ToInt32(dgArtikliBaza.CurrentRow.Cells["Id"].Value);
                Artikal.Naziv = Convert.ToString(dgArtikliBaza.CurrentRow.Cells["Naziv"].Value);
                Artikal.Sifra = Convert.ToString(dgArtikliBaza.CurrentRow.Cells["Sifra"].Value);
                Artikal.JedMjere = Convert.ToString(dgArtikliBaza.CurrentRow.Cells["JedMjere"].Value);
                Artikal.Vrsta = Convert.ToString(dgArtikliBaza.CurrentRow.Cells["Vrsta"].Value);
                Artikal.Kolicina = Convert.ToInt32(txtKolicina.Text);
                
            }
        }

        #region Validacija
        private void txtKolicina_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKolicina.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtKolicina, "");
            }
        }

        private void txtSifra_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtSifra, "");
            }

            TextBox ak = sender as TextBox;
            string ar = ak.Text as string;
            if (ak.Text == "")
            {
                HttpResponseMessage responsedva = artikliService.GetActionResponse("GetArtikliBaza");
                if (responsedva.IsSuccessStatusCode)
                {
                    dgArtikliBaza.DataSource = responsedva.Content.ReadAsAsync<List<ArtikalPrijemDTO>>().Result;

                }
            }

            List<ArtikalPrijemDTO> artikli = new List<ArtikalPrijemDTO>();
            List<ArtikalPrijemDTO> listaArtikala = new List<ArtikalPrijemDTO>();

            HttpResponseMessage response = artikliService.GetActionResponse("GetArtikliBaza");
            if (response.IsSuccessStatusCode)
            {
                artikli = response.Content.ReadAsAsync<List<ArtikalPrijemDTO>>().Result;

            }

            foreach (ArtikalPrijemDTO item in artikli)
            {
                if (Regex.IsMatch(item.Sifra, ar, RegexOptions.IgnoreCase))
                {
                    listaArtikala.Add(item);
                }
            }

            dgArtikliBaza.DataSource = listaArtikala;
        }

        private void txtKolicina_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtKolicina, "");
            }
        } 
        #endregion
    }
}

