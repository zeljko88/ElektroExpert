﻿using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Artikli
{
    public partial class UrediArtikal : MetroFramework.Forms.MetroForm
    {
        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");
        WebApiHelper jedMjereService = new WebApiHelper("http://localhost:63632/", "api/JedinicaMjere");
        WebApiHelper vrstaService = new WebApiHelper("http://localhost:63632/", "api/Vrsta");
        private int id;

        public Artikal a { get;  set; }

        public UrediArtikal()
        {
            InitializeComponent();
        }

        public UrediArtikal(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void UrediArtikal_Load(object sender, EventArgs e)
        {

          
            BindVrste();
            BindJediniceMjere();
            HttpResponseMessage response = artikliService.GetActionResponse("GetArtikalById",id.ToString());
            if (response.IsSuccessStatusCode)
            {
                a = response.Content.ReadAsAsync<Artikal>().Result;

                cbxJedMjere.SelectedValue = a.JedinicaMjereId;       
               cbxVrsta.SelectedValue = a.VrstaId;              
                txtNaziv.Text = a.Naziv;
                txtSifra.Text = a.Sifra;
                rtxtOpis.Text = a.Opis;

            }

           
        }
        

   

        public void BindVrste()
        {
            HttpResponseMessage response = vrstaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                List<Vrsta> vrste = response.Content.ReadAsAsync<List<Vrsta>>().Result;

                cbxVrsta.DataSource = vrste;
                cbxVrsta.DisplayMember = "Naziv";
                cbxVrsta.ValueMember = "Id";

            }

        }
        private void BindJediniceMjere()
        {
            HttpResponseMessage response = jedMjereService.GetResponse();
            if (response.IsSuccessStatusCode)
            {

                List<JedinicaMjere> jedMjere = response.Content.ReadAsAsync<List<JedinicaMjere>>().Result;

                cbxJedMjere.DataSource = jedMjere;
                cbxJedMjere.DisplayMember = "Naziv";
                cbxJedMjere.ValueMember = "Id";
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSnimiArtikal_Click(object sender, EventArgs e)
        {

            if (this.ValidateChildren())
            {
                a.Sifra = txtSifra.Text;
                a.Naziv = txtNaziv.Text;
                a.Opis = rtxtOpis.Text;
                a.JedinicaMjereId = Convert.ToInt32(cbxJedMjere.SelectedValue);
                a.VrstaId = Convert.ToInt32(cbxVrsta.SelectedValue);



                HttpResponseMessage response = artikliService.PostActionResponse("UrediArtikal", a);
                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("art_update"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void cbxVrsta_Validating(object sender, CancelEventArgs e)
        {
            if (cbxVrsta.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxVrsta, Globall.GetMessage("vrs_req"));
            }
            else
            {
                errorProvider1.SetError(cbxVrsta, "");
            }
        }

        private void txtNaziv_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNaziv.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtNaziv, Globall.GetMessage("empty"));

            }
            else if (Regex.IsMatch(txtNaziv.Text, "^[0 - 9a - zA - Z] + $"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtNaziv, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(txtNaziv, "");

            }
        }

        private void txtSifra_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSifra.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtSifra, Globall.GetMessage("empty"));

            }


            else if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*[1-9][0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));

            }

            else
            {
                errorProvider1.SetError(txtSifra, "");
            }
        }
    }
}
                                                                    
