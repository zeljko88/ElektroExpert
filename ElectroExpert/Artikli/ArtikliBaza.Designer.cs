﻿namespace ElectroExpert.Artikli
{
    partial class ArtikliBaza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgArtikliBaza = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sifra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vrsta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuAddEditDelete = new System.Windows.Forms.MenuStrip();
            this.btnDodaj = new System.Windows.Forms.ToolStripMenuItem();
            this.urediToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgArtikliBaza)).BeginInit();
            this.menuAddEditDelete.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgArtikliBaza
            // 
            this.dgArtikliBaza.AllowUserToAddRows = false;
            this.dgArtikliBaza.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgArtikliBaza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgArtikliBaza.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Sifra,
            this.Vrsta,
            this.Naziv,
            this.JedMjere});
            this.dgArtikliBaza.Location = new System.Drawing.Point(20, 132);
            this.dgArtikliBaza.MultiSelect = false;
            this.dgArtikliBaza.Name = "dgArtikliBaza";
            this.dgArtikliBaza.RowHeadersWidth = 40;
            this.dgArtikliBaza.RowTemplate.Height = 26;
            this.dgArtikliBaza.RowTemplate.ReadOnly = true;
            this.dgArtikliBaza.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgArtikliBaza.Size = new System.Drawing.Size(989, 502);
            this.dgArtikliBaza.TabIndex = 15;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // Sifra
            // 
            this.Sifra.DataPropertyName = "Sifra";
            this.Sifra.HeaderText = "Šifra";
            this.Sifra.Name = "Sifra";
            // 
            // Vrsta
            // 
            this.Vrsta.DataPropertyName = "Vrsta";
            this.Vrsta.HeaderText = "Vrsta";
            this.Vrsta.Name = "Vrsta";
            // 
            // Naziv
            // 
            this.Naziv.DataPropertyName = "Naziv";
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            // 
            // JedMjere
            // 
            this.JedMjere.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jedinica mjere";
            this.JedMjere.Name = "JedMjere";
            // 
            // menuAddEditDelete
            // 
            this.menuAddEditDelete.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuAddEditDelete.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodaj,
            this.urediToolStripMenuItem});
            this.menuAddEditDelete.Location = new System.Drawing.Point(20, 60);
            this.menuAddEditDelete.Name = "menuAddEditDelete";
            this.menuAddEditDelete.Size = new System.Drawing.Size(981, 56);
            this.menuAddEditDelete.TabIndex = 19;
            this.menuAddEditDelete.Text = "menuStrip1";
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDodaj.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(110, 52);
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // urediToolStripMenuItem
            // 
            this.urediToolStripMenuItem.Image = global::ElectroExpert.Properties.Resources.if_edit_find_replace_118921__1_;
            this.urediToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.urediToolStripMenuItem.Name = "urediToolStripMenuItem";
            this.urediToolStripMenuItem.Size = new System.Drawing.Size(105, 52);
            this.urediToolStripMenuItem.Text = "Uredi";
            this.urediToolStripMenuItem.Click += new System.EventHandler(this.urediToolStripMenuItem_Click);
            // 
            // ArtikliBaza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 645);
            this.Controls.Add(this.menuAddEditDelete);
            this.Controls.Add(this.dgArtikliBaza);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "ArtikliBaza";
            this.Resizable = false;
            this.Text = "Artikli u bazi";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ArtikliBaza_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgArtikliBaza)).EndInit();
            this.menuAddEditDelete.ResumeLayout(false);
            this.menuAddEditDelete.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgArtikliBaza;
        private System.Windows.Forms.MenuStrip menuAddEditDelete;
        private System.Windows.Forms.ToolStripMenuItem btnDodaj;
        private System.Windows.Forms.ToolStripMenuItem urediToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sifra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vrsta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
    }
}