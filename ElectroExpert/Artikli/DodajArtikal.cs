﻿using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Artikli
{
    public partial class DodajArtikal : MetroFramework.Forms.MetroForm
    {

        WebApiHelper vrstaService = new WebApiHelper("http://localhost:63632/", "api/Vrsta");
        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");
        WebApiHelper jedMjereService = new WebApiHelper("http://localhost:63632/", "api/JedinicaMjere");
       

        private Artikal artikal;
        public DodajArtikal()
        {
            InitializeComponent();
        }

        private void DodajArtikal_Load(object sender, EventArgs e)
        {
            BindVrste();
            BindJediniceMjere();
            

        }

        
        private void BindJediniceMjere()
        {
            HttpResponseMessage response = jedMjereService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<JedinicaMjere> jedMjere = response.Content.ReadAsAsync<List<JedinicaMjere>>().Result;
                jedMjere.Insert(0, new JedinicaMjere());
                cbxJedMjere.DataSource = jedMjere;
                cbxJedMjere.DisplayMember = "Naziv";
                cbxJedMjere.ValueMember = "Id";
            }
        }

        public void BindVrste()
        {
            HttpResponseMessage response = vrstaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Vrsta> vrste = response.Content.ReadAsAsync<List<Vrsta>>().Result;
                vrste.Insert(0, new Vrsta());
                cbxVrsta.DataSource = vrste;
                cbxVrsta.DisplayMember = "Naziv";
                cbxVrsta.ValueMember = "Id";

            }

        }

        //private void btnDodajSliku_Click(object sender, EventArgs e)
        //{
        //    artikal = new Artikal();
        //    openFileDialog1.ShowDialog();
        //    txtSlika.Text = openFileDialog1.FileName;

        //    Image image = Image.FromFile(txtSlika.Text);

        //    MemoryStream ms = new MemoryStream();

        //    image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

        //    artikal.Slika = ms.ToArray();


        //    int resizedImgWidht = Convert.ToInt32(ConfigurationManager.AppSettings["resizedImgWidth"]);
        //    int resizedImgHeight = Convert.ToInt32(ConfigurationManager.AppSettings["resizedImgHeight"]);
        //    int croppedImgWidht = Convert.ToInt32(ConfigurationManager.AppSettings["croppedImgWidth"]);
        //    int croppedImgHeight = Convert.ToInt32(ConfigurationManager.AppSettings["croppedImgHeight"]);

        //    if (image.Width > resizedImgWidht)
        //    {
        //        Image resizedImage = Util.UIHelper.ResizeImage(image, new Size(resizedImgWidht, resizedImgHeight));

        //        pbxSlika.Image = resizedImage;

        //        Image croppedImage = resizedImage;

        //        int croppedXPosition = (resizedImage.Width - croppedImgWidht) / 2;

        //        int croppedYPosition = (resizedImage.Height - croppedImgHeight) / 2;

        //        if (resizedImage.Width >= croppedImgWidht && resizedImage.Height >= croppedImgHeight)
        //        {

        //            croppedImage = Util.UIHelper.CropImage(resizedImage, new Rectangle(croppedXPosition, croppedYPosition, croppedImgWidht, croppedImgHeight));
        //            ms = new MemoryStream();

        //            croppedImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

        //            artikal.SlikaThumb = ms.ToArray();

        //            pbxSlika.Image = croppedImage;
        //        }


        //    }
        //}

        private void btnDodajArtikal_Click(object sender, EventArgs e)
        {


            if (this.ValidateChildren())

            {
                artikal = new Artikal();
                artikal.Sifra = Convert.ToString(txtSifra.Text);
                artikal.Naziv = txtNaziv.Text;
                artikal.Opis = rtxtOpis.Text;
                artikal.Dostupno = true;

                artikal.VrstaId = Convert.ToInt32(cbxVrsta.SelectedValue);

                artikal.JedinicaMjereId = Convert.ToInt32(cbxJedMjere.SelectedValue);

                HttpResponseMessage response = artikliService.PostResponse(artikal);


                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("art_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearInput();
                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }



        private void ClearInput()
        {
            txtNaziv.Text = txtSifra.Text = "";
            //txtSlika.Text = "";
            cbxVrsta.SelectedIndex = 0;
            cbxJedMjere.SelectedIndex = 0;
            rtxtOpis.Text = "";
        }

        private void cbxVrsta_Validating(object sender, CancelEventArgs e)
        {
            if (cbxVrsta.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxVrsta, Globall.GetMessage("vrs_req"));
            }
            else
            {
                errorProvider1.SetError(cbxVrsta, "");
            }

        }

        private void txtNaziv_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNaziv.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtNaziv, Globall.GetMessage("empty"));

            }
            else if (Regex.IsMatch(txtNaziv.Text, "^[0 - 9a - zA - Z] + $"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtNaziv, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(txtNaziv, "");

            }
        }

        private void txtSifra_Validating(object sender, CancelEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtSifra.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtSifra, Globall.GetMessage("empty"));

            }


           else if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*[1-9][0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));

            }

            else  
            {
                errorProvider1.SetError(txtSifra, "");
            }
        }

        private void cbxJedMjere_Validating(object sender, CancelEventArgs e)
        {
            if (cbxJedMjere.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxJedMjere, Globall.GetMessage("jm_req"));
            }
            else
            {
                errorProvider1.SetError(cbxJedMjere, "");
            }
        }
    }
    
}
