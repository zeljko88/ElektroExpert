﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Vozila
{
    public partial class ListaVozila : MetroFramework.Forms.MetroForm
    {

        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        public ListaVozila()
        {
            InitializeComponent();
            dgVozila.AutoGenerateColumns = false;
        }

        private void ListaVozila_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            HttpResponseMessage response = voziloService.GetActionResponse("GetVozila");

            if (response.IsSuccessStatusCode)
            {
                List<ListaVozilaDTO> vozila = response.Content.ReadAsAsync<List<ListaVozilaDTO>>().Result;
                dgVozila.DataSource = vozila;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajVozilo frm = new DodajVozilo();

            

            frm.ShowDialog();
        }

        private void urediToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DodajVozilo frm = new DodajVozilo(Convert.ToInt32(dgVozila.Rows[dgVozila.CurrentRow.Index].Cells[0].Value));
            frm.FormClosing += new FormClosingEventHandler(this.DodajVozilo_FormClosing);
            frm.Show();
        }


        private void DodajVozilo_FormClosing(object sender, FormClosingEventArgs e)
        {
            BindGrid();
        }
    }
}
