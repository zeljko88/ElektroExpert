﻿using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Vozila
{
    public partial class DodajVozilo : MetroFramework.Forms.MetroForm
    {

        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        private int id;

        public Vozilo v { get;  set; }
        public Vozilo voz { get; set; }
        public DodajVozilo(int? id)
        {
            InitializeComponent();

            this.id = Convert.ToInt32(id);

           
        }

        public DodajVozilo()
        {
            InitializeComponent();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {

            if (this.ValidateChildren())
            {
                 voz = new Vozilo();
                if (id != 0)
                {
                    voz.Id = id;

                    voz.Naziv = txtNazivVozila.Text;
                    voz.DatumIstekaReg = dtpDatIstekaReg.Value;
                    voz.RegOznaka = mtRegOznaka.Text;
                }
                else
                {
                   

                    voz.Naziv = txtNazivVozila.Text;
                    voz.DatumIstekaReg = dtpDatIstekaReg.Value;
                    voz.RegOznaka = mtRegOznaka.Text;
                }
                HttpResponseMessage response = voziloService.PostResponse(voz);


                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("voz_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void txtNazivVozila_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNazivVozila.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtNazivVozila, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(txtNazivVozila.Text, "[^A-Za-z0-9]+"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtNazivVozila, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(txtNazivVozila, "");

            }
        }

        private void mtRegOznaka_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mtRegOznaka.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(mtRegOznaka, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(mtRegOznaka.Text, "[^A-Za-z0-9]+"))
            {
                e.Cancel = true;
                errorProvider1.SetError(mtRegOznaka, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(mtRegOznaka, "");

            }
        }

        private void DodajVozilo_Load(object sender, EventArgs e)
        {
            if (id != 0)
            {
                this.Text = "Uredi vozilo";

                HttpResponseMessage response = voziloService.GetResponse(id);


                if (response.IsSuccessStatusCode)
                {
                    v = response.Content.ReadAsAsync<Vozilo>().Result;

                  
                    txtNazivVozila.Text = v.Naziv;
                    dtpDatIstekaReg.Value = v.DatumIstekaReg;
                    mtRegOznaka.Text = v.RegOznaka;

                }
            }
        }
    }
}
