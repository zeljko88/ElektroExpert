﻿namespace ElectroExpert.Vozila
{
    partial class ListaVozila
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgVozila = new System.Windows.Forms.DataGridView();
            this.VoziloId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatIstekaReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegOznaka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuAddEditDelete = new System.Windows.Forms.MenuStrip();
            this.btnDodaj = new System.Windows.Forms.ToolStripMenuItem();
            this.urediToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgVozila)).BeginInit();
            this.menuAddEditDelete.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgVozila
            // 
            this.dgVozila.AllowUserToAddRows = false;
            this.dgVozila.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgVozila.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVozila.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoziloId,
            this.NazivVozila,
            this.DatIstekaReg,
            this.RegOznaka});
            this.dgVozila.Location = new System.Drawing.Point(23, 163);
            this.dgVozila.MultiSelect = false;
            this.dgVozila.Name = "dgVozila";
            this.dgVozila.ReadOnly = true;
            this.dgVozila.RowTemplate.Height = 24;
            this.dgVozila.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVozila.Size = new System.Drawing.Size(706, 332);
            this.dgVozila.TabIndex = 11;
            // 
            // VoziloId
            // 
            this.VoziloId.DataPropertyName = "VoziloId";
            this.VoziloId.HeaderText = "Id";
            this.VoziloId.Name = "VoziloId";
            this.VoziloId.ReadOnly = true;
            this.VoziloId.Visible = false;
            // 
            // NazivVozila
            // 
            this.NazivVozila.DataPropertyName = "NazivVozila";
            this.NazivVozila.HeaderText = "Naziv vozila";
            this.NazivVozila.Name = "NazivVozila";
            this.NazivVozila.ReadOnly = true;
            // 
            // DatIstekaReg
            // 
            this.DatIstekaReg.DataPropertyName = "DatIstekaReg";
            this.DatIstekaReg.HeaderText = "Datum isteka reg.";
            this.DatIstekaReg.Name = "DatIstekaReg";
            this.DatIstekaReg.ReadOnly = true;
            // 
            // RegOznaka
            // 
            this.RegOznaka.DataPropertyName = "RegOznaka";
            this.RegOznaka.HeaderText = "Reg. oznaka";
            this.RegOznaka.Name = "RegOznaka";
            this.RegOznaka.ReadOnly = true;
            // 
            // menuAddEditDelete
            // 
            this.menuAddEditDelete.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuAddEditDelete.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodaj,
            this.urediToolStripMenuItem});
            this.menuAddEditDelete.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuAddEditDelete.Location = new System.Drawing.Point(20, 60);
            this.menuAddEditDelete.Margin = new System.Windows.Forms.Padding(5);
            this.menuAddEditDelete.Name = "menuAddEditDelete";
            this.menuAddEditDelete.Size = new System.Drawing.Size(720, 56);
            this.menuAddEditDelete.TabIndex = 19;
            this.menuAddEditDelete.Text = "menuStrip1";
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDodaj.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(110, 52);
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // urediToolStripMenuItem
            // 
            this.urediToolStripMenuItem.Image = global::ElectroExpert.Properties.Resources.if_edit_find_replace_118921__1_;
            this.urediToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.urediToolStripMenuItem.Name = "urediToolStripMenuItem";
            this.urediToolStripMenuItem.Size = new System.Drawing.Size(105, 52);
            this.urediToolStripMenuItem.Text = "Uredi";
            this.urediToolStripMenuItem.Click += new System.EventHandler(this.urediToolStripMenuItem_Click);
            // 
            // ListaVozila
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 517);
            this.Controls.Add(this.menuAddEditDelete);
            this.Controls.Add(this.dgVozila);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "ListaVozila";
            this.Resizable = false;
            this.Text = "Lista vozila";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ListaVozila_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgVozila)).EndInit();
            this.menuAddEditDelete.ResumeLayout(false);
            this.menuAddEditDelete.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoziloId;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatIstekaReg;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegOznaka;
        private System.Windows.Forms.MenuStrip menuAddEditDelete;
        private System.Windows.Forms.ToolStripMenuItem btnDodaj;
        private System.Windows.Forms.ToolStripMenuItem urediToolStripMenuItem;
    }
}