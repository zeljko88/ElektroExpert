﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Korisnici
{
    public partial class DodajKorisnika : MetroFramework.Forms.MetroForm
    {

        private WebApiHelper korisniciService = new WebApiHelper("http://localhost:63632/", "api/Korisnik");
        public DodajKorisnika()
        {
            InitializeComponent();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {

            if (this.ValidateChildren())
            {
                Korisnik k = new Korisnik();

                k.Ime = txtIme.Text;
                k.Prezime = txtPrezime.Text;
                k.Adresa = txtAdresa.Text;
                k.Email = txtEmail.Text;
                k.Telefon = mtxtTelefon.Text;
                k.KorisnickoIme = txtKorisnickoIme.Text;
                k.LozinkaSalt = UIHelper.GenerateSalt();
                k.LozinkaHash = UIHelper.GenerateHash(txtLozinka.Text, k.LozinkaSalt);
                //  k.Uloge = ulogeList.CheckedItems.Cast<Uloga>().ToList();

                if (ckbAktivan.Checked)
                {
                    k.Status = true;
                }
                else
                {
                    k.Status = false;
                }
                
                HttpResponseMessage response = korisniciService.PostResponse(k);
                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("user_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearInput();
                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ClearInput()
        {

            txtIme.Text = txtPrezime.Text = txtKorisnickoIme.Text = txtAdresa.Text = txtEmail.Text = txtLozinka.Text = "";

        }

        private void txtIme_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtIme.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtIme, Globall.GetMessage("empty"));

            }

            else if (!Regex.IsMatch(txtIme.Text, "^[a-zA-Z]+$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtIme, Globall.GetMessage("let_only"));
            }

           

            else
            {
                errorProvider1.SetError(txtIme, "");
               
            }
        }

        private void txtPrezime_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPrezime.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtPrezime, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(txtPrezime.Text, "^[a-zA-Z]+$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtPrezime, Globall.GetMessage("let_only"));
            }
            else
            {
                errorProvider1.SetError(txtPrezime, "");

            }
        }

        private void txtAdresa_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtAdresa.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtAdresa, Globall.GetMessage("empty"));

            }
            else if (Regex.IsMatch(txtAdresa.Text, "[^A-Za-z0-9]+"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtAdresa, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(txtAdresa, "");

            }
        }

        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {   
                try
                {
                    var addr = new System.Net.Mail.MailAddress(txtEmail.Text);
                errorProvider1.SetError(txtEmail, "");
            }
                catch
                {
                e.Cancel = true;
                errorProvider1.SetError(txtEmail, Globall.GetMessage("email_err"));
            }
            }

        private void txtKorisnickoIme_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKorisnickoIme.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKorisnickoIme, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(txtKorisnickoIme.Text, "^[a-zA-Z]+$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKorisnickoIme, Globall.GetMessage("let_only"));
            }
            else
            {
                errorProvider1.SetError(txtKorisnickoIme, "");

            }
        }

        private void txtLozinka_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtLozinka.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtLozinka, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(txtLozinka.Text, "[^A-Za-z0-9]+"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtLozinka, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(txtLozinka, "");

            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}