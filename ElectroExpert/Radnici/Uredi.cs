﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Radnici
{
    public partial class Uredi : MetroFramework.Forms.MetroForm
    {
        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        public int id;
        public Radnik r { get; set; }
        public Uredi()
        {
            InitializeComponent();
        }

        public Uredi(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void Uredi_Load(object sender, EventArgs e)
        {
            HttpResponseMessage response = radniciService.GetActionResponse("GetRadnikById", id.ToString());
            if (response.IsSuccessStatusCode)
            {
                r = response.Content.ReadAsAsync<Radnik>().Result;

                txtAdresa.Text = r.Adresa;
                txtEmail.Text = r.Email;
                txtIme.Text = r.Ime;
                txtPrezime.Text = r.Prezime;
                mtxtTelefon.Text = r.Telefon;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {

            var confirmResult = MessageBox.Show("Jeste li sigurni ?", "Upozorenje !", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (confirmResult == DialogResult.Yes)
            {

               
                r.Ime = txtIme.Text;
                r.Prezime = txtPrezime.Text;
                r.Telefon = mtxtTelefon.Text;
                r.Adresa = txtAdresa.Text;
                r.Email = txtEmail.Text;



                HttpResponseMessage response = radniciService.PostActionResponse("UrediRadnika", r);
                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("rad_update"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                return;
            }
        }
    }
}
