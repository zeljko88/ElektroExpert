﻿using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Radnici
{
    public partial class DodajRadnika : MetroFramework.Forms.MetroForm
    {
        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");

        public DodajRadnika()
        {
            InitializeComponent();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {

            if (this.ValidateChildren())
            {
                Radnik radnik = new Radnik();

                radnik.Ime = txtIme.Text;
                radnik.Prezime = txtPrezime.Text;
                radnik.Email = txtEmail.Text;
                radnik.VoziloId = Convert.ToInt32(cbxVozilo.SelectedValue);
                radnik.SefVozila = chkSef.Checked ? true : false;
                radnik.Telefon = mtxtTelefon.Text;
                radnik.Adresa = txtAdresa.Text;

                HttpResponseMessage response = radniciService.PostResponse(radnik);


                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("rad_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearInput();
                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ClearInput()
        {
            txtIme.Text = txtPrezime.Text = txtEmail.Text = mtxtTelefon.Text = txtAdresa.Text = "";
           
        }

        private void DodajRadnika_Load(object sender, EventArgs e)
        {
            BindVozilo();
        }

        private void BindVozilo()
        {
            HttpResponseMessage response = voziloService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                vozila.Insert(0, new Vozilo());
                cbxVozilo.DataSource = vozila;
                cbxVozilo.DisplayMember = "Naziv";
                cbxVozilo.ValueMember = "Id";

            }
        }

        //private void cbxVozilo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ProvjeriSef(cbxVozilo.SelectedValue.ToString());
        //}

        private void ProvjeriSef(string id )
        {
            HttpResponseMessage response = radniciService.GetActionResponse("ProvjeriSef",id);
            if (response.IsSuccessStatusCode)
            {

                chkSef.Enabled = false;

            }
            else
                chkSef.Enabled = true;
        }

        //private void cbxVozilo_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    ProvjeriSef(cbxVozilo.SelectedValue.ToString());
        //}

        private void cbxVozilo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ProvjeriSef(cbxVozilo.SelectedValue.ToString());
        }

        private void txtIme_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtIme.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtIme, Globall.GetMessage("empty"));

            }

            else if (!Regex.IsMatch(txtIme.Text, "^[a-zA-Z]+$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtIme, Globall.GetMessage("let_only"));
            }



            else
            {
                errorProvider1.SetError(txtIme, "");

            }
        }

        private void txtPrezime_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPrezime.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtPrezime, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(txtPrezime.Text, "^[a-zA-Z]+$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtPrezime, Globall.GetMessage("let_only"));
            }
            else
            {
                errorProvider1.SetError(txtPrezime, "");

            }
        }

        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {
       
            try
            {
                var addr = new System.Net.Mail.MailAddress(txtEmail.Text);
                errorProvider1.SetError(txtEmail, "");
            }
            catch
            {
                e.Cancel = true;
                errorProvider1.SetError(txtEmail, Globall.GetMessage("email_err"));
            }
        }

        private void txtAdresa_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void txtAdresa_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtAdresa.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtAdresa, Globall.GetMessage("empty"));

            }
            else if (!Regex.IsMatch(txtAdresa.Text, "[^A-Za-z0-9]+"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtAdresa, Globall.GetMessage("letnum_only"));
            }
            else
            {
                errorProvider1.SetError(txtAdresa, "");

            }
        }

        private void cbxVozilo_Validating(object sender, CancelEventArgs e)
        {
            if (cbxVozilo.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxVozilo, Globall.GetMessage("voz_req"));
            }
            else
            {
                errorProvider1.SetError(cbxVozilo, "");
            }
        }
    }
}
