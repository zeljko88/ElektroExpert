﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Radnici
{
    public partial class ListaRadnika : MetroFramework.Forms.MetroForm
    {

        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        public ListaRadnika()
        {
            InitializeComponent();
            dgRadnici.AutoGenerateColumns = false;
        }

        private void ListaRadnika_Load(object sender, EventArgs e)
        {

            BindGrid();

        }



        private void BindGrid()
        {

            HttpResponseMessage response = radniciService.GetActionResponse("GetSviRadnici");

            if (response.IsSuccessStatusCode)
            {
                List<RadnikDTO> radnici = response.Content.ReadAsAsync<List<RadnikDTO>>().Result;
                dgRadnici.DataSource = radnici;
            }

            else
            {
                MessageBox.Show("Greska - Error Code:" + response.StatusCode + "-Message:" + response.ReasonPhrase);
            }
        }


     

        private void btnDodaj_Click_1(object sender, EventArgs e)
        {
            DodajRadnika frm = new DodajRadnika();
            frm.ShowDialog();
        }

        private void urediToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Uredi frm = new Uredi(Convert.ToInt32(dgRadnici.Rows[dgRadnici.CurrentRow.Index].Cells[0].Value));
            frm.FormClosing += new FormClosingEventHandler(this.Uredi_FormClosing);
            frm.ShowDialog();
         
        }

        private void Uredi_FormClosing(object sender, FormClosingEventArgs e)
        {
            BindGrid();
        }

        private void txtIme_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtIme.Text, "^[a-zA-Z]+$"))
            {

                errorProvider1.SetError(txtIme, Globall.GetMessage("let_only"));
            }

            else
            {

                errorProvider1.SetError(txtIme, "");
            }

            TextBox ak = sender as TextBox;
            string ar = ak.Text as string;
            if (ak.Text == "")
            {
                BindGrid();
            }

            List<RadnikDTO> radnici = new List<RadnikDTO>();
            List<RadnikDTO> listaRadnika = new List<RadnikDTO>();

            HttpResponseMessage response = radniciService.GetActionResponse("GetSviRadnici");
            if (response.IsSuccessStatusCode)
            {
                radnici = response.Content.ReadAsAsync<List<RadnikDTO>>().Result;

            }

            foreach (RadnikDTO item in radnici)
            {
                if (Regex.IsMatch(item.Ime+item.Prezime, ar, RegexOptions.IgnoreCase))
                {
                    listaRadnika.Add(item);
                }
            }

            dgRadnici.DataSource = listaRadnika;
        }
    }
}
