﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.PrijenosRobe
{
    public partial class VoziloSkladisteLista : MetroFramework.Forms.MetroForm
    {
        WebApiHelper ulazService = new WebApiHelper("http://localhost:63632/", "api/Ulaz");

        public List<ListaUlazaDTO> ulazi; 
        public VoziloSkladisteLista()
        {
            InitializeComponent();
        }

        private void VoziloSkladisteLista_Load(object sender, EventArgs e)
        {
            HttpResponseMessage response = ulazService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                ulazi = response.Content.ReadAsAsync<List<ListaUlazaDTO>>().Result;
                dgUlazi.DataSource = ulazi;

            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {

        }
    }
}
