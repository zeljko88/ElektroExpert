﻿using ElectroExpert.Izvjestaji;
using ElectroExpert.ZaduzenjeRobe;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.PrijenosRobe
{
    public partial class SkladisteVoziloLista : MetroFramework.Forms.MetroForm
    {
        WebApiHelper zaduzenaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaUlaz");
        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        private List<ZaduzenaListaDTO> zaduzenaOprema { get; set; }
        public SkladisteVoziloLista()
        {
            InitializeComponent();
            dgSkladisteVozilo.AutoGenerateColumns = false;
        }

   
        


        private void SkladisteVoziloLista_Load(object sender, EventArgs e)
        {
            BindVozila();
            BindListaZaduzenja();

           
        }

        private void BindListaZaduzenja()
        {
            HttpResponseMessage response = zaduzenaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                zaduzenaOprema = response.Content.ReadAsAsync<List<ZaduzenaListaDTO>>().Result;
                dgSkladisteVozilo.DataSource = zaduzenaOprema;

            }
        }

        private void BindVozila()
        {
            HttpResponseMessage response = voziloService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                vozila.Insert(0, new Vozilo());
                cbxVozila.DataSource = vozila;
                cbxVozila.DisplayMember = "Naziv";
                cbxVozila.ValueMember = "Id";

            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ZaduziOpremu frm = new ZaduziOpremu();
            this.Close();
            frm.ShowDialog();
        }

        private void btnStampaj_Click(object sender, EventArgs e)
        {

            int noviUlazId = Convert.ToInt32(dgSkladisteVozilo.Rows[dgSkladisteVozilo.CurrentRow.Index].Cells["ZaduzenaOpremaId"].Value);


            string vozilo = Convert.ToString(dgSkladisteVozilo.Rows[dgSkladisteVozilo.CurrentRow.Index].Cells["Vozilo"].Value);
            string datumzaduzenja = Convert.ToString(dgSkladisteVozilo.Rows[dgSkladisteVozilo.CurrentRow.Index].Cells["DatumZaduzenja"].Value);

            DateTime dat=Convert.ToDateTime(datumzaduzenja);

            SkladisteVozilo frm = new SkladisteVozilo(noviUlazId, vozilo, dat);
            frm.ShowDialog();
        }

        private void cbxVozila_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxVozila.SelectedIndex == 0)
            {
                BindListaZaduzenja();
            }

            else
            {
                HttpResponseMessage response = zaduzenaService.GetActionResponse("GetZaduzenaByVozilo", cbxVozila.SelectedValue.ToString());
                if (response.IsSuccessStatusCode)
                {
                    zaduzenaOprema = response.Content.ReadAsAsync<List<ZaduzenaListaDTO>>().Result;
                    dgSkladisteVozilo.DataSource = zaduzenaOprema;

                }
            }
        }
    }
}
