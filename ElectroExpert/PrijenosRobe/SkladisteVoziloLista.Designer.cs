﻿namespace ElectroExpert.PrijenosRobe
{
    partial class SkladisteVoziloLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgSkladisteVozilo = new System.Windows.Forms.DataGridView();
            this.ZaduzenaOpremaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vozilo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumZaduzenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbxVozila = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnDodaj = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStampaj = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgSkladisteVozilo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgSkladisteVozilo
            // 
            this.dgSkladisteVozilo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgSkladisteVozilo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSkladisteVozilo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ZaduzenaOpremaId,
            this.Vozilo,
            this.DatumZaduzenja});
            this.dgSkladisteVozilo.Location = new System.Drawing.Point(9, 213);
            this.dgSkladisteVozilo.Name = "dgSkladisteVozilo";
            this.dgSkladisteVozilo.RowTemplate.Height = 24;
            this.dgSkladisteVozilo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSkladisteVozilo.Size = new System.Drawing.Size(737, 419);
            this.dgSkladisteVozilo.TabIndex = 0;
            // 
            // ZaduzenaOpremaId
            // 
            this.ZaduzenaOpremaId.DataPropertyName = "ZaduzenaOpremaId";
            this.ZaduzenaOpremaId.HeaderText = "ZaduzenaOpremaId";
            this.ZaduzenaOpremaId.Name = "ZaduzenaOpremaId";
            this.ZaduzenaOpremaId.ReadOnly = true;
            this.ZaduzenaOpremaId.Visible = false;
            // 
            // Vozilo
            // 
            this.Vozilo.DataPropertyName = "Vozilo";
            this.Vozilo.HeaderText = "Vozilo";
            this.Vozilo.Name = "Vozilo";
            this.Vozilo.ReadOnly = true;
            // 
            // DatumZaduzenja
            // 
            this.DatumZaduzenja.DataPropertyName = "DatumZaduzenja";
            this.DatumZaduzenja.HeaderText = "Datum zaduženja";
            this.DatumZaduzenja.Name = "DatumZaduzenja";
            this.DatumZaduzenja.ReadOnly = true;
            // 
            // cbxVozila
            // 
            this.cbxVozila.FormattingEnabled = true;
            this.cbxVozila.Location = new System.Drawing.Point(9, 160);
            this.cbxVozila.Name = "cbxVozila";
            this.cbxVozila.Size = new System.Drawing.Size(207, 24);
            this.cbxVozila.TabIndex = 16;
            this.cbxVozila.SelectedIndexChanged += new System.EventHandler(this.cbxVozila_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "Vozilo:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodaj,
            this.btnStampaj});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(726, 56);
            this.menuStrip1.TabIndex = 35;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDodaj.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(110, 52);
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnStampaj
            // 
            this.btnStampaj.Image = global::ElectroExpert.Properties.Resources.if_document_print_38984;
            this.btnStampaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStampaj.Name = "btnStampaj";
            this.btnStampaj.Size = new System.Drawing.Size(124, 52);
            this.btnStampaj.Text = "Štampaj";
            this.btnStampaj.Click += new System.EventHandler(this.btnStampaj_Click);
            // 
            // SkladisteVoziloLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 655);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxVozila);
            this.Controls.Add(this.dgSkladisteVozilo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SkladisteVoziloLista";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Historija zaduženja po vozilu";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.SkladisteVoziloLista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgSkladisteVozilo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgSkladisteVozilo;
        private System.Windows.Forms.ComboBox cbxVozila;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnDodaj;
        private System.Windows.Forms.ToolStripMenuItem btnStampaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZaduzenaOpremaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vozilo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumZaduzenja;
    }
}