﻿namespace ElectroExpert.PrijenosRobe
{
    partial class VoziloSkladisteLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgUlazi = new System.Windows.Forms.DataGridView();
            this.UlazId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Datum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Skladiste = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vozilo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnDodaj = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStampaj = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxVozila = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgUlazi)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgUlazi
            // 
            this.dgUlazi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUlazi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUlazi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UlazId,
            this.Datum,
            this.Skladiste,
            this.Vozilo});
            this.dgUlazi.Location = new System.Drawing.Point(20, 197);
            this.dgUlazi.Name = "dgUlazi";
            this.dgUlazi.RowTemplate.Height = 24;
            this.dgUlazi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUlazi.Size = new System.Drawing.Size(888, 436);
            this.dgUlazi.TabIndex = 0;
            // 
            // UlazId
            // 
            this.UlazId.DataPropertyName = "UlazId";
            this.UlazId.HeaderText = "UlazId";
            this.UlazId.Name = "UlazId";
            this.UlazId.ReadOnly = true;
            this.UlazId.Visible = false;
            // 
            // Datum
            // 
            this.Datum.DataPropertyName = "Datum";
            this.Datum.HeaderText = "Datum";
            this.Datum.Name = "Datum";
            this.Datum.ReadOnly = true;
            // 
            // Skladiste
            // 
            this.Skladiste.DataPropertyName = "Skladiste";
            this.Skladiste.HeaderText = "Skladiste";
            this.Skladiste.Name = "Skladiste";
            this.Skladiste.ReadOnly = true;
            // 
            // Vozilo
            // 
            this.Vozilo.DataPropertyName = "Vozilo";
            this.Vozilo.HeaderText = "Vozilo";
            this.Vozilo.Name = "Vozilo";
            this.Vozilo.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodaj,
            this.btnStampaj});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(888, 56);
            this.menuStrip1.TabIndex = 36;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDodaj.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(110, 52);
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnStampaj
            // 
            this.btnStampaj.Image = global::ElectroExpert.Properties.Resources.if_document_print_38984;
            this.btnStampaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStampaj.Name = "btnStampaj";
            this.btnStampaj.Size = new System.Drawing.Size(124, 52);
            this.btnStampaj.Text = "Štampaj";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 25);
            this.label1.TabIndex = 38;
            this.label1.Text = "Vozilo:";
            // 
            // cbxVozila
            // 
            this.cbxVozila.FormattingEnabled = true;
            this.cbxVozila.Location = new System.Drawing.Point(20, 161);
            this.cbxVozila.Name = "cbxVozila";
            this.cbxVozila.Size = new System.Drawing.Size(207, 24);
            this.cbxVozila.TabIndex = 37;
            // 
            // VoziloSkladisteLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 656);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxVozila);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgUlazi);
            this.MaximizeBox = false;
            this.Name = "VoziloSkladisteLista";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Historija vraćanja na skladište";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.VoziloSkladisteLista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgUlazi)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgUlazi;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnDodaj;
        private System.Windows.Forms.ToolStripMenuItem btnStampaj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn UlazId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Datum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Skladiste;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vozilo;
    }
}