﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using ElectroExpert_API.Models;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class PredracunReport : MetroFramework.Forms.MetroForm
    {
        Predracun p { get; set; }
        string usluga { get; set; }
        string kupac { get; set; }
        public PredracunReport(Predracun p, string usluga,string kupac)
        {
            InitializeComponent();
            this.p = p;
            this.usluga = usluga;
            this.kupac = kupac;
        }

        private void PredracunReport_Load(object sender, EventArgs e)
        {
            string DatumOd= p.DatumOd.ToShortDateString();
            string DatumDo = p.DatumDo.ToShortDateString();

            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            rptPredracun.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectPredracunByIdTableAdapter adapter = new sp_SelectPredracunByIdTableAdapter();

            adapter.Fill(ds.sp_SelectPredracunById, p.Id);
            bindingSource1.DataSource = ds.sp_SelectPredracunById;
            rptPredracun.LocalReport.SetParameters(new ReportParameter("Broj", p.Broj));
            rptPredracun.LocalReport.SetParameters(new ReportParameter("VrstaUsluge", usluga));
             rptPredracun.LocalReport.SetParameters(new ReportParameter("DatumOd", DatumOd));
            rptPredracun.LocalReport.SetParameters(new ReportParameter("DatumDo", DatumDo));
            rptPredracun.LocalReport.SetParameters(new ReportParameter("Kupac", kupac));
         
            this.rptPredracun.RefreshReport();
        }
    }
}
