﻿namespace ElectroExpert.Izvjestaji
{
    partial class OdradjeniNaloziReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rpOdradjeniNalozi = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // rpOdradjeniNalozi
            // 
            this.rpOdradjeniNalozi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpOdradjeniNalozi.LocalReport.ReportEmbeddedResource = "ElectroExpert.Izvjestaji.rpt.OdradjeniNalozi.rdlc";
            this.rpOdradjeniNalozi.Location = new System.Drawing.Point(20, 60);
            this.rpOdradjeniNalozi.Name = "rpOdradjeniNalozi";
            this.rpOdradjeniNalozi.ServerReport.BearerToken = null;
            this.rpOdradjeniNalozi.Size = new System.Drawing.Size(978, 502);
            this.rpOdradjeniNalozi.TabIndex = 1;
            // 
            // OdradjeniNaloziReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 582);
            this.Controls.Add(this.rpOdradjeniNalozi);
            this.MaximizeBox = false;
            this.Name = "OdradjeniNaloziReport";
            this.Load += new System.EventHandler(this.OdradjeniNaloziReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpOdradjeniNalozi;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}