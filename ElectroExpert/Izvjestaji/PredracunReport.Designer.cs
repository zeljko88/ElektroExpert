﻿namespace ElectroExpert.Izvjestaji
{
    partial class PredracunReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rptPredracun = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // rptPredracun
            // 
            this.rptPredracun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptPredracun.LocalReport.ReportEmbeddedResource = "ElectroExpert.Izvjestaji.rpt.Predracun.rdlc";
            this.rptPredracun.Location = new System.Drawing.Point(20, 60);
            this.rptPredracun.Name = "rptPredracun";
            this.rptPredracun.ServerReport.BearerToken = null;
            this.rptPredracun.Size = new System.Drawing.Size(969, 454);
            this.rptPredracun.TabIndex = 0;
            // 
            // PredracunReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 534);
            this.Controls.Add(this.rptPredracun);
            this.MaximizeBox = false;
            this.Name = "PredracunReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.PredracunReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptPredracun;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}