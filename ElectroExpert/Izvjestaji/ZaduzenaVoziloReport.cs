﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class ZaduzenaVoziloReport : MetroFramework.Forms.MetroForm
    {
        private int voziloId;
        private string vozilo;
        public ZaduzenaVoziloReport()
        {
            InitializeComponent();
        }

        public ZaduzenaVoziloReport(int v,string vozilo)
        {
            InitializeComponent();
            this.voziloId = v;
            this.vozilo = vozilo;
        }

        private void ZaduzenaVoziloReport_Load(object sender, EventArgs e)
        {
            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            rptZaduzenaVozilo.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectZaduzenaOpremaByVoziloIdTableAdapter adapter = new sp_SelectZaduzenaOpremaByVoziloIdTableAdapter();

            adapter.Fill(ds.sp_SelectZaduzenaOpremaByVoziloId, voziloId);
            bindingSource1.DataSource = ds.sp_SelectZaduzenaOpremaByVoziloId;
            rptZaduzenaVozilo.LocalReport.SetParameters(new ReportParameter("Vozilo", vozilo));
           

            this.rptZaduzenaVozilo.RefreshReport();
        }
    }
}
