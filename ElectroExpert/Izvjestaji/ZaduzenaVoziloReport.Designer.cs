﻿namespace ElectroExpert.Izvjestaji
{
    partial class ZaduzenaVoziloReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rptZaduzenaVozilo = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // rptZaduzenaVozilo
            // 
            this.rptZaduzenaVozilo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptZaduzenaVozilo.LocalReport.ReportEmbeddedResource = "ElectroExpert.Izvjestaji.rpt.ZaduzenaVozilo.rdlc";
            this.rptZaduzenaVozilo.Location = new System.Drawing.Point(20, 60);
            this.rptZaduzenaVozilo.Name = "rptZaduzenaVozilo";
            this.rptZaduzenaVozilo.ServerReport.BearerToken = null;
            this.rptZaduzenaVozilo.Size = new System.Drawing.Size(828, 519);
            this.rptZaduzenaVozilo.TabIndex = 1;
            // 
            // ZaduzenaVoziloReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 599);
            this.Controls.Add(this.rptZaduzenaVozilo);
            this.MaximizeBox = false;
            this.Name = "ZaduzenaVoziloReport";
            this.Load += new System.EventHandler(this.ZaduzenaVoziloReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptZaduzenaVozilo;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}