﻿namespace ElectroExpert.Izvjestaji
{
    partial class OdradjeniNalozi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgOdradjeniNalozi = new System.Windows.Forms.DataGridView();
            this.NalogId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrojNaloga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Radnik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumZavrsetka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pretplatnik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AdresaInstalacije = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbxVozila = new MetroFramework.Controls.MetroComboBox();
            this.dtpDatumOd = new MetroFramework.Controls.MetroDateTime();
            this.dtpDatumDo = new MetroFramework.Controls.MetroDateTime();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.btnPrintaj = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgOdradjeniNalozi)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "Vozilo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(236, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Od:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(456, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "Do:";
            // 
            // dgOdradjeniNalozi
            // 
            this.dgOdradjeniNalozi.AllowUserToAddRows = false;
            this.dgOdradjeniNalozi.AllowUserToDeleteRows = false;
            this.dgOdradjeniNalozi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgOdradjeniNalozi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOdradjeniNalozi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NalogId,
            this.BrojNaloga,
            this.Radnik,
            this.DatumZavrsetka,
            this.Pretplatnik,
            this.AdresaInstalacije});
            this.dgOdradjeniNalozi.Location = new System.Drawing.Point(23, 165);
            this.dgOdradjeniNalozi.Name = "dgOdradjeniNalozi";
            this.dgOdradjeniNalozi.ReadOnly = true;
            this.dgOdradjeniNalozi.RowTemplate.Height = 24;
            this.dgOdradjeniNalozi.Size = new System.Drawing.Size(798, 388);
            this.dgOdradjeniNalozi.TabIndex = 20;
            // 
            // NalogId
            // 
            this.NalogId.DataPropertyName = "NalogId";
            this.NalogId.HeaderText = "NalogId";
            this.NalogId.Name = "NalogId";
            this.NalogId.ReadOnly = true;
            this.NalogId.Visible = false;
            // 
            // BrojNaloga
            // 
            this.BrojNaloga.DataPropertyName = "BrojNaloga";
            this.BrojNaloga.HeaderText = "Broj naloga";
            this.BrojNaloga.Name = "BrojNaloga";
            this.BrojNaloga.ReadOnly = true;
            // 
            // Radnik
            // 
            this.Radnik.DataPropertyName = "Radnik";
            this.Radnik.HeaderText = "Vozilo";
            this.Radnik.Name = "Radnik";
            this.Radnik.ReadOnly = true;
            // 
            // DatumZavrsetka
            // 
            this.DatumZavrsetka.DataPropertyName = "DatumZavrsetka";
            this.DatumZavrsetka.HeaderText = "Datum završetka";
            this.DatumZavrsetka.Name = "DatumZavrsetka";
            this.DatumZavrsetka.ReadOnly = true;
            // 
            // Pretplatnik
            // 
            this.Pretplatnik.DataPropertyName = "Pretplatnik";
            this.Pretplatnik.HeaderText = "Pretplatnik";
            this.Pretplatnik.Name = "Pretplatnik";
            this.Pretplatnik.ReadOnly = true;
            // 
            // AdresaInstalacije
            // 
            this.AdresaInstalacije.DataPropertyName = "AdresaInstalacije";
            this.AdresaInstalacije.HeaderText = "Adresa instalacije";
            this.AdresaInstalacije.Name = "AdresaInstalacije";
            this.AdresaInstalacije.ReadOnly = true;
            // 
            // cbxVozila
            // 
            this.cbxVozila.FormattingEnabled = true;
            this.cbxVozila.ItemHeight = 24;
            this.cbxVozila.Location = new System.Drawing.Point(23, 105);
            this.cbxVozila.Name = "cbxVozila";
            this.cbxVozila.Size = new System.Drawing.Size(197, 30);
            this.cbxVozila.TabIndex = 31;
            this.cbxVozila.UseSelectable = true;
            // 
            // dtpDatumOd
            // 
            this.dtpDatumOd.Location = new System.Drawing.Point(240, 105);
            this.dtpDatumOd.MinimumSize = new System.Drawing.Size(0, 30);
            this.dtpDatumOd.Name = "dtpDatumOd";
            this.dtpDatumOd.Size = new System.Drawing.Size(203, 30);
            this.dtpDatumOd.TabIndex = 32;
            // 
            // dtpDatumDo
            // 
            this.dtpDatumDo.Location = new System.Drawing.Point(460, 105);
            this.dtpDatumDo.MinimumSize = new System.Drawing.Size(0, 30);
            this.dtpDatumDo.Name = "dtpDatumDo";
            this.dtpDatumDo.Size = new System.Drawing.Size(196, 30);
            this.dtpDatumDo.TabIndex = 33;
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPrikazi.Location = new System.Drawing.Point(675, 100);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(146, 39);
            this.btnPrikazi.TabIndex = 34;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // btnPrintaj
            // 
            this.btnPrintaj.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrintaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintaj.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPrintaj.Location = new System.Drawing.Point(675, 559);
            this.btnPrintaj.Name = "btnPrintaj";
            this.btnPrintaj.Size = new System.Drawing.Size(146, 39);
            this.btnPrintaj.TabIndex = 35;
            this.btnPrintaj.Text = "Štampaj";
            this.btnPrintaj.UseVisualStyleBackColor = false;
            this.btnPrintaj.Click += new System.EventHandler(this.btnPrintaj_Click);
            // 
            // OdradjeniNalozi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 609);
            this.Controls.Add(this.btnPrintaj);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.dtpDatumDo);
            this.Controls.Add(this.dtpDatumOd);
            this.Controls.Add(this.cbxVozila);
            this.Controls.Add(this.dgOdradjeniNalozi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.Name = "OdradjeniNalozi";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Odrađeni nalozi";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.OdradjeniNalozi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgOdradjeniNalozi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgOdradjeniNalozi;
        private MetroFramework.Controls.MetroComboBox cbxVozila;
        private MetroFramework.Controls.MetroDateTime dtpDatumOd;
        private MetroFramework.Controls.MetroDateTime dtpDatumDo;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.Button btnPrintaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn NalogId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojNaloga;
        private System.Windows.Forms.DataGridViewTextBoxColumn Radnik;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumZavrsetka;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pretplatnik;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdresaInstalacije;
    }
}