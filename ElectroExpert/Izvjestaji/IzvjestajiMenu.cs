﻿using ElectroExpert.Skladiste;
using ElectroExpert.ZaduzenjeRobe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class IzvjestajiMenu : MetroFramework.Forms.MetroForm
    {
        public IzvjestajiMenu()
        {
            InitializeComponent();
        }

        private void btnOdradjeniNalozi_Click(object sender, EventArgs e)
        {

            OdradjeniNalozi frm = new OdradjeniNalozi();
            frm.ShowDialog();
        }

        private void btnZaduzenaRoba_Click(object sender, EventArgs e)
        {
            ZaduzenaVozilo frm = new ZaduzenaVozilo();
            frm.ShowDialog();
        }

        private void btnStanjeRobe_Click(object sender, EventArgs e)
        {
            StanjeSkladista frm = new StanjeSkladista();
            frm.ShowDialog();
        }
    }
}
