﻿namespace ElectroExpert.Izvjestaji
{
    partial class StanjeSkladistaReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rptStanjeSkladista = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // rptStanjeSkladista
            // 
            this.rptStanjeSkladista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptStanjeSkladista.LocalReport.ReportEmbeddedResource = "ElectroExpert.Izvjestaji.rpt.StanjeSkladista.rdlc";
            this.rptStanjeSkladista.Location = new System.Drawing.Point(20, 60);
            this.rptStanjeSkladista.Name = "rptStanjeSkladista";
            this.rptStanjeSkladista.ServerReport.BearerToken = null;
            this.rptStanjeSkladista.Size = new System.Drawing.Size(850, 497);
            this.rptStanjeSkladista.TabIndex = 1;
            // 
            // StanjeSkladistaReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 577);
            this.Controls.Add(this.rptStanjeSkladista);
            this.Movable = false;
            this.Name = "StanjeSkladistaReport";
            this.Resizable = false;
            this.Text = "Roba u skladištu";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.StanjeSkladistaReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptStanjeSkladista;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}