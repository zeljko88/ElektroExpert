﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class OdradjeniNaloziReport : MetroFramework.Forms.MetroForm
    {
        int voziloId { get; set; }
        string Vozilo { get; set; }
        DateTime datOd { get; set; }
        DateTime datDo { get; set; }



        public OdradjeniNaloziReport(int voziloId,string Vozilo, DateTime datOd, DateTime datDo)
        {
            InitializeComponent();
            this.voziloId = voziloId;
            this.datOd = datOd;
            this.datDo = datDo;
            this.Vozilo = Vozilo;
            
        }

        private void OdradjeniNaloziReport_Load(object sender, EventArgs e)
        {
            string DatumOd = datOd.ToShortDateString();
            DateTime newDatDo = datDo.AddDays(1);
            string DatumDo = datDo.ToShortDateString();

            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            rpOdradjeniNalozi.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectNaloziByVoziloAndDateTableAdapter adapter = new sp_SelectNaloziByVoziloAndDateTableAdapter();

            adapter.Fill(ds.sp_SelectNaloziByVoziloAndDate,voziloId,datOd, newDatDo);
            bindingSource1.DataSource = ds.sp_SelectNaloziByVoziloAndDate;            

            rpOdradjeniNalozi.LocalReport.SetParameters(new ReportParameter("DatumOd", DatumOd));
            rpOdradjeniNalozi.LocalReport.SetParameters(new ReportParameter("DatumDo", DatumDo));
            rpOdradjeniNalozi.LocalReport.SetParameters(new ReportParameter("Vozilo", Vozilo));


            this.rpOdradjeniNalozi.RefreshReport();
        }
    }
}
