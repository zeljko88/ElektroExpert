﻿namespace ElectroExpert.Izvjestaji
{
    partial class IzvjestajiMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStanjeRobe = new MetroFramework.Controls.MetroTile();
            this.btnOdradjeniNalozi = new MetroFramework.Controls.MetroTile();
            this.btnZaduzenaRoba = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // btnStanjeRobe
            // 
            this.btnStanjeRobe.ActiveControl = null;
            this.btnStanjeRobe.Location = new System.Drawing.Point(113, 115);
            this.btnStanjeRobe.Name = "btnStanjeRobe";
            this.btnStanjeRobe.Size = new System.Drawing.Size(245, 60);
            this.btnStanjeRobe.TabIndex = 6;
            this.btnStanjeRobe.Text = "Roba u skladištu";
            this.btnStanjeRobe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnStanjeRobe.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnStanjeRobe.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.btnStanjeRobe.UseSelectable = true;
            this.btnStanjeRobe.UseTileImage = true;
            this.btnStanjeRobe.Click += new System.EventHandler(this.btnStanjeRobe_Click);
            // 
            // btnOdradjeniNalozi
            // 
            this.btnOdradjeniNalozi.ActiveControl = null;
            this.btnOdradjeniNalozi.Location = new System.Drawing.Point(113, 206);
            this.btnOdradjeniNalozi.Name = "btnOdradjeniNalozi";
            this.btnOdradjeniNalozi.Size = new System.Drawing.Size(245, 60);
            this.btnOdradjeniNalozi.TabIndex = 7;
            this.btnOdradjeniNalozi.Text = "Odrađeni nalozi";
            this.btnOdradjeniNalozi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOdradjeniNalozi.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOdradjeniNalozi.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.btnOdradjeniNalozi.UseSelectable = true;
            this.btnOdradjeniNalozi.UseTileImage = true;
            this.btnOdradjeniNalozi.Click += new System.EventHandler(this.btnOdradjeniNalozi_Click);
            // 
            // btnZaduzenaRoba
            // 
            this.btnZaduzenaRoba.ActiveControl = null;
            this.btnZaduzenaRoba.Location = new System.Drawing.Point(113, 302);
            this.btnZaduzenaRoba.Name = "btnZaduzenaRoba";
            this.btnZaduzenaRoba.Size = new System.Drawing.Size(245, 60);
            this.btnZaduzenaRoba.TabIndex = 8;
            this.btnZaduzenaRoba.Text = "Zadužena roba";
            this.btnZaduzenaRoba.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnZaduzenaRoba.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnZaduzenaRoba.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.btnZaduzenaRoba.UseSelectable = true;
            this.btnZaduzenaRoba.UseTileImage = true;
            this.btnZaduzenaRoba.Click += new System.EventHandler(this.btnZaduzenaRoba_Click);
            // 
            // IzvjestajiMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 470);
            this.Controls.Add(this.btnZaduzenaRoba);
            this.Controls.Add(this.btnOdradjeniNalozi);
            this.Controls.Add(this.btnStanjeRobe);
            this.MaximizeBox = false;
            this.Name = "IzvjestajiMenu";
            this.Resizable = false;
            this.Text = "Izvjestaji ";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile btnStanjeRobe;
        private MetroFramework.Controls.MetroTile btnOdradjeniNalozi;
        private MetroFramework.Controls.MetroTile btnZaduzenaRoba;
    }
}