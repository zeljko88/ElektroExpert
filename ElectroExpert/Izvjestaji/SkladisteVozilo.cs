﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using ElectroExpert_API.Models;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class SkladisteVozilo : Form
    {

        private int noviUlazId;
        private string vozilo;
      
        public DateTime Datum { get; }

        public SkladisteVozilo(int noviUlazId, string vozilo,DateTime datum)
        {
            this.noviUlazId = noviUlazId;
            this.vozilo = vozilo;
            this.Datum = datum;
            InitializeComponent();
        }

        private void SkladisteVozilo_Load(object sender, EventArgs e)
        {

            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            reportViewer1.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectZaduzenaOpremaByIdTableAdapter adapter = new sp_SelectZaduzenaOpremaByIdTableAdapter();

            adapter.Fill(ds.sp_SelectZaduzenaOpremaById, noviUlazId);
            bindingSource1.DataSource = ds.sp_SelectZaduzenaOpremaById;
            reportViewer1.LocalReport.SetParameters(new ReportParameter("Vozilo", vozilo));
            reportViewer1.LocalReport.SetParameters(new ReportParameter("Datum", Datum.ToShortDateString()));
            this.reportViewer1.RefreshReport();
        }
    }
}
