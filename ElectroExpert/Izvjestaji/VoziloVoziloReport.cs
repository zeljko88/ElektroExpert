﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class VoziloVoziloReport : Form
    {

        private int noviUlazId;
        private string vozilo;
        private string vozilo2;
        public VoziloVoziloReport(int noviUlazId, string vozilo, string vozilo2)
        {
            this.noviUlazId = noviUlazId;
            this.vozilo = vozilo;
            this.vozilo2 = vozilo2;
            InitializeComponent();
        }

        private void VoziloVoziloReport_Load(object sender, EventArgs e)
        {

            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            reportViewer1.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectZaduzenaOpremaByIdTableAdapter adapter = new sp_SelectZaduzenaOpremaByIdTableAdapter();

            adapter.Fill(ds.sp_SelectZaduzenaOpremaById, noviUlazId);
            bindingSource1.DataSource = ds.sp_SelectZaduzenaOpremaById;
            reportViewer1.LocalReport.SetParameters(new ReportParameter("vozilo1par", vozilo));
            reportViewer1.LocalReport.SetParameters(new ReportParameter("vozilo2par", vozilo2));
            this.reportViewer1.RefreshReport();
        }
    }
}
