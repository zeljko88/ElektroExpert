﻿namespace ElectroExpert.Izvjestaji
{
    partial class DetaljiNalogaReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rptDetaljiNaloga = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // rptDetaljiNaloga
            // 
            this.rptDetaljiNaloga.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptDetaljiNaloga.LocalReport.ReportEmbeddedResource = "ElectroExpert.Izvjestaji.rpt.DetaljiNaloga.rdlc";
            this.rptDetaljiNaloga.Location = new System.Drawing.Point(20, 60);
            this.rptDetaljiNaloga.Name = "rptDetaljiNaloga";
            this.rptDetaljiNaloga.ServerReport.BearerToken = null;
            this.rptDetaljiNaloga.Size = new System.Drawing.Size(1112, 649);
            this.rptDetaljiNaloga.TabIndex = 1;
            this.rptDetaljiNaloga.Load += new System.EventHandler(this.rptPredracun_Load);
            // 
            // DetaljiNalogaReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1152, 729);
            this.Controls.Add(this.rptDetaljiNaloga);
            this.MaximizeBox = false;
            this.Name = "DetaljiNalogaReport";
            this.Text = "DetaljiNalogaReport";
            this.Load += new System.EventHandler(this.DetaljiNalogaReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptDetaljiNaloga;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bindingSource2;
    }
}