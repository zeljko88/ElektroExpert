﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElectroExpert_API.DTO;

namespace ElectroExpert.Izvjestaji
{
    public partial class DetaljiNalogaReport : MetroFramework.Forms.MetroForm
    {
        private NalogDTO nalog;

        public DetaljiNalogaReport(NalogDTO nalog)
        {
            InitializeComponent();
            this.nalog = nalog;
            nalog.Status = "Zavrsen";
        }

      

        private void rptPredracun_Load(object sender, EventArgs e)
        {
            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            rptDetaljiNaloga.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectUgradjenaByNalogTableAdapter adapter = new sp_SelectUgradjenaByNalogTableAdapter();

            adapter.Fill(ds.sp_SelectUgradjenaByNalog, nalog.Id);
            bindingSource1.DataSource = ds.sp_SelectUgradjenaByNalog;

            ReportDataSource rds2 = new ReportDataSource("DataSet2", bindingSource2);
            rptDetaljiNaloga.LocalReport.DataSources.Add(rds2);
            ElectroExpertv1DataSet ds2 = new ElectroExpertv1DataSet();
            sp_SelectUslugeByNalogTableAdapter adapter2 = new sp_SelectUslugeByNalogTableAdapter();

            adapter2.Fill(ds2.sp_SelectUslugeByNalog, nalog.Id);
            bindingSource2.DataSource = ds2.sp_SelectUslugeByNalog;



            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("Pretplatnik", nalog.Pretplatnik));
            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("Adresa", nalog.AdresaInstalacije));
            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("Status", nalog.Status));
            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("BrojNaloga", nalog.BrojNaloga.ToString()));
            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("DatOt", nalog.DatumOtvaranja.ToString()));
            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("DatZat", nalog.DatumZavrsetka.ToString()));
            rptDetaljiNaloga.LocalReport.SetParameters(new ReportParameter("Vozilo", nalog.Vozilo.ToString()));

            this.rptDetaljiNaloga.RefreshReport();
        }

        private void DetaljiNalogaReport_Load(object sender, EventArgs e)
        {

        }
    }
}
