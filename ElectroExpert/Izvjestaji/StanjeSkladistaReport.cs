﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class StanjeSkladistaReport : MetroFramework.Forms.MetroForm
    {
        public StanjeSkladistaReport()
        {
            InitializeComponent();
        }

        private void StanjeSkladistaReport_Load(object sender, EventArgs e)
        {
            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            rptStanjeSkladista.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectRobaFromSkladisteTableAdapter adapter = new sp_SelectRobaFromSkladisteTableAdapter();

            adapter.Fill(ds.sp_SelectRobaFromSkladiste);
            bindingSource1.DataSource = ds.sp_SelectRobaFromSkladiste;
                        

            this.rptStanjeSkladista.RefreshReport();
        }
    }
}
