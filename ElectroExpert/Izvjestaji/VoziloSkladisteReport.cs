﻿using ElectroExpert.ElectroExpertv1DataSetTableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static ElectroExpert.ElectroExpertv1DataSet;

namespace ElectroExpert.Izvjestaji
{
    public partial class VoziloSkladisteReport : Form
    {
        private int noviUlazId;
        private string vozilo;

        public DateTime Datum { get; }

        public VoziloSkladisteReport(int noviUlazId,string vozilo,DateTime datum)
        {
            this.noviUlazId = noviUlazId;
            this.vozilo = vozilo;
            this.Datum = datum;
            InitializeComponent();
        }

  

        private void VoziloSkladisteReport_Load(object sender, EventArgs e)
        {


            ReportDataSource rds = new ReportDataSource("DataSet1", bindingSource1);
            reportViewer1.LocalReport.DataSources.Add(rds);
            ElectroExpertv1DataSet ds = new ElectroExpertv1DataSet();
            sp_SelectUlazByUlazIdTableAdapter adapter = new sp_SelectUlazByUlazIdTableAdapter();

            adapter.Fill(ds.sp_SelectUlazByUlazId, noviUlazId);
            bindingSource1.DataSource = ds.sp_SelectUlazByUlazId;
            reportViewer1.LocalReport.SetParameters(new ReportParameter("Vozilo", vozilo));
            reportViewer1.LocalReport.SetParameters(new ReportParameter("Datum", Datum.ToShortDateString()));
            this.reportViewer1.RefreshReport();

        }
    }
}
