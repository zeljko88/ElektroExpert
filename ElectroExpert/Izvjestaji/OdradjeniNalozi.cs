﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Izvjestaji
{
    public partial class OdradjeniNalozi : MetroFramework.Forms.MetroForm
    {

        WebApiHelper nalogService = new WebApiHelper("http://localhost:63632/", "api/Nalog");
        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        private List<ZavrseniNaloziDTO> listaNaloga;

        public OdradjeniNalozi()
        {
            InitializeComponent();
            btnPrintaj.Enabled = false;
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {

            if (cbxVozila.SelectedIndex != 0 && dtpDatumOd.Value != dtpDatumDo.Value)
            {

                string datOd = dtpDatumOd.Value.ToString("D");
                string datDo = dtpDatumDo.Value.ToString("D");

                

                HttpResponseMessage responseVozilo = nalogService.GetActionResponse("GetZavrseniNaloziByVoziloAndDatum", cbxVozila.SelectedValue.ToString(), datOd, datDo);
                if (responseVozilo.IsSuccessStatusCode)
                {
                    listaNaloga = responseVozilo.Content.ReadAsAsync<List<ZavrseniNaloziDTO>>().Result;

                    dgOdradjeniNalozi.DataSource = listaNaloga;
                    btnPrintaj.Enabled = true;

                }
            }
          

            else if (cbxVozila.SelectedIndex != 0)
            {
                HttpResponseMessage responseVozilo = nalogService.GetActionResponse("GetZavrseniNaloziByRadnik", cbxVozila.SelectedValue.ToString());
                if (responseVozilo.IsSuccessStatusCode)
                {
                    listaNaloga = responseVozilo.Content.ReadAsAsync<List<ZavrseniNaloziDTO>>().Result;

                    dgOdradjeniNalozi.DataSource = listaNaloga;
                    btnPrintaj.Enabled = true;
                }
            }

            else
            {
                HttpResponseMessage response = nalogService.GetActionResponse("GetZavrseniNalozi");
                if (response.IsSuccessStatusCode)
                {
                    listaNaloga = response.Content.ReadAsAsync<List<ZavrseniNaloziDTO>>().Result;

                    dgOdradjeniNalozi.DataSource = listaNaloga;
                    btnPrintaj.Enabled = true;
                }
            }
        }

        private void OdradjeniNalozi_Load(object sender, EventArgs e)
        {
            HttpResponseMessage response = voziloService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                vozila.Insert(0, new Vozilo());
                cbxVozila.DataSource = vozila;
                cbxVozila.DisplayMember = "Naziv";
                cbxVozila.ValueMember = "Id";

            }
        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            OdradjeniNaloziReport frm = new OdradjeniNaloziReport(Convert.ToInt32(cbxVozila.SelectedValue), cbxVozila.Text, dtpDatumOd.Value, dtpDatumDo.Value);
            frm.ShowDialog();
        }
    }
}
