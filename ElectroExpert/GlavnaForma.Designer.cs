﻿namespace ElectroExpert
{
    partial class GlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.mtPrijenosRobe = new MetroFramework.Controls.MetroTile();
            this.mtSkladiste = new MetroFramework.Controls.MetroTile();
            this.mtPredracuni = new MetroFramework.Controls.MetroTile();
            this.mtRadnici = new MetroFramework.Controls.MetroTile();
            this.mtIzvjestaji = new MetroFramework.Controls.MetroTile();
            this.mtNalozi = new MetroFramework.Controls.MetroTile();
            this.mtPrijemRobe = new MetroFramework.Controls.MetroTile();
            this.mtArtikli = new MetroFramework.Controls.MetroTile();
            this.mtKorisnici = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroTile1.Location = new System.Drawing.Point(242, 402);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(160, 150);
            this.metroTile1.TabIndex = 19;
            this.metroTile1.Text = "Vozila";
            this.metroTile1.TileImage = global::ElectroExpert.Properties.Resources.if_van_285814;
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.UseTileImage = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // mtPrijenosRobe
            // 
            this.mtPrijenosRobe.ActiveControl = null;
            this.mtPrijenosRobe.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtPrijenosRobe.Location = new System.Drawing.Point(76, 249);
            this.mtPrijenosRobe.Name = "mtPrijenosRobe";
            this.mtPrijenosRobe.Size = new System.Drawing.Size(160, 150);
            this.mtPrijenosRobe.TabIndex = 18;
            this.mtPrijenosRobe.Text = "Prijenos robe";
            this.mtPrijenosRobe.TileImage = global::ElectroExpert.Properties.Resources.if_20_2135800__1_;
            this.mtPrijenosRobe.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtPrijenosRobe.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.mtPrijenosRobe.UseSelectable = true;
            this.mtPrijenosRobe.UseTileImage = true;
            this.mtPrijenosRobe.Click += new System.EventHandler(this.mtPrijenosRobe_Click);
            // 
            // mtSkladiste
            // 
            this.mtSkladiste.ActiveControl = null;
            this.mtSkladiste.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtSkladiste.Location = new System.Drawing.Point(242, 93);
            this.mtSkladiste.Name = "mtSkladiste";
            this.mtSkladiste.Size = new System.Drawing.Size(160, 150);
            this.mtSkladiste.TabIndex = 17;
            this.mtSkladiste.Text = "Skladište";
            this.mtSkladiste.TileImage = global::ElectroExpert.Properties.Resources.if_warehouse_103222;
            this.mtSkladiste.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtSkladiste.UseSelectable = true;
            this.mtSkladiste.UseTileImage = true;
            this.mtSkladiste.Click += new System.EventHandler(this.mtSkladiste_Click);
            // 
            // mtPredracuni
            // 
            this.mtPredracuni.ActiveControl = null;
            this.mtPredracuni.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtPredracuni.Location = new System.Drawing.Point(487, 402);
            this.mtPredracuni.Name = "mtPredracuni";
            this.mtPredracuni.Size = new System.Drawing.Size(326, 150);
            this.mtPredracuni.TabIndex = 16;
            this.mtPredracuni.Text = "Predračuni";
            this.mtPredracuni.TileImage = global::ElectroExpert.Properties.Resources.if_moneyreport_2423718__1_;
            this.mtPredracuni.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtPredracuni.UseSelectable = true;
            this.mtPredracuni.UseTileImage = true;
            this.mtPredracuni.Click += new System.EventHandler(this.mtPredracuni_Click);
            // 
            // mtRadnici
            // 
            this.mtRadnici.ActiveControl = null;
            this.mtRadnici.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtRadnici.Location = new System.Drawing.Point(487, 249);
            this.mtRadnici.Name = "mtRadnici";
            this.mtRadnici.Size = new System.Drawing.Size(160, 150);
            this.mtRadnici.TabIndex = 14;
            this.mtRadnici.Text = "Radnici";
            this.mtRadnici.TileImage = global::ElectroExpert.Properties.Resources.if_General_Office_37_2530816;
            this.mtRadnici.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtRadnici.UseSelectable = true;
            this.mtRadnici.UseTileImage = true;
            this.mtRadnici.Click += new System.EventHandler(this.mtRadnici_Click);
            // 
            // mtIzvjestaji
            // 
            this.mtIzvjestaji.ActiveControl = null;
            this.mtIzvjestaji.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtIzvjestaji.Location = new System.Drawing.Point(653, 249);
            this.mtIzvjestaji.Name = "mtIzvjestaji";
            this.mtIzvjestaji.Size = new System.Drawing.Size(160, 150);
            this.mtIzvjestaji.TabIndex = 13;
            this.mtIzvjestaji.Text = "Izvještaji";
            this.mtIzvjestaji.TileImage = global::ElectroExpert.Properties.Resources.if_General_Office_18_2530783__1_;
            this.mtIzvjestaji.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtIzvjestaji.UseSelectable = true;
            this.mtIzvjestaji.UseTileImage = true;
            this.mtIzvjestaji.Click += new System.EventHandler(this.mtIzvjestaji_Click);
            // 
            // mtNalozi
            // 
            this.mtNalozi.ActiveControl = null;
            this.mtNalozi.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtNalozi.Location = new System.Drawing.Point(487, 93);
            this.mtNalozi.Name = "mtNalozi";
            this.mtNalozi.Size = new System.Drawing.Size(326, 150);
            this.mtNalozi.TabIndex = 12;
            this.mtNalozi.Text = "Nalozi";
            this.mtNalozi.TileImage = global::ElectroExpert.Properties.Resources.if_order_history_49596;
            this.mtNalozi.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtNalozi.UseSelectable = true;
            this.mtNalozi.UseTileImage = true;
            this.mtNalozi.Click += new System.EventHandler(this.mtNalozi_Click);
            // 
            // mtPrijemRobe
            // 
            this.mtPrijemRobe.ActiveControl = null;
            this.mtPrijemRobe.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtPrijemRobe.Location = new System.Drawing.Point(76, 93);
            this.mtPrijemRobe.Name = "mtPrijemRobe";
            this.mtPrijemRobe.Size = new System.Drawing.Size(160, 150);
            this.mtPrijemRobe.TabIndex = 11;
            this.mtPrijemRobe.Text = "Prijem robe";
            this.mtPrijemRobe.TileImage = global::ElectroExpert.Properties.Resources.if_General_Office_42_2530810__1_;
            this.mtPrijemRobe.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtPrijemRobe.UseSelectable = true;
            this.mtPrijemRobe.UseTileImage = true;
            this.mtPrijemRobe.Click += new System.EventHandler(this.mtPrijemRobe_Click);
            // 
            // mtArtikli
            // 
            this.mtArtikli.ActiveControl = null;
            this.mtArtikli.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtArtikli.Location = new System.Drawing.Point(242, 249);
            this.mtArtikli.Name = "mtArtikli";
            this.mtArtikli.Size = new System.Drawing.Size(160, 150);
            this.mtArtikli.TabIndex = 10;
            this.mtArtikli.Text = "Artikli";
            this.mtArtikli.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtArtikli.TileImage = global::ElectroExpert.Properties.Resources.if_article_add_61781;
            this.mtArtikli.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtArtikli.UseSelectable = true;
            this.mtArtikli.UseTileImage = true;
            this.mtArtikli.Click += new System.EventHandler(this.mtArtikli_Click);
            // 
            // mtKorisnici
            // 
            this.mtKorisnici.ActiveControl = null;
            this.mtKorisnici.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtKorisnici.Location = new System.Drawing.Point(76, 402);
            this.mtKorisnici.Name = "mtKorisnici";
            this.mtKorisnici.Size = new System.Drawing.Size(160, 150);
            this.mtKorisnici.TabIndex = 20;
            this.mtKorisnici.Text = "Korisnici";
            this.mtKorisnici.TileImage = global::ElectroExpert.Properties.Resources.if_General_Office_37_2530816;
            this.mtKorisnici.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtKorisnici.UseSelectable = true;
            this.mtKorisnici.UseTileImage = true;
            this.mtKorisnici.Click += new System.EventHandler(this.mtKorisnici_Click);
            // 
            // GlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 680);
            this.Controls.Add(this.mtKorisnici);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.mtPrijenosRobe);
            this.Controls.Add(this.mtSkladiste);
            this.Controls.Add(this.mtPredracuni);
            this.Controls.Add(this.mtRadnici);
            this.Controls.Add(this.mtIzvjestaji);
            this.Controls.Add(this.mtNalozi);
            this.Controls.Add(this.mtPrijemRobe);
            this.Controls.Add(this.mtArtikli);
            this.Name = "GlavnaForma";
            this.Padding = new System.Windows.Forms.Padding(10, 60, 10, 10);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Menu";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTile mtArtikli;
        private MetroFramework.Controls.MetroTile mtPrijemRobe;
        private MetroFramework.Controls.MetroTile mtIzvjestaji;
        private MetroFramework.Controls.MetroTile mtNalozi;
        private MetroFramework.Controls.MetroTile mtRadnici;
        private MetroFramework.Controls.MetroTile mtPredracuni;
        private MetroFramework.Controls.MetroTile mtSkladiste;
        private MetroFramework.Controls.MetroTile mtPrijenosRobe;
        private MetroFramework.Controls.MetroTile metroTile1;
        private MetroFramework.Controls.MetroTile mtKorisnici;
    }
}

