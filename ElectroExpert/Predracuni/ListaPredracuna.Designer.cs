﻿namespace ElectroExpert.Predracuni
{
    partial class ListaPredracuna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgPredracuni = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VrstaUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrojPredracuna = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Datum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IznosBezPdv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IznosSaPdv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivKupca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnDodaj = new System.Windows.Forms.ToolStripMenuItem();
            this.btnIzbrisi = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStampaj = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgPredracuni)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgPredracuni
            // 
            this.dgPredracuni.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgPredracuni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPredracuni.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.VrstaUsluge,
            this.BrojPredracuna,
            this.Datum,
            this.IznosBezPdv,
            this.IznosSaPdv,
            this.NazivKupca});
            this.dgPredracuni.Location = new System.Drawing.Point(23, 152);
            this.dgPredracuni.Name = "dgPredracuni";
            this.dgPredracuni.ReadOnly = true;
            this.dgPredracuni.RowTemplate.Height = 24;
            this.dgPredracuni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPredracuni.Size = new System.Drawing.Size(947, 459);
            this.dgPredracuni.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // VrstaUsluge
            // 
            this.VrstaUsluge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.VrstaUsluge.DataPropertyName = "VrstaUsluge";
            this.VrstaUsluge.FillWeight = 71.26904F;
            this.VrstaUsluge.HeaderText = "Vrsta usluge";
            this.VrstaUsluge.Name = "VrstaUsluge";
            this.VrstaUsluge.ReadOnly = true;
            this.VrstaUsluge.Width = 234;
            // 
            // BrojPredracuna
            // 
            this.BrojPredracuna.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BrojPredracuna.DataPropertyName = "BrojPredracuna";
            this.BrojPredracuna.FillWeight = 243.6548F;
            this.BrojPredracuna.HeaderText = "Broj predračuna";
            this.BrojPredracuna.Name = "BrojPredracuna";
            this.BrojPredracuna.ReadOnly = true;
            // 
            // Datum
            // 
            this.Datum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Datum.DataPropertyName = "Datum";
            this.Datum.FillWeight = 71.26904F;
            this.Datum.HeaderText = "Datum";
            this.Datum.Name = "Datum";
            this.Datum.ReadOnly = true;
            this.Datum.Width = 150;
            // 
            // IznosBezPdv
            // 
            this.IznosBezPdv.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IznosBezPdv.DataPropertyName = "IznosBezPdv";
            this.IznosBezPdv.FillWeight = 71.26904F;
            this.IznosBezPdv.HeaderText = "Iznos bez pdv";
            this.IznosBezPdv.Name = "IznosBezPdv";
            this.IznosBezPdv.ReadOnly = true;
            this.IznosBezPdv.Width = 120;
            // 
            // IznosSaPdv
            // 
            this.IznosSaPdv.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IznosSaPdv.DataPropertyName = "IznosSaPdv";
            this.IznosSaPdv.FillWeight = 71.26904F;
            this.IznosSaPdv.HeaderText = "Iznos sa pdv";
            this.IznosSaPdv.Name = "IznosSaPdv";
            this.IznosSaPdv.ReadOnly = true;
            this.IznosSaPdv.Width = 120;
            // 
            // NazivKupca
            // 
            this.NazivKupca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NazivKupca.DataPropertyName = "NazivKupca";
            this.NazivKupca.FillWeight = 71.26904F;
            this.NazivKupca.HeaderText = "Kupac";
            this.NazivKupca.Name = "NazivKupca";
            this.NazivKupca.ReadOnly = true;
            this.NazivKupca.Width = 180;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDodaj,
            this.btnIzbrisi,
            this.btnStampaj});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(5);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(950, 56);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnDodaj
            // 
            this.btnDodaj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDodaj.Image = global::ElectroExpert.Properties.Resources.if_list_add_118777;
            this.btnDodaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(110, 52);
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Image = global::ElectroExpert.Properties.Resources.if_edit_delete_118920;
            this.btnIzbrisi.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(108, 52);
            this.btnIzbrisi.Text = "Izbriši";
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // btnStampaj
            // 
            this.btnStampaj.Image = global::ElectroExpert.Properties.Resources.if_document_print_38984;
            this.btnStampaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnStampaj.Name = "btnStampaj";
            this.btnStampaj.Size = new System.Drawing.Size(124, 52);
            this.btnStampaj.Text = "Štampaj";
            this.btnStampaj.Click += new System.EventHandler(this.btnStampaj_Click);
            // 
            // ListaPredracuna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 634);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgPredracuni);
            this.MaximizeBox = false;
            this.Name = "ListaPredracuna";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Lista predracuna";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ListaPredracuna_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgPredracuni)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgPredracuni;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnDodaj;
        private System.Windows.Forms.ToolStripMenuItem btnIzbrisi;
        private System.Windows.Forms.ToolStripMenuItem btnStampaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn VrstaUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojPredracuna;
        private System.Windows.Forms.DataGridViewTextBoxColumn Datum;
        private System.Windows.Forms.DataGridViewTextBoxColumn IznosBezPdv;
        private System.Windows.Forms.DataGridViewTextBoxColumn IznosSaPdv;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivKupca;
    }
}