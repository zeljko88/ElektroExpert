﻿namespace ElectroExpert.Predracuni
{
    partial class DodajPredracun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cbxUsluga = new MetroFramework.Controls.MetroComboBox();
            this.Datum = new MetroFramework.Controls.MetroLabel();
            this.dtpOd = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dtpDo = new MetroFramework.Controls.MetroDateTime();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.cbxKupac = new MetroFramework.Controls.MetroComboBox();
            this.btnIzradi = new MetroFramework.Controls.MetroButton();
            this.dgPredracunStavke = new MetroFramework.Controls.MetroGrid();
            this.NazivUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UslugaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjereId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CijenaBezPdv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IznosBezPdv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pdv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PdvIznos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CijenaSaPdv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IznosSaPDV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.btnPrintaj = new MetroFramework.Controls.MetroButton();
            this.lblIznosZaNaplatu = new MetroFramework.Controls.MetroLabel();
            this.lblPdv = new MetroFramework.Controls.MetroLabel();
            this.lblBezPdv = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblPrazno = new System.Windows.Forms.Label();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPredracunStavke)).BeginInit();
            this.metroPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxUsluga
            // 
            this.cbxUsluga.FormattingEnabled = true;
            this.cbxUsluga.ItemHeight = 24;
            this.cbxUsluga.Location = new System.Drawing.Point(23, 92);
            this.cbxUsluga.Name = "cbxUsluga";
            this.cbxUsluga.Size = new System.Drawing.Size(224, 30);
            this.cbxUsluga.TabIndex = 0;
            this.cbxUsluga.UseSelectable = true;
            this.cbxUsluga.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbxUsluga_Format);
            this.cbxUsluga.Validating += new System.ComponentModel.CancelEventHandler(this.cbxUsluga_Validating);
            // 
            // Datum
            // 
            this.Datum.AutoSize = true;
            this.Datum.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Datum.Location = new System.Drawing.Point(23, 60);
            this.Datum.Name = "Datum";
            this.Datum.Size = new System.Drawing.Size(118, 20);
            this.Datum.TabIndex = 28;
            this.Datum.Text = "Odaberi usluge:";
            // 
            // dtpOd
            // 
            this.dtpOd.Location = new System.Drawing.Point(51, 44);
            this.dtpOd.MinimumSize = new System.Drawing.Size(0, 30);
            this.dtpOd.Name = "dtpOd";
            this.dtpOd.Size = new System.Drawing.Size(236, 30);
            this.dtpOd.TabIndex = 29;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 12);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(58, 20);
            this.metroLabel1.TabIndex = 30;
            this.metroLabel1.Text = "Period:";
            // 
            // dtpDo
            // 
            this.dtpDo.Location = new System.Drawing.Point(367, 44);
            this.dtpDo.MinimumSize = new System.Drawing.Size(0, 30);
            this.dtpDo.Name = "dtpDo";
            this.dtpDo.Size = new System.Drawing.Size(235, 30);
            this.dtpDo.TabIndex = 31;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.dtpOd);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.dtpDo);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 143);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(971, 126);
            this.metroPanel1.TabIndex = 32;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(316, 54);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(33, 20);
            this.metroLabel3.TabIndex = 33;
            this.metroLabel3.Text = "Do:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(3, 54);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(33, 20);
            this.metroLabel2.TabIndex = 32;
            this.metroLabel2.Text = "Od:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(755, 60);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(113, 20);
            this.metroLabel4.TabIndex = 34;
            this.metroLabel4.Text = "Odaberi kupca:";
            // 
            // cbxKupac
            // 
            this.cbxKupac.FormattingEnabled = true;
            this.cbxKupac.ItemHeight = 24;
            this.cbxKupac.Location = new System.Drawing.Point(755, 92);
            this.cbxKupac.Name = "cbxKupac";
            this.cbxKupac.Size = new System.Drawing.Size(224, 30);
            this.cbxKupac.TabIndex = 33;
            this.cbxKupac.UseSelectable = true;
            this.cbxKupac.Validating += new System.ComponentModel.CancelEventHandler(this.cbxKupac_Validating);
            // 
            // btnIzradi
            // 
            this.btnIzradi.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIzradi.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIzradi.Location = new System.Drawing.Point(23, 275);
            this.btnIzradi.Name = "btnIzradi";
            this.btnIzradi.Size = new System.Drawing.Size(178, 47);
            this.btnIzradi.TabIndex = 35;
            this.btnIzradi.Text = "Izradi";
            this.btnIzradi.UseCustomBackColor = true;
            this.btnIzradi.UseCustomForeColor = true;
            this.btnIzradi.UseSelectable = true;
            this.btnIzradi.Click += new System.EventHandler(this.btnIzradi_Click);
            // 
            // dgPredracunStavke
            // 
            this.dgPredracunStavke.AllowUserToResizeRows = false;
            this.dgPredracunStavke.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgPredracunStavke.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgPredracunStavke.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPredracunStavke.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgPredracunStavke.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPredracunStavke.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPredracunStavke.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPredracunStavke.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NazivUsluge,
            this.UslugaId,
            this.JedMjereId,
            this.JedMjere,
            this.Kolicina,
            this.CijenaBezPdv,
            this.IznosBezPdv,
            this.Pdv,
            this.PdvIznos,
            this.CijenaSaPdv,
            this.IznosSaPDV});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPredracunStavke.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgPredracunStavke.EnableHeadersVisualStyles = false;
            this.dgPredracunStavke.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgPredracunStavke.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgPredracunStavke.Location = new System.Drawing.Point(13, 30);
            this.dgPredracunStavke.Name = "dgPredracunStavke";
            this.dgPredracunStavke.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPredracunStavke.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgPredracunStavke.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgPredracunStavke.RowTemplate.Height = 24;
            this.dgPredracunStavke.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPredracunStavke.Size = new System.Drawing.Size(955, 261);
            this.dgPredracunStavke.TabIndex = 36;
            this.dgPredracunStavke.Visible = false;
            // 
            // NazivUsluge
            // 
            this.NazivUsluge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NazivUsluge.DataPropertyName = "NazivUsluge";
            this.NazivUsluge.HeaderText = "Naziv usluge";
            this.NazivUsluge.Name = "NazivUsluge";
            this.NazivUsluge.Width = 93;
            // 
            // UslugaId
            // 
            this.UslugaId.DataPropertyName = "UslugaId";
            this.UslugaId.HeaderText = "UslugaId";
            this.UslugaId.Name = "UslugaId";
            this.UslugaId.Visible = false;
            // 
            // JedMjereId
            // 
            this.JedMjereId.DataPropertyName = "JedMjereId";
            this.JedMjereId.HeaderText = "JedMjereId";
            this.JedMjereId.Name = "JedMjereId";
            this.JedMjereId.Visible = false;
            // 
            // JedMjere
            // 
            this.JedMjere.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "JM";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.Width = 49;
            // 
            // Kolicina
            // 
            this.Kolicina.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Kolicina.DataPropertyName = "Kolicina";
            this.Kolicina.HeaderText = "Količina";
            this.Kolicina.Name = "Kolicina";
            this.Kolicina.Width = 76;
            // 
            // CijenaBezPdv
            // 
            this.CijenaBezPdv.DataPropertyName = "CijenaBezPdv";
            this.CijenaBezPdv.HeaderText = "Cijena bez PDV";
            this.CijenaBezPdv.Name = "CijenaBezPdv";
            // 
            // IznosBezPdv
            // 
            this.IznosBezPdv.DataPropertyName = "IznosBezPdv";
            this.IznosBezPdv.HeaderText = "Iznos bez PDV";
            this.IznosBezPdv.Name = "IznosBezPdv";
            // 
            // Pdv
            // 
            this.Pdv.DataPropertyName = "Pdv";
            this.Pdv.HeaderText = "PDV";
            this.Pdv.Name = "Pdv";
            // 
            // PdvIznos
            // 
            this.PdvIznos.DataPropertyName = "PdvIznos";
            this.PdvIznos.HeaderText = "PDV Iznos";
            this.PdvIznos.Name = "PdvIznos";
            // 
            // CijenaSaPdv
            // 
            this.CijenaSaPdv.DataPropertyName = "CijenaSaPdv";
            this.CijenaSaPdv.HeaderText = "Cijena sa PDV-om";
            this.CijenaSaPdv.Name = "CijenaSaPdv";
            // 
            // IznosSaPDV
            // 
            this.IznosSaPDV.DataPropertyName = "IznosSaPDV";
            this.IznosSaPDV.HeaderText = "Iznos sa pdv-om";
            this.IznosSaPDV.Name = "IznosSaPDV";
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.lblPrazno);
            this.metroPanel2.Controls.Add(this.btnPrintaj);
            this.metroPanel2.Controls.Add(this.lblIznosZaNaplatu);
            this.metroPanel2.Controls.Add(this.lblPdv);
            this.metroPanel2.Controls.Add(this.lblBezPdv);
            this.metroPanel2.Controls.Add(this.metroLabel7);
            this.metroPanel2.Controls.Add(this.metroLabel6);
            this.metroPanel2.Controls.Add(this.metroLabel5);
            this.metroPanel2.Controls.Add(this.metroButton1);
            this.metroPanel2.Controls.Add(this.dgPredracunStavke);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(23, 328);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(971, 471);
            this.metroPanel2.TabIndex = 39;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            this.metroPanel2.Visible = false;
            // 
            // btnPrintaj
            // 
            this.btnPrintaj.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrintaj.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPrintaj.Location = new System.Drawing.Point(569, 421);
            this.btnPrintaj.Name = "btnPrintaj";
            this.btnPrintaj.Size = new System.Drawing.Size(178, 47);
            this.btnPrintaj.TabIndex = 47;
            this.btnPrintaj.Text = "Printaj";
            this.btnPrintaj.UseCustomBackColor = true;
            this.btnPrintaj.UseCustomForeColor = true;
            this.btnPrintaj.UseSelectable = true;
            this.btnPrintaj.Click += new System.EventHandler(this.btnPrintaj_Click);
            // 
            // lblIznosZaNaplatu
            // 
            this.lblIznosZaNaplatu.AutoSize = true;
            this.lblIznosZaNaplatu.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblIznosZaNaplatu.Location = new System.Drawing.Point(777, 372);
            this.lblIznosZaNaplatu.Name = "lblIznosZaNaplatu";
            this.lblIznosZaNaplatu.Size = new System.Drawing.Size(48, 20);
            this.lblIznosZaNaplatu.TabIndex = 46;
            this.lblIznosZaNaplatu.Text = "sapdv";
            // 
            // lblPdv
            // 
            this.lblPdv.AutoSize = true;
            this.lblPdv.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblPdv.Location = new System.Drawing.Point(777, 342);
            this.lblPdv.Name = "lblPdv";
            this.lblPdv.Size = new System.Drawing.Size(40, 20);
            this.lblPdv.TabIndex = 45;
            this.lblPdv.Text = "PDV:";
            // 
            // lblBezPdv
            // 
            this.lblBezPdv.AutoSize = true;
            this.lblBezPdv.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblBezPdv.Location = new System.Drawing.Point(777, 311);
            this.lblBezPdv.Name = "lblBezPdv";
            this.lblBezPdv.Size = new System.Drawing.Size(62, 20);
            this.lblBezPdv.TabIndex = 44;
            this.lblBezPdv.Tag = "bez pdv";
            this.lblBezPdv.Text = "bez pdv";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.Location = new System.Drawing.Point(571, 372);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(158, 20);
            this.metroLabel7.TabIndex = 43;
            this.metroLabel7.Text = "Iznos za naplatu s pdv:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.Location = new System.Drawing.Point(669, 342);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(78, 20);
            this.metroLabel6.TabIndex = 42;
            this.metroLabel6.Text = "PDV(17%):";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.Location = new System.Drawing.Point(635, 311);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(103, 20);
            this.metroLabel5.TabIndex = 41;
            this.metroLabel5.Text = "Iznos bez pdv:";
            // 
            // metroButton1
            // 
            this.metroButton1.BackColor = System.Drawing.SystemColors.Highlight;
            this.metroButton1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.metroButton1.Location = new System.Drawing.Point(763, 421);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(178, 47);
            this.metroButton1.TabIndex = 40;
            this.metroButton1.Text = "Zaključi";
            this.metroButton1.UseCustomBackColor = true;
            this.metroButton1.UseCustomForeColor = true;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_ClickAsync);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // lblPrazno
            // 
            this.lblPrazno.AutoSize = true;
            this.lblPrazno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrazno.ForeColor = System.Drawing.Color.Red;
            this.lblPrazno.Location = new System.Drawing.Point(178, 169);
            this.lblPrazno.Name = "lblPrazno";
            this.lblPrazno.Size = new System.Drawing.Size(64, 25);
            this.lblPrazno.TabIndex = 48;
            this.lblPrazno.Text = "label1";
            // 
            // DodajPredracun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(1017, 804);
            this.Controls.Add(this.btnIzradi);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.cbxKupac);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.Datum);
            this.Controls.Add(this.cbxUsluga);
            this.Controls.Add(this.metroPanel2);
            this.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.MaximizeBox = false;
            this.Name = "DodajPredracun";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Novi predračun";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.DodajPredracun_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPredracunStavke)).EndInit();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cbxUsluga;
        private MetroFramework.Controls.MetroLabel Datum;
        private MetroFramework.Controls.MetroDateTime dtpOd;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroDateTime dtpDo;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroComboBox cbxKupac;
        private MetroFramework.Controls.MetroButton btnIzradi;
        private MetroFramework.Controls.MetroGrid dgPredracunStavke;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel lblIznosZaNaplatu;
        private MetroFramework.Controls.MetroLabel lblPdv;
        private MetroFramework.Controls.MetroLabel lblBezPdv;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn UslugaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjereId;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kolicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn CijenaBezPdv;
        private System.Windows.Forms.DataGridViewTextBoxColumn IznosBezPdv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pdv;
        private System.Windows.Forms.DataGridViewTextBoxColumn PdvIznos;
        private System.Windows.Forms.DataGridViewTextBoxColumn CijenaSaPdv;
        private System.Windows.Forms.DataGridViewTextBoxColumn IznosSaPDV;
        private MetroFramework.Controls.MetroButton btnPrintaj;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblPrazno;
    }
}