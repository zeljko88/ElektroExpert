﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Predracuni
{
    public partial class DodajPredracun : MetroFramework.Forms.MetroForm
    {

        WebApiHelper uslugaService = new WebApiHelper("http://localhost:63632/", "api/VrstaUsluge");
        WebApiHelper kupacService = new WebApiHelper("http://localhost:63632/", "api/Kupac");
        WebApiHelper predracunService = new WebApiHelper("http://localhost:63632/", "api/Predracun");

        Predracun predracun { get; set; }
        private decimal sum = 0;
        private decimal sum2 = 0;
        private decimal sum3 = 0;
        public DodajPredracun()
        {
            InitializeComponent();
            dtpOd.Value = DateTime.Today.AddDays(-7);
            dgPredracunStavke.Visible = false;
            lblBezPdv.Visible = false;
            lblBezPdv.Visible = false;
            lblIznosZaNaplatu.Visible = false;
            btnPrintaj.Enabled = false;

        }

        private void DodajPredracun_Load(object sender, EventArgs e)
        {
            BindVrsteUsluge();
            BindKupci();
        }

        private void BindKupci()
        {
            HttpResponseMessage response = kupacService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Kupac> kupac = response.Content.ReadAsAsync<List<Kupac>>().Result;
                kupac.Insert(0, new Kupac());
                cbxKupac.DataSource = kupac;
                cbxKupac.DisplayMember = "Ime";
                cbxKupac.ValueMember = "Id";
            }
        }

        private void BindVrsteUsluge()
        {      

                HttpResponseMessage response = uslugaService.GetResponse();
                if (response.IsSuccessStatusCode)
                {


                    List<VrstaUsluge> uslugavrsta = response.Content.ReadAsAsync<List<VrstaUsluge>>().Result;
                     uslugavrsta.Insert(0, new VrstaUsluge());
                    cbxUsluga.DataSource = uslugavrsta;
                    cbxUsluga.DisplayMember = "Naziv";
                    cbxUsluga.ValueMember = "Id";

                }
        }

        private void btnIzradi_Click(object sender, EventArgs e)
        {


            if (this.ValidateChildren())
            {
                string datOd = dtpOd.Value.ToString("D");
                string datDo = dtpDo.Value.ToString("D");

                HttpResponseMessage response = uslugaService.GetActionResponse("GetUslugeByDateAndVrsta", Convert.ToString(cbxUsluga.SelectedValue), datOd, datDo);
                if (response.IsSuccessStatusCode)
                {
                    List<PredracunStavkeDTO> stavke = response.Content.ReadAsAsync<List<PredracunStavkeDTO>>().Result;          

                    dgPredracunStavke.DataSource = stavke;
                    dgPredracunStavke.Columns["IznosBezPdv"].DefaultCellStyle.Format = "0.00";
                    dgPredracunStavke.Columns["CijenaSaPdv"].DefaultCellStyle.Format = "0.00";
                    dgPredracunStavke.Columns["CijenaBezPdv"].DefaultCellStyle.Format = "0.00";
                    dgPredracunStavke.Columns["PdvIznos"].DefaultCellStyle.Format = "0.00";
                    dgPredracunStavke.Columns["IznosSaPDV"].DefaultCellStyle.Format = "0.00";

                    for (int i = 0; i < dgPredracunStavke.Rows.Count; ++i)
                    {
                        sum += Convert.ToDecimal(dgPredracunStavke.Rows[i].Cells["IznosBezPdv"].Value);
                        Math.Round(sum, 2);
                    }
                    Math.Round(sum, 2);
                    lblBezPdv.Text = sum.ToString("0.00") + " KM";



                    for (int i = 0; i < dgPredracunStavke.Rows.Count; ++i)
                    {
                        sum2 += Convert.ToDecimal(dgPredracunStavke.Rows[i].Cells["PdvIznos"].Value);
                        Math.Round(sum2, 2);
                    }
                    Math.Round(sum2, 2);
                    lblPdv.Text = sum2.ToString("0.00") + " KM";



                    for (int i = 0; i < dgPredracunStavke.Rows.Count; ++i)
                    {
                        sum3 += Convert.ToDecimal(dgPredracunStavke.Rows[i].Cells["IznosSaPDV"].Value);

                    }


                    Math.Round(sum3, 2);
                    lblIznosZaNaplatu.Text = sum3.ToString("0.00") + " KM";
                    if (stavke.Count == 0)
                    {
                        lblPrazno.Text = "Nema odrađenih naloga za tu uslugu, u datom vremenskom periodu !";
                        btnIzradi.Enabled = true;
                        metroButton1.Enabled = false;
                        lblBezPdv.Visible = true;
                        lblIznosZaNaplatu.Visible = true;
                        lblPdv.Visible = true;
                        metroPanel2.Visible = true;

                    }

                    else
                    {

                        lblPrazno.Visible = false;
                        lblBezPdv.Visible = true;
                        lblIznosZaNaplatu.Visible = true;
                        lblPdv.Visible = true;
                        metroPanel2.Visible = true;
                        dgPredracunStavke.Visible = true;
                        btnIzradi.Enabled = false;
                        metroButton1.Enabled = true;
                    }
                    
                }
            }
        }

        private async void metroButton1_ClickAsync(object sender, EventArgs e)
        {
            string br="";
            int prvibroj = 0;


            HttpResponseMessage responseMessage = predracunService.GetActionResponse("GetPredracunBroj");
            if (responseMessage.IsSuccessStatusCode)
            {
                 br =  responseMessage.Content.ReadAsAsync<string>().Result;
                if (!string.IsNullOrEmpty(br))
                {
                    if (!int.TryParse(br, out prvibroj))
                    {
                        throw new ArgumentException($"Argument is not expected format [{br}]");
                    }
                }

                prvibroj++;
            }      
            


            
            Predracun p = new Predracun();
            p.Broj = prvibroj.ToString()+"/"+ DateTime.Now.Year.ToString().Substring(2); 
            p.DatumOd = dtpOd.Value;
            p.DatumDo = dtpDo.Value;
            p.Zakljucen = true;
            p.IznosBezPdv = sum;
            p.IznosSaPdv = sum3;
            p.VrstaUslugeId = Convert.ToInt32(cbxUsluga.SelectedValue);
            p.KupacId = Convert.ToInt32(cbxKupac.SelectedValue);
            p.Pdv = sum2;
            if (p.PredracunStavke == null)
            {
                //It's null - create it
                p.PredracunStavke = new List<PredracunStavke>();
                for (int i = 0; i < dgPredracunStavke.RowCount; i++)
                {
                    PredracunStavke s = new PredracunStavke();
                    s.Kolicina = Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["Kolicina"].Value);
                    s.CijenaBezPdv = Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["CijenaBezPdv"].Value);
                    s.CijenaSaPdv = Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["CijenaSaPdv"].Value);
                    s.IznosBezPdv= Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["IznosBezPdv"].Value);
                    s.IznosSaPdv= Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["IznosSaPDV"].Value);
                    s.Pdv= Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["Pdv"].Value);
                    s.JedinicaMjereId = Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["JedMjereId"].Value);
                    s.UslugaId= Convert.ToInt32(dgPredracunStavke.Rows[i].Cells["UslugaId"].Value);
                    p.PredracunStavke.Add(s);
                }

                HttpResponseMessage response = predracunService.PostResponse(p);
                if (response.IsSuccessStatusCode)
                {
                    MessageBox.Show(Globall.GetMessage("predracun_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    predracun = response.Content.ReadAsAsync<Predracun>().Result;
                   
                    btnPrintaj.Enabled = true;
                    metroButton1.Enabled = false;

                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }
 


        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            PredracunReport frm = new PredracunReport(predracun,cbxUsluga.Text,cbxKupac.Text);
            frm.ShowDialog();
        }

        private void cbxUsluga_Format(object sender, ListControlConvertEventArgs e)
        {
            string naziv = ((VrstaUsluge)e.ListItem).Naziv;
            string sifra = ((VrstaUsluge)e.ListItem).Sifra;
            e.Value = naziv + "-" + sifra;
        }

        private void cbxUsluga_Validating(object sender, CancelEventArgs e)
        {
            if (cbxUsluga.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxUsluga, Globall.GetMessage("empty"));
            }
            else
            {
                errorProvider1.SetError(cbxUsluga, "");
            }
        }

        private void cbxKupac_Validating(object sender, CancelEventArgs e)
        {
            if (cbxKupac.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxKupac, Globall.GetMessage("empty"));
            }
            else
            {
                errorProvider1.SetError(cbxKupac, "");
            }
        }
    }
}
