﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Predracuni
{
    public partial class ListaPredracuna : MetroFramework.Forms.MetroForm
    {

        WebApiHelper predracunService = new WebApiHelper("http://localhost:63632/", "api/Predracun");
        private List<PredracunDTO> listaPredracuna { get; set; }
        private Predracun predracun { get; set; }
        public ListaPredracuna()
        {
            InitializeComponent();
            dgPredracuni.AutoGenerateColumns = false;
        }

        private void ListaPredracuna_Load(object sender, EventArgs e)
        {
            BindPredracuni();
        }

        private void BindPredracuni()
        {
            HttpResponseMessage response = predracunService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                listaPredracuna = response.Content.ReadAsAsync<List<PredracunDTO>>().Result;
                dgPredracuni.DataSource = listaPredracuna;

            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajPredracun frm = new DodajPredracun();
            frm.ShowDialog();
        }

        private void btnStampaj_Click(object sender, EventArgs e)
        {
            int rowindex = Convert.ToInt32(dgPredracuni.Rows[dgPredracuni.CurrentRow.Index].Cells["Id"].Value);


            string usluga = Convert.ToString(dgPredracuni.Rows[dgPredracuni.CurrentRow.Index].Cells["VrstaUsluge"].Value);
            string kupac = Convert.ToString(dgPredracuni.Rows[dgPredracuni.CurrentRow.Index].Cells["NazivKupca"].Value);

           

            HttpResponseMessage response = predracunService.GetResponse(rowindex);

            predracun = response.Content.ReadAsAsync<Predracun>().Result;

            PredracunReport frm = new PredracunReport(predracun, usluga, kupac);
            frm.ShowDialog();

        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Jeste li sigurni da želite obrisati predračun ?", "Upozorenje !", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (confirmResult == DialogResult.Yes)
            {
                int id = Convert.ToInt32(dgPredracuni.Rows[dgPredracuni.CurrentRow.Index].Cells[0].Value);


                HttpResponseMessage httpResponse = predracunService.GetActionResponse("ObrisiPredracun", id.ToString());
                if (httpResponse.IsSuccessStatusCode)
                {
                    dgPredracuni.DataSource = httpResponse.Content.ReadAsAsync<List<PredracunDTO>>().Result;

                }
            }
            else
            {
                return;
            }

        }
    }
}
