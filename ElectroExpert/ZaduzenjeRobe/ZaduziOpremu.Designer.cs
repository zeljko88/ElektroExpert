﻿namespace ElectroExpert.ZaduzenjeRobe
{
    partial class ZaduziOpremu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgRadnici = new System.Windows.Forms.DataGridView();
            this.VoziloId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SefVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrTelefona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgRadnici)).BeginInit();
            this.SuspendLayout();
            // 
            // dgRadnici
            // 
            this.dgRadnici.AllowUserToAddRows = false;
            this.dgRadnici.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgRadnici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRadnici.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoziloId,
            this.NazivVozila,
            this.SefVozila,
            this.BrTelefona,
            this.Email});
            this.dgRadnici.Location = new System.Drawing.Point(23, 113);
            this.dgRadnici.Name = "dgRadnici";
            this.dgRadnici.RowTemplate.Height = 24;
            this.dgRadnici.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgRadnici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRadnici.Size = new System.Drawing.Size(720, 358);
            this.dgRadnici.TabIndex = 11;
            this.dgRadnici.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRadnici_CellContentDoubleClick);
            // 
            // VoziloId
            // 
            this.VoziloId.DataPropertyName = "VoziloId";
            this.VoziloId.HeaderText = "VoziloId";
            this.VoziloId.Name = "VoziloId";
            this.VoziloId.Visible = false;
            // 
            // NazivVozila
            // 
            this.NazivVozila.DataPropertyName = "NazivVozila";
            this.NazivVozila.HeaderText = "Naziv vozila";
            this.NazivVozila.Name = "NazivVozila";
            this.NazivVozila.ReadOnly = true;
            this.NazivVozila.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // SefVozila
            // 
            this.SefVozila.DataPropertyName = "SefVozila";
            this.SefVozila.HeaderText = "Šef vozila";
            this.SefVozila.Name = "SefVozila";
            this.SefVozila.ReadOnly = true;
            this.SefVozila.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SefVozila.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BrTelefona
            // 
            this.BrTelefona.DataPropertyName = "BrTelefona";
            this.BrTelefona.HeaderText = "Telefon";
            this.BrTelefona.Name = "BrTelefona";
            this.BrTelefona.ReadOnly = true;
            this.BrTelefona.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ZaduziOpremu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 485);
            this.Controls.Add(this.dgRadnici);
            this.MaximizeBox = false;
            this.Name = "ZaduziOpremu";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Odaberi vozilo";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ZaduziOpremu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRadnici)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgRadnici;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoziloId;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn SefVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrTelefona;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
    }
}