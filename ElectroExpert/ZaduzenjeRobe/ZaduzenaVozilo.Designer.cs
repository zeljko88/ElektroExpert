﻿namespace ElectroExpert.ZaduzenjeRobe
{
    partial class ZaduzenaVozilo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgZaduzenaOprema = new System.Windows.Forms.DataGridView();
            this.SifraArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KolicinaZaduzenog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbxVozila = new MetroFramework.Controls.MetroComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.btnPrintaj = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgZaduzenaOprema)).BeginInit();
            this.SuspendLayout();
            // 
            // dgZaduzenaOprema
            // 
            this.dgZaduzenaOprema.AllowUserToAddRows = false;
            this.dgZaduzenaOprema.AllowUserToDeleteRows = false;
            this.dgZaduzenaOprema.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgZaduzenaOprema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgZaduzenaOprema.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SifraArtikla,
            this.NazivArtikla,
            this.KolicinaZaduzenog,
            this.JedMjere});
            this.dgZaduzenaOprema.Location = new System.Drawing.Point(23, 148);
            this.dgZaduzenaOprema.Name = "dgZaduzenaOprema";
            this.dgZaduzenaOprema.ReadOnly = true;
            this.dgZaduzenaOprema.RowTemplate.Height = 24;
            this.dgZaduzenaOprema.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgZaduzenaOprema.Size = new System.Drawing.Size(798, 437);
            this.dgZaduzenaOprema.TabIndex = 0;
            // 
            // SifraArtikla
            // 
            this.SifraArtikla.DataPropertyName = "SifraArtikla";
            this.SifraArtikla.HeaderText = "Šifra ";
            this.SifraArtikla.Name = "SifraArtikla";
            this.SifraArtikla.ReadOnly = true;
            // 
            // NazivArtikla
            // 
            this.NazivArtikla.DataPropertyName = "NazivArtikla";
            this.NazivArtikla.HeaderText = "Naziv";
            this.NazivArtikla.Name = "NazivArtikla";
            this.NazivArtikla.ReadOnly = true;
            // 
            // KolicinaZaduzenog
            // 
            this.KolicinaZaduzenog.DataPropertyName = "KolicinaZaduzenog";
            this.KolicinaZaduzenog.HeaderText = "Količina zaduženog";
            this.KolicinaZaduzenog.Name = "KolicinaZaduzenog";
            this.KolicinaZaduzenog.ReadOnly = true;
            // 
            // JedMjere
            // 
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jed. mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            // 
            // cbxVozila
            // 
            this.cbxVozila.FormattingEnabled = true;
            this.cbxVozila.ItemHeight = 24;
            this.cbxVozila.Location = new System.Drawing.Point(27, 99);
            this.cbxVozila.Name = "cbxVozila";
            this.cbxVozila.Size = new System.Drawing.Size(197, 30);
            this.cbxVozila.TabIndex = 32;
            this.cbxVozila.UseSelectable = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 33;
            this.label2.Text = "Vozilo:";
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPrikazi.Location = new System.Drawing.Point(675, 99);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(146, 39);
            this.btnPrikazi.TabIndex = 35;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // btnPrintaj
            // 
            this.btnPrintaj.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrintaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintaj.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPrintaj.Location = new System.Drawing.Point(675, 591);
            this.btnPrintaj.Name = "btnPrintaj";
            this.btnPrintaj.Size = new System.Drawing.Size(146, 39);
            this.btnPrintaj.TabIndex = 36;
            this.btnPrintaj.Text = "Printaj";
            this.btnPrintaj.UseVisualStyleBackColor = false;
            this.btnPrintaj.Click += new System.EventHandler(this.btnPrintaj_Click);
            // 
            // ZaduzenaVozilo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 646);
            this.Controls.Add(this.btnPrintaj);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxVozila);
            this.Controls.Add(this.dgZaduzenaOprema);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "ZaduzenaVozilo";
            this.Resizable = false;
            this.Text = "Zadužena oprema po vozilu";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ZaduzenaVozilo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgZaduzenaOprema)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgZaduzenaOprema;
        private MetroFramework.Controls.MetroComboBox cbxVozila;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.Button btnPrintaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn SifraArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn KolicinaZaduzenog;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
    }
}