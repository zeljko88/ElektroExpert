﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.ZaduzenjeRobe
{
    public partial class ZaduziOpremu : MetroFramework.Forms.MetroForm
    {

        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        private List<VoziloDTO> vozila { get; set; }
        public ZaduziOpremu()
        {
            InitializeComponent();
            dgRadnici.AutoGenerateColumns = false;
        }


        private void ZaduziOpremu_Load(object sender, EventArgs e)
        {
            BindGrid();
        }


        private void BindGrid()
        {

            HttpResponseMessage response = radniciService.GetActionResponse("GetRadniciAndVozilo");

            if (response.IsSuccessStatusCode)
            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                dgRadnici.DataSource = vozila;
            }

            else
            {
                MessageBox.Show("Greska - Error Code:" + response.StatusCode + "-Message:" + response.ReasonPhrase);
            }
        }

        private void dgRadnici_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DodajOpremu dodajOpremuForm = new DodajOpremu(vozila[e.RowIndex]);
            dodajOpremuForm.ShowDialog();
        }
    }
}
