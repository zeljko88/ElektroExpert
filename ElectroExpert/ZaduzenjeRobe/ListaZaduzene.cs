﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.ZaduzenjeRobe
{
    public partial class ListaZaduzene : MetroFramework.Forms.MetroForm
    {


        WebApiHelper zaduzenaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOpremaStavke");

        public NalogDTO nalog { get; set; }
        public UgradjenaOpremaDTO Ugradjena { get; set; }
        public List<UgradjenaOpremaDTO> vecUgradjena { get; set; }
        public ListaZaduzene(NalogDTO nalog)
        {
            InitializeComponent();
            this.nalog = nalog;
            this.vecUgradjena = vecUgradjena;

        }

        public ListaZaduzene(NalogDTO nalog, List<UgradjenaOpremaDTO> vecUgradjena)
        {
            InitializeComponent();
            this.nalog = nalog;
            this.vecUgradjena = vecUgradjena;

        }

        private void ListaZaduzene_Load(object sender, EventArgs e)
        {
            BindZaduzene();
        }

        private void BindZaduzene()
        {
            HttpResponseMessage response = zaduzenaService.GetActionResponse("GetZaduzenaOpremaByVozilo",Convert.ToString(nalog.VoziloId));
            if (response.IsSuccessStatusCode)
            {

                dgListaZaduzene.DataSource = response.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;

            }
        }

        private void btnDodajZaduzene_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            
            {
                Ugradjena.ArtikalId = Convert.ToInt32(dgListaZaduzene.CurrentRow.Cells["ArtikalId"].Value);
                Ugradjena.SifraArtikla = Convert.ToString(dgListaZaduzene.CurrentRow.Cells["SifraArtikla"].Value);

                Ugradjena.NazivArtikla = Convert.ToString(dgListaZaduzene.CurrentRow.Cells["NazivArtikla"].Value);

                Ugradjena.KolicinaUgradjenog = Convert.ToInt32(txtKolicina.Text);
                Ugradjena.JedinicaMjere = Convert.ToString(dgListaZaduzene.CurrentRow.Cells["JedMjere"].Value);
            }
        }

        private void txtKolicina_Validating(object sender, CancelEventArgs e)
        {
            if (vecUgradjena != null)
            {
                foreach (UgradjenaOpremaDTO item in vecUgradjena)
                {
                    if (item.ArtikalId == Convert.ToInt32(dgListaZaduzene.CurrentRow.Cells["ArtikalId"].Value))
                    {
                        if (item.KolicinaUgradjenog + Convert.ToInt32(txtKolicina.Text) > Convert.ToInt32(dgListaZaduzene.CurrentRow.Cells["KolicinaZaduzenog"].Value))
                        {
                            e.Cancel = true;
                            errorProvider1.SetError(dgListaZaduzene, Globall.GetMessage("nema_dovoljno"));
                        }
                    }
                }
            }


            if (string.IsNullOrWhiteSpace(txtKolicina.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("kol_req"));

            }

            else if (Convert.ToInt32(txtKolicina.Text) > Convert.ToInt32(dgListaZaduzene.Rows[dgListaZaduzene.CurrentRow.Index].Cells["KolicinaZaduzenog"].Value))
            {
                e.Cancel = true;
                errorProvider1.SetError(dgListaZaduzene, Globall.GetMessage("nema_dovoljno"));

            }

            else
            {
                errorProvider1.SetError(txtKolicina, "");
                errorProvider1.SetError(dgListaZaduzene, "");
                
            }
        }

        private void txtKolicina_TextChanged(object sender, EventArgs e)
        {

            if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtKolicina, "");
            }
        }

        private void txtSifra_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtKolicina, "");
            }
        }

        private void btnDodajZaduzene_Click_1(object sender, EventArgs e)
        {
            if (this.ValidateChildren())

            {
                Ugradjena.ArtikalId = Convert.ToInt32(dgListaZaduzene.CurrentRow.Cells["ArtikalId"].Value);
                Ugradjena.SifraArtikla = Convert.ToString(dgListaZaduzene.CurrentRow.Cells["SifraArtikla"].Value);

                Ugradjena.NazivArtikla = Convert.ToString(dgListaZaduzene.CurrentRow.Cells["NazivArtikla"].Value);

                Ugradjena.KolicinaUgradjenog = Convert.ToInt32(txtKolicina.Text);
                Ugradjena.JedinicaMjere = Convert.ToString(dgListaZaduzene.CurrentRow.Cells["JedMjere"].Value);
            }
        }
    }
}
