﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.ZaduzenjeRobe
{
    public partial class DodajOpremu : MetroFramework.Forms.MetroForm
    {
        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        WebApiHelper ulazStavkeService = new WebApiHelper("http://localhost:63632/", "api/UlazStavke");
        WebApiHelper stanjeService = new WebApiHelper("http://localhost:63632/", "api/StanjeSkladista");
        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");
        WebApiHelper zaduzenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOprema");
        WebApiHelper ulazService = new WebApiHelper("http://localhost:63632/", "api/Ulaz");
        WebApiHelper zaduzenaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaUlaz");
        private int noviUlazId;

        public List<ZaduzenaOprema> novaLista = new List<ZaduzenaOprema>();
        private DateTime datumzaduzenja;

        private VoziloDTO vozilo { get; set; }
        public DodajOpremu(VoziloDTO vozilo)
        {
            InitializeComponent();
            dgOpremaSkladiste.AutoGenerateColumns = false;
            dgZaduzenaOprema.AutoGenerateColumns = false;
            if (vozilo != null)
                this.vozilo = vozilo;
            
            btnZakljuci.Enabled = false;
            btnPrintaj.Enabled = false;
            dgOpremaSkladiste.ClearSelection();
        }

        private void DodajOpremu_Load(object sender, EventArgs e)
        {
            BindForm();
        }

        private void BindForm()
        {
            lblVozilo.Text = vozilo.NazivVozila;
            lblSefVozila.Text = vozilo.SefVozila;

            HttpResponseMessage response = stanjeService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                dgOpremaSkladiste.DataSource = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

            }          
       
        }    

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                int kol;
                foreach (DataGridViewRow row in dgZaduzenaOprema.Rows)
                {
                    if (Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[0].Value) == Convert.ToInt32(row.Cells["IdArtikla"].Value))
                    {
                        kol = Convert.ToInt32(row.Cells["KolicinaZaduzenog"].Value);
                        kol += Convert.ToInt32(txtKolicina.Text);
                        row.Cells["KolicinaZaduzenog"].Value = kol;
                        txtKolicina.Text = "";
                        return;
                    }

                }
                int n = dgZaduzenaOprema.Rows.Add();
                dgZaduzenaOprema.Rows[n].Cells[0].Value = Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[0].Value);
                dgZaduzenaOprema.Rows[n].Cells[1].Value = Convert.ToString(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[1].Value);
                dgZaduzenaOprema.Rows[n].Cells[2].Value = Convert.ToString(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[2].Value);
                dgZaduzenaOprema.Rows[n].Cells[3].Value = Convert.ToString(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[3].Value);
                dgZaduzenaOprema.Rows[n].Cells[4].Value = Convert.ToInt32(txtKolicina.Text);
                txtKolicina.Text = "";
                btnZakljuci.Enabled = true;
            }

        }


        private void ClearInput()
        {
            txtKolicina.Text = txtSifra.Text = "";
           
        }   

        private void btnZakljuci_Click(object sender, EventArgs e)
        {         

        int id = Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value);

            ZaduzenaUlaz u = new ZaduzenaUlaz();
            u.KorisnikId = Globall.prijavljeniKorisnik.Id;
            u.VoziloId = vozilo.VoziloId;
            u.Datum = DateTime.Now;
            datumzaduzenja = DateTime.Now;
            u.SaVozila = 0;

                if (u.ZaduzenaUlazStavke == null)
                {               
                    u.ZaduzenaUlazStavke = new List<ZaduzenaUlazStavke>();
                    for (int i = 0; i < dgZaduzenaOprema.RowCount; i++)
                    {
                        ZaduzenaUlazStavke s = new ZaduzenaUlazStavke();
                        s.Kolicina = Convert.ToInt32(dgZaduzenaOprema.Rows[i].Cells[4].Value);
                        s.ArtikalId = Convert.ToInt32(dgZaduzenaOprema.Rows[i].Cells[0].Value);

                        u.ZaduzenaUlazStavke.Add(s);
                    }


                    HttpResponseMessage response = zaduzenaService.PostResponse(u);
                    if (response.IsSuccessStatusCode)
                    {
                        ZaduzenaUlaz zaduzena = response.Content.ReadAsAsync<ZaduzenaUlaz>().Result;
                        noviUlazId = zaduzena.Id;
                    
                        MessageBox.Show(Globall.GetMessage("zo_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ClearInput();
                        btnZakljuci.Enabled = false;
                        btnDodaj.Enabled = false;
                        btnPrintaj.Enabled = true;
                        txtKolicina.Enabled = false;
                        BindForm();
                    }
                    else
                    {
                        MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }               

            }


            //for (int i = 0; i < dgZaduzenaOprema.RowCount; i++)
            //{
            //    ZaduzenaOprema s = new ZaduzenaOprema();
            //    s.Kolicina = Convert.ToInt32(dgZaduzenaOprema.Rows[i].Cells[4].Value);
            //    s.ArtikalId = Convert.ToInt32(dgZaduzenaOprema.Rows[i].Cells[0].Value);
            //    s.VoziloId = vozilo.VoziloId;
            //    novaLista.Add(s);
            //}


            //HttpResponseMessage response = ul.PostResponse(novaLista);

            //if (response.IsSuccessStatusCode)
            //{
            //    List<ZaduzenaOprema> zaduzena = response.Content.ReadAsAsync<List<ZaduzenaOprema>>().Result;
            //    noviUlazId = zaduzena[0].Id;


            //    MessageBox.Show(Globall.GetMessage("zo_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    ClearInput();
            //    btnZakljuci.Enabled = false;
            //    btnDodaj.Enabled = false;
            //    btnPrintaj.Enabled = true;
            //    txtKolicina.Enabled = false;
            //    BindForm();
            //}
            //else
            //{
            //    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}


        }
    

        private void txtKolicina_Validating(object sender, CancelEventArgs e)
    {
            if (string.IsNullOrWhiteSpace(txtKolicina.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("kol_req"));

            }

           else if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*[1-9][0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else if (Convert.ToInt32(txtKolicina.Text) > Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[4].Value))
            {
                e.Cancel = true;
                errorProvider1.SetError(dgOpremaSkladiste, Globall.GetMessage("nema_dovoljno"));

            }

            else
            {
                errorProvider1.SetError(txtKolicina, "");
                errorProvider1.SetError(dgOpremaSkladiste, "");

            }
        }     
    

         private void btnIzbrisi_Click(object sender, EventArgs e)
    {
        int row = dgZaduzenaOprema.CurrentCell.RowIndex;
        dgZaduzenaOprema.Rows.RemoveAt(row);
    }

        private void btnPrintaj_Click(object sender, EventArgs e)
    {
        SkladisteVozilo frm = new SkladisteVozilo(noviUlazId, vozilo.NazivVozila, datumzaduzenja);
        frm.ShowDialog();
    }

        private void txtSifra_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtSifra, "");
            }



            TextBox ak = sender as TextBox;

            string ar = ak.Text as string;

            if (ak.Text == "")
            {
                HttpResponseMessage responsedva = stanjeService.GetResponse();
                if (responsedva.IsSuccessStatusCode)
                {
                    dgOpremaSkladiste.DataSource = responsedva.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

                }

            }

            List<StanjeSkladistaDTO> artikli = new List<StanjeSkladistaDTO>();
            List<StanjeSkladistaDTO> listaArtikala = new List<StanjeSkladistaDTO>();

            HttpResponseMessage response = stanjeService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                artikli = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

            }

            foreach (StanjeSkladistaDTO item in artikli)
            {
                if (Regex.IsMatch(item.Sifra, ar, RegexOptions.IgnoreCase))
                {
                    listaArtikala.Add(item);
                }


            }

            dgOpremaSkladiste.DataSource = listaArtikala;
        }

        private void dgOpremaSkladiste_Validating(object sender, CancelEventArgs e)
        {
            int kol;
            if (txtKolicina.Text == "")
            {
                kol = 0;
            }
            else
            {
                kol = Convert.ToInt32(txtKolicina.Text);
            }


            foreach (DataGridViewRow row in dgZaduzenaOprema.Rows)
            {
               
                
                    if (Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[0].Value) == Convert.ToInt32(row.Cells["IdArtikla"].Value) &&
                    Convert.ToInt32(dgOpremaSkladiste.Rows[dgOpremaSkladiste.CurrentRow.Index].Cells[4].Value) < Convert.ToInt32(row.Cells["KolicinaZaduzenog"].Value) + kol)
                    {
                        e.Cancel = true;
                        errorProvider1.SetError(dgOpremaSkladiste, Globall.GetMessage("nema_dovoljno"));
                       
                    }


                
                else
                {
                    errorProvider1.SetError(dgOpremaSkladiste, "");
                }
            }
        }

        private void txtKolicina_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*$"))
            {

                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtKolicina, "");
            }
        }

   
    }
    
    }
