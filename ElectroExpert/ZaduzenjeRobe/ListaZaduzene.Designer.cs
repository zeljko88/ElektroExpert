﻿namespace ElectroExpert.ZaduzenjeRobe
{
    partial class ListaZaduzene
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgListaZaduzene = new System.Windows.Forms.DataGridView();
            this.ArtikalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SifraArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KolicinaZaduzenog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSifra = new System.Windows.Forms.TextBox();
            this.btnDodajZaduzene = new System.Windows.Forms.Button();
            this.txtKolicina = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaZaduzene)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgListaZaduzene
            // 
            this.dgListaZaduzene.AllowUserToAddRows = false;
            this.dgListaZaduzene.AllowUserToDeleteRows = false;
            this.dgListaZaduzene.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgListaZaduzene.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgListaZaduzene.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ArtikalId,
            this.SifraArtikla,
            this.NazivArtikla,
            this.JedMjere,
            this.KolicinaZaduzenog});
            this.dgListaZaduzene.Location = new System.Drawing.Point(21, 119);
            this.dgListaZaduzene.MultiSelect = false;
            this.dgListaZaduzene.Name = "dgListaZaduzene";
            this.dgListaZaduzene.ReadOnly = true;
            this.dgListaZaduzene.RowTemplate.Height = 24;
            this.dgListaZaduzene.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgListaZaduzene.Size = new System.Drawing.Size(772, 428);
            this.dgListaZaduzene.TabIndex = 0;
            // 
            // ArtikalId
            // 
            this.ArtikalId.DataPropertyName = "ArtikalId";
            this.ArtikalId.HeaderText = "ArtikalId";
            this.ArtikalId.Name = "ArtikalId";
            this.ArtikalId.ReadOnly = true;
            this.ArtikalId.Visible = false;
            // 
            // SifraArtikla
            // 
            this.SifraArtikla.DataPropertyName = "SifraArtikla";
            this.SifraArtikla.HeaderText = "Šifra";
            this.SifraArtikla.Name = "SifraArtikla";
            this.SifraArtikla.ReadOnly = true;
            // 
            // NazivArtikla
            // 
            this.NazivArtikla.DataPropertyName = "NazivArtikla";
            this.NazivArtikla.HeaderText = "Naziv";
            this.NazivArtikla.Name = "NazivArtikla";
            this.NazivArtikla.ReadOnly = true;
            // 
            // JedMjere
            // 
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jedinica mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            // 
            // KolicinaZaduzenog
            // 
            this.KolicinaZaduzenog.DataPropertyName = "KolicinaZaduzenog";
            this.KolicinaZaduzenog.HeaderText = "Količina zaduženog";
            this.KolicinaZaduzenog.Name = "KolicinaZaduzenog";
            this.KolicinaZaduzenog.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Šifra:";
            // 
            // txtSifra
            // 
            this.txtSifra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSifra.Location = new System.Drawing.Point(23, 72);
            this.txtSifra.Name = "txtSifra";
            this.txtSifra.Size = new System.Drawing.Size(148, 30);
            this.txtSifra.TabIndex = 18;
            this.txtSifra.TextChanged += new System.EventHandler(this.txtSifra_TextChanged);
            // 
            // btnDodajZaduzene
            // 
            this.btnDodajZaduzene.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDodajZaduzene.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnDodajZaduzene.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDodajZaduzene.Location = new System.Drawing.Point(657, 62);
            this.btnDodajZaduzene.Name = "btnDodajZaduzene";
            this.btnDodajZaduzene.Size = new System.Drawing.Size(136, 40);
            this.btnDodajZaduzene.TabIndex = 22;
            this.btnDodajZaduzene.Text = "&Dodaj";
            this.btnDodajZaduzene.UseVisualStyleBackColor = false;
            this.btnDodajZaduzene.Click += new System.EventHandler(this.btnDodajZaduzene_Click_1);
            // 
            // txtKolicina
            // 
            this.txtKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKolicina.Location = new System.Drawing.Point(483, 67);
            this.txtKolicina.Name = "txtKolicina";
            this.txtKolicina.Size = new System.Drawing.Size(115, 30);
            this.txtKolicina.TabIndex = 18;
            this.txtKolicina.TextChanged += new System.EventHandler(this.txtKolicina_TextChanged);
            this.txtKolicina.Validating += new System.ComponentModel.CancelEventHandler(this.txtKolicina_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(479, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 23;
            this.label3.Text = "Količina:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ListaZaduzene
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(844, 561);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnDodajZaduzene);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKolicina);
            this.Controls.Add(this.txtSifra);
            this.Controls.Add(this.dgListaZaduzene);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "ListaZaduzene";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.ListaZaduzene_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaZaduzene)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgListaZaduzene;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSifra;
        private System.Windows.Forms.Button btnDodajZaduzene;
        private System.Windows.Forms.TextBox txtKolicina;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArtikalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SifraArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn KolicinaZaduzenog;
    }
}