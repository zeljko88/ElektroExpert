﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.ZaduzenjeRobe
{
    public partial class ZaduzenaVozilo : MetroFramework.Forms.MetroForm
    {
        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        WebApiHelper zaduzenaStavkeService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOpremaStavke");
        public ZaduzenaVozilo()
        {
            InitializeComponent();
            dgZaduzenaOprema.AutoGenerateColumns = false;
        }

        private void ZaduzenaVozilo_Load(object sender, EventArgs e)
        {
            HttpResponseMessage response = voziloService.GetResponse();
            if (response.IsSuccessStatusCode)
            {


                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                vozila.Insert(0, new Vozilo());
                cbxVozila.DataSource = vozila;
                cbxVozila.DisplayMember = "Naziv";
                cbxVozila.ValueMember = "Id";

            }
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            HttpResponseMessage zaduzenaresponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(cbxVozila.SelectedValue));
            if (zaduzenaresponse.IsSuccessStatusCode)
            {
                dgZaduzenaOprema.DataSource = zaduzenaresponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
                dgZaduzenaOprema.ClearSelection();
            }
        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            ZaduzenaVoziloReport frm = new ZaduzenaVoziloReport(Convert.ToInt32(cbxVozila.SelectedValue),cbxVozila.Text);
            frm.ShowDialog();
        }
    }
}
