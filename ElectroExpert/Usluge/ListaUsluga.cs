﻿using ElectroExpert.Nalozi;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Usluge
{
   
    public partial class ListaUsluga : MetroFramework.Forms.MetroForm
    {

        WebApiHelper uslugaService = new WebApiHelper("http://localhost:63632/", "api/Usluga");
       
       
        public UslugaDTO Usluga { get; set; }
        public ListaUsluga()
        {
            InitializeComponent();
            dgUsluge.AutoGenerateColumns = false;
           
        }

        private void ListaUsluga_Load(object sender, EventArgs e)
        {
            BindUsluge();
        }

        private void BindUsluge()
        {
            HttpResponseMessage response = uslugaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {

                dgUsluge.DataSource = response.Content.ReadAsAsync<List<UslugaDTO>>().Result;

            }
        }

        private void btnDodajUslugu_Click(object sender, EventArgs e)
        {

          
            
            Usluga.Id = Convert.ToInt32(dgUsluge.CurrentRow.Cells["Id"].Value);
            Usluga.VrstaUsluge =Convert.ToString(dgUsluge.CurrentRow.Cells["VrstaUsluge"].Value);
            Usluga.VrstaUslugeId = Convert.ToInt32(dgUsluge.CurrentRow.Cells["VrstaUslugeId"].Value);
            Usluga.JedMjereId = Convert.ToInt32(dgUsluge.CurrentRow.Cells["JedMjereId"].Value);
            Usluga.SifraUsluge = Convert.ToInt32(dgUsluge.CurrentRow.Cells["SifraUsluge"].Value);
            Usluga.NazivUsluge = Convert.ToString(dgUsluge.CurrentRow.Cells["NazivUsluge"].Value);
            Usluga.JedMjere = Convert.ToString(dgUsluge.CurrentRow.Cells["JedMjere"].Value);
            Usluga.Cijena = Convert.ToInt32(dgUsluge.CurrentRow.Cells["Cijena"].Value);
        }

        











        //DetaljiNaloga frm = new DetaljiNaloga(Nalog);
        //int n = frm.dgUsluge.Rows.Add();

        //frm.dgUsluge.Rows[n].Cells[0].Value = Convert.ToInt32(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[0].Value);
        //frm.dgUsluge.Rows[n].Cells[1].Value = Convert.ToString(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[1].Value);
        //frm.dgUsluge.Rows[n].Cells[2].Value = Convert.ToString(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[2].Value);
        //frm.dgUsluge.Rows[n].Cells[3].Value = Convert.ToString(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[3].Value);
        //frm.dgUsluge.Rows[n].Cells[4].Value = Convert.ToString(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[4].Value);
        //frm.dgUsluge.Rows[n].Cells[5].Value = Convert.ToString(dgUsluge.Rows[dgUsluge.CurrentRow.Index].Cells[5].Value);

        //frm.Refresh();
        //this.Close();
    }
    
}
