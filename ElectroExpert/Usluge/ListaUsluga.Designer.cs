﻿namespace ElectroExpert.Usluge
{
    partial class ListaUsluga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgUsluge = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VrstaUslugeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjereId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VrstaUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SifraUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivUsluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cijena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSifra = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDodajUslugu = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsluge)).BeginInit();
            this.SuspendLayout();
            // 
            // dgUsluge
            // 
            this.dgUsluge.AllowUserToAddRows = false;
            this.dgUsluge.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUsluge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsluge.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.VrstaUslugeId,
            this.JedMjereId,
            this.VrstaUsluge,
            this.SifraUsluge,
            this.NazivUsluge,
            this.JedMjere,
            this.Cijena});
            this.dgUsluge.Location = new System.Drawing.Point(12, 163);
            this.dgUsluge.Name = "dgUsluge";
            this.dgUsluge.ReadOnly = true;
            this.dgUsluge.RowTemplate.Height = 24;
            this.dgUsluge.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUsluge.Size = new System.Drawing.Size(775, 379);
            this.dgUsluge.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "UslugaId";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // VrstaUslugeId
            // 
            this.VrstaUslugeId.DataPropertyName = "VrstaUslugeId";
            this.VrstaUslugeId.HeaderText = "VrstaUslugeId";
            this.VrstaUslugeId.Name = "VrstaUslugeId";
            this.VrstaUslugeId.ReadOnly = true;
            this.VrstaUslugeId.Visible = false;
            // 
            // JedMjereId
            // 
            this.JedMjereId.DataPropertyName = "JedMjereId";
            this.JedMjereId.HeaderText = "JedMjereId";
            this.JedMjereId.Name = "JedMjereId";
            this.JedMjereId.ReadOnly = true;
            this.JedMjereId.Visible = false;
            // 
            // VrstaUsluge
            // 
            this.VrstaUsluge.DataPropertyName = "VrstaUsluge";
            this.VrstaUsluge.HeaderText = "Vrsta usluge";
            this.VrstaUsluge.Name = "VrstaUsluge";
            this.VrstaUsluge.ReadOnly = true;
            // 
            // SifraUsluge
            // 
            this.SifraUsluge.DataPropertyName = "SifraUsluge";
            this.SifraUsluge.HeaderText = "Sifra";
            this.SifraUsluge.Name = "SifraUsluge";
            this.SifraUsluge.ReadOnly = true;
            // 
            // NazivUsluge
            // 
            this.NazivUsluge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NazivUsluge.DataPropertyName = "NazivUsluge";
            this.NazivUsluge.HeaderText = "Naziv";
            this.NazivUsluge.Name = "NazivUsluge";
            this.NazivUsluge.ReadOnly = true;
            this.NazivUsluge.Width = 72;
            // 
            // JedMjere
            // 
            this.JedMjere.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jedinica mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            this.JedMjere.Width = 118;
            // 
            // Cijena
            // 
            this.Cijena.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Cijena.DataPropertyName = "Cijena";
            this.Cijena.HeaderText = "Cijena";
            this.Cijena.Name = "Cijena";
            this.Cijena.ReadOnly = true;
            this.Cijena.Width = 76;
            // 
            // txtSifra
            // 
            this.txtSifra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSifra.Location = new System.Drawing.Point(27, 98);
            this.txtSifra.Name = "txtSifra";
            this.txtSifra.Size = new System.Drawing.Size(148, 30);
            this.txtSifra.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Šifra:";
            // 
            // btnDodajUslugu
            // 
            this.btnDodajUslugu.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDodajUslugu.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnDodajUslugu.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDodajUslugu.Location = new System.Drawing.Point(608, 72);
            this.btnDodajUslugu.Name = "btnDodajUslugu";
            this.btnDodajUslugu.Size = new System.Drawing.Size(136, 40);
            this.btnDodajUslugu.TabIndex = 21;
            this.btnDodajUslugu.Text = "&Dodaj";
            this.btnDodajUslugu.UseVisualStyleBackColor = false;
            this.btnDodajUslugu.Click += new System.EventHandler(this.btnDodajUslugu_Click);
            // 
            // ListaUsluga
            // 
            this.AcceptButton = this.btnDodajUslugu;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(795, 554);
            this.Controls.Add(this.btnDodajUslugu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSifra);
            this.Controls.Add(this.dgUsluge);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "ListaUsluga";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lista usluga";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.ListaUsluga_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsluge)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgUsluge;
        private System.Windows.Forms.TextBox txtSifra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDodajUslugu;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn VrstaUslugeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjereId;
        private System.Windows.Forms.DataGridViewTextBoxColumn VrstaUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn SifraUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivUsluge;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cijena;
    }
}