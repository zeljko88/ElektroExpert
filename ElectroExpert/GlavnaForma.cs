﻿using ElectroExpert.Artikli;
using ElectroExpert.Izvjestaji;
using ElectroExpert.Korisnici;
using ElectroExpert.Menu;
using ElectroExpert.Nalozi;
using ElectroExpert.Predracuni;
using ElectroExpert.Radnici;
using ElectroExpert.Skladiste;
using ElectroExpert.Vozila;
using ElectroExpert.ZaduzenjeRobe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert
{
    public partial class GlavnaForma : MetroFramework.Forms.MetroForm
    {
        public GlavnaForma()
        {
            InitializeComponent();
        }

        private void btnPrijemRobe_Click(object sender, EventArgs e)
        {
            DodajArtikal frm = new DodajArtikal();
            frm.ShowDialog();

        }

        private void btnRadnici_Click(object sender, EventArgs e)
        {
            ListaRadnika frm = new ListaRadnika();
            frm.ShowDialog();
        }

        private void btnZaduzenjeRobe_Click(object sender, EventArgs e)
        {
            IzdavanjeRobe_Menu frm = new IzdavanjeRobe_Menu();
            frm.ShowDialog();
        }

        private void btnSkladiste_Click(object sender, EventArgs e)
        {
            PrijemRobe frm = new PrijemRobe();
            frm.ShowDialog();
        }

        private void btnNalozi_Click(object sender, EventArgs e)
        {
            ListaNaloga frm = new ListaNaloga();
            frm.ShowDialog();
        }

   

        private void btnSkladiste_Click_1(object sender, EventArgs e)
        {
            ListaArtikala frm = new ListaArtikala();
            frm.ShowDialog();
        }

        private void mtArtikli_Click(object sender, EventArgs e)
        {
            ArtikliBaza frm = new ArtikliBaza();
            frm.ShowDialog();
        }

        private void mtPrijemRobe_Click(object sender, EventArgs e)
        {
            PrijemRobe frm = new PrijemRobe();
            frm.ShowDialog();
        }

        private void mtRadnici_Click(object sender, EventArgs e)
        {
            ListaRadnika frm = new ListaRadnika();
            frm.ShowDialog();
        }

        private void mtFakture_Click(object sender, EventArgs e)
        {

        }

 

        private void mtNalozi_Click(object sender, EventArgs e)
        {
            ListaNaloga frm = new ListaNaloga();
            frm.ShowDialog();
        }

        private void mtPredracuni_Click(object sender, EventArgs e)
        {
            ListaPredracuna frm = new ListaPredracuna();
            frm.ShowDialog();
        }

        private void mtSkladiste_Click(object sender, EventArgs e)
        {
            ListaArtikala frm = new ListaArtikala();
            frm.ShowDialog();
        }

        private void mtPrijenosRobe_Click(object sender, EventArgs e)
        {
            IzdavanjeRobe_Menu frm = new IzdavanjeRobe_Menu();
            frm.ShowDialog();
        }

        private void mtIzvjestaji_Click(object sender, EventArgs e)
        {
            IzvjestajiMenu frm = new IzvjestajiMenu();
            frm.ShowDialog();
        }

        private void mtKorisnici_Click(object sender, EventArgs e)
        {
            DodajKorisnika frm = new DodajKorisnika();

            frm.ShowDialog();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            ListaVozila frm = new ListaVozila();
            frm.ShowDialog();
        }
    }
}
