﻿namespace ElectroExpert.Menu
{
    partial class IzdavanjeRobe_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSklVoz = new MetroFramework.Controls.MetroTile();
            this.btnVozSkl = new MetroFramework.Controls.MetroTile();
            this.btnVozVoz = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // btnSklVoz
            // 
            this.btnSklVoz.ActiveControl = null;
            this.btnSklVoz.Location = new System.Drawing.Point(148, 123);
            this.btnSklVoz.Name = "btnSklVoz";
            this.btnSklVoz.Size = new System.Drawing.Size(286, 91);
            this.btnSklVoz.TabIndex = 5;
            this.btnSklVoz.Text = "Skladište             Vozilo";
            this.btnSklVoz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSklVoz.TileImage = global::ElectroExpert.Properties.Resources.if_go_next_118773;
            this.btnSklVoz.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSklVoz.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.btnSklVoz.UseSelectable = true;
            this.btnSklVoz.UseTileImage = true;
            this.btnSklVoz.Click += new System.EventHandler(this.btnSklVoz_Click);
            // 
            // btnVozSkl
            // 
            this.btnVozSkl.ActiveControl = null;
            this.btnVozSkl.Location = new System.Drawing.Point(148, 257);
            this.btnVozSkl.Name = "btnVozSkl";
            this.btnVozSkl.Size = new System.Drawing.Size(286, 91);
            this.btnVozSkl.TabIndex = 6;
            this.btnVozSkl.Text = "    Vozilo       Skladište";
            this.btnVozSkl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnVozSkl.TileImage = global::ElectroExpert.Properties.Resources.if_go_next_118773;
            this.btnVozSkl.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnVozSkl.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.btnVozSkl.UseSelectable = true;
            this.btnVozSkl.UseTileImage = true;
            this.btnVozSkl.Click += new System.EventHandler(this.btnVozSkl_Click);
            // 
            // btnVozVoz
            // 
            this.btnVozVoz.ActiveControl = null;
            this.btnVozVoz.Location = new System.Drawing.Point(148, 390);
            this.btnVozVoz.Name = "btnVozVoz";
            this.btnVozVoz.Size = new System.Drawing.Size(286, 91);
            this.btnVozVoz.TabIndex = 7;
            this.btnVozVoz.Text = "Vozilo          Vozilo";
            this.btnVozVoz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnVozVoz.TileImage = global::ElectroExpert.Properties.Resources.if_go_next_118773;
            this.btnVozVoz.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnVozVoz.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.btnVozVoz.UseSelectable = true;
            this.btnVozVoz.UseTileImage = true;
            this.btnVozVoz.Click += new System.EventHandler(this.btnVozVoz_Click_1);
            // 
            // IzdavanjeRobe_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 605);
            this.Controls.Add(this.btnVozVoz);
            this.Controls.Add(this.btnVozSkl);
            this.Controls.Add(this.btnSklVoz);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "IzdavanjeRobe_Menu";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prijenos robe";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTile btnSklVoz;
        private MetroFramework.Controls.MetroTile btnVozSkl;
        private MetroFramework.Controls.MetroTile btnVozVoz;
    }
}