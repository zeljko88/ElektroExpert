﻿using ElectroExpert.PrijenosRobe;
using ElectroExpert.Skladiste;
using ElectroExpert.ZaduzenjeRobe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Menu
{
    public partial class IzdavanjeRobe_Menu : MetroFramework.Forms.MetroForm
    {
        public IzdavanjeRobe_Menu()
        {
            InitializeComponent();
        }

        private void btnSkladisteRadnik_Click(object sender, EventArgs e)
        {
            ZaduziOpremu frm = new ZaduziOpremu();
            frm.ShowDialog();
        }

        private void btnRadnikSkladiste_Click(object sender, EventArgs e)
        {
            RadnikSkladiste frm = new RadnikSkladiste();
            frm.ShowDialog();
        }

        private void btnRadnikRadnik_Click(object sender, EventArgs e)
        {
            RadnikRadnik frm = new RadnikRadnik();
            frm.ShowDialog();
        }

        private void btnSklVoz_Click(object sender, EventArgs e)
        {
            SkladisteVoziloLista frm = new SkladisteVoziloLista();
            frm.ShowDialog();
        }

        private void btnVozSkl_Click(object sender, EventArgs e)
        {
            RadnikSkladiste frm = new RadnikSkladiste();
            frm.ShowDialog();
        }

        private void btnVozVoz_Click(object sender, EventArgs e)
        {
            RadnikRadnik frm = new RadnikRadnik();
            frm.ShowDialog();
        }

        private void btnVozVoz_Click_1(object sender, EventArgs e)
        {
            RadnikRadnik frm = new RadnikRadnik();
            frm.ShowDialog();
        }
    }
}
