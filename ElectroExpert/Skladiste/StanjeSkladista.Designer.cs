﻿namespace ElectroExpert.Skladiste
{
    partial class StanjeSkladista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgOpremaSkladiste = new System.Windows.Forms.DataGridView();
            this.ArtikalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sifra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuAddEditDelete = new System.Windows.Forms.MenuStrip();
            this.btnPrintaj = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgOpremaSkladiste)).BeginInit();
            this.menuAddEditDelete.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgOpremaSkladiste
            // 
            this.dgOpremaSkladiste.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgOpremaSkladiste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOpremaSkladiste.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ArtikalId,
            this.Sifra,
            this.Naziv,
            this.JedMjere,
            this.Kolicina});
            this.dgOpremaSkladiste.Location = new System.Drawing.Point(23, 129);
            this.dgOpremaSkladiste.MultiSelect = false;
            this.dgOpremaSkladiste.Name = "dgOpremaSkladiste";
            this.dgOpremaSkladiste.ReadOnly = true;
            this.dgOpremaSkladiste.RowHeadersWidth = 40;
            this.dgOpremaSkladiste.RowTemplate.Height = 26;
            this.dgOpremaSkladiste.RowTemplate.ReadOnly = true;
            this.dgOpremaSkladiste.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOpremaSkladiste.Size = new System.Drawing.Size(843, 463);
            this.dgOpremaSkladiste.TabIndex = 14;
            // 
            // ArtikalId
            // 
            this.ArtikalId.DataPropertyName = "ArtikalId";
            this.ArtikalId.HeaderText = "Id";
            this.ArtikalId.Name = "ArtikalId";
            this.ArtikalId.ReadOnly = true;
            this.ArtikalId.Visible = false;
            // 
            // Sifra
            // 
            this.Sifra.DataPropertyName = "Sifra";
            this.Sifra.HeaderText = "Šifra";
            this.Sifra.Name = "Sifra";
            this.Sifra.ReadOnly = true;
            // 
            // Naziv
            // 
            this.Naziv.DataPropertyName = "Naziv";
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.ReadOnly = true;
            // 
            // JedMjere
            // 
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jedinica mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            // 
            // Kolicina
            // 
            this.Kolicina.DataPropertyName = "Kolicina";
            this.Kolicina.HeaderText = "Količina";
            this.Kolicina.Name = "Kolicina";
            this.Kolicina.ReadOnly = true;
            // 
            // menuAddEditDelete
            // 
            this.menuAddEditDelete.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuAddEditDelete.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPrintaj});
            this.menuAddEditDelete.Location = new System.Drawing.Point(20, 60);
            this.menuAddEditDelete.Name = "menuAddEditDelete";
            this.menuAddEditDelete.Size = new System.Drawing.Size(851, 56);
            this.menuAddEditDelete.TabIndex = 19;
            this.menuAddEditDelete.Text = "menuStrip1";
            // 
            // btnPrintaj
            // 
            this.btnPrintaj.Image = global::ElectroExpert.Properties.Resources.if_document_print_38984;
            this.btnPrintaj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnPrintaj.Name = "btnPrintaj";
            this.btnPrintaj.ShowShortcutKeys = false;
            this.btnPrintaj.Size = new System.Drawing.Size(124, 52);
            this.btnPrintaj.Text = "Štampaj";
            this.btnPrintaj.Click += new System.EventHandler(this.btnPrintaj_Click);
            // 
            // StanjeSkladista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 615);
            this.Controls.Add(this.menuAddEditDelete);
            this.Controls.Add(this.dgOpremaSkladiste);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "StanjeSkladista";
            this.Resizable = false;
            this.Text = "Roba u skladištu";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.StanjeSkladista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgOpremaSkladiste)).EndInit();
            this.menuAddEditDelete.ResumeLayout(false);
            this.menuAddEditDelete.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgOpremaSkladiste;
        private System.Windows.Forms.MenuStrip menuAddEditDelete;
        private System.Windows.Forms.ToolStripMenuItem btnPrintaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArtikalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sifra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kolicina;
    }
}