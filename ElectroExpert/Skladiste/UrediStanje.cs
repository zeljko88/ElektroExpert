﻿using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Skladiste
{
    public partial class UrediStanje : MetroFramework.Forms.MetroForm
    {
        WebApiHelper stanjeSkladistaService = new WebApiHelper("http://localhost:63632/", "api/StanjeSkladista");
        private int id;
        public UrediStanje(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void UrediStanje_Load(object sender, EventArgs e)
        {
            HttpResponseMessage response = stanjeSkladistaService.GetActionResponse("GetStanjeArtikla", id.ToString());
            if (response.IsSuccessStatusCode)
            {
                List<StanjeSkladistaDTO> s = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

                txtSifra.Text = s[0].Sifra;
                txtJedMjere.Text = s[0].JedMjere;
                txtKolicina.Text = s[0].Kolicina.ToString();
                txtNaziv.Text = s[0].Naziv;

            }
        }


        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Jeste li sigurni ?", "Upozorenje !", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (confirmResult == DialogResult.Yes)
            {

                HttpResponseMessage response = stanjeSkladistaService.GetActionResponse("UrediArtikal", id.ToString(), txtKolicina.Text);
                if (response.IsSuccessStatusCode)
                {

                    this.Close();

                }
            }
            else
            {
                return;
            }
        }

        private void txtKolicina_Validating(object sender, CancelEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtKolicina.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtKolicina, "");
            }
        }
    }
}
