﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Skladiste
{
    public partial class StanjeSkladista : MetroFramework.Forms.MetroForm
    {
        WebApiHelper stanjeSkladistaService = new WebApiHelper("http://localhost:63632/", "api/StanjeSkladista");
        public StanjeSkladista()
        {
            InitializeComponent();
            dgOpremaSkladiste.AutoGenerateColumns = false;
        }

        private void StanjeSkladista_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            

            HttpResponseMessage response = stanjeSkladistaService.GetResponse();
            if (response.IsSuccessStatusCode)
            {
                dgOpremaSkladiste.DataSource = response.Content.ReadAsAsync<List<StanjeSkladistaDTO>>().Result;

            }


        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            StanjeSkladistaReport frm = new StanjeSkladistaReport();
            frm.ShowDialog();
        }
    }
}
