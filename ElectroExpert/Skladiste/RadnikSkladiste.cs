﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Skladiste
{
    public partial class RadnikSkladiste : MetroFramework.Forms.MetroForm
    {
        WebApiHelper zaduzenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOprema");
        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        WebApiHelper ulazStavkeService = new WebApiHelper("http://localhost:63632/", "api/UlazStavke");
        WebApiHelper ulazService = new WebApiHelper("http://localhost:63632/", "api/Ulaz");
        WebApiHelper zaduzenaStavkeService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOpremaStavke");

        private List<VoziloDTO> vozila { get; set; }
        private int noviUlazId;
        private DateTime datumUlaza;
        public VoziloDTO voz { get; set; }
        private string vozilo;
        public RadnikSkladiste()
        {
            InitializeComponent();
            dgRadnici.AutoGenerateColumns = false;
            dgOpremaSkladiste.AutoGenerateColumns = false;
            dgRadnici.ClearSelection();
            dgZaduzenaOprema.AutoGenerateColumns = false;
            btnZakljuci.Enabled = false;
            btnPrintaj.Enabled = false;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {


            if (this.ValidateChildren())
            {

                int kol;
                foreach (DataGridViewRow row in dgOpremaSkladiste.Rows)
                {
                    if (Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value) == Convert.ToInt32(row.Cells["IdArtikla"].Value))
                    {
                        kol = Convert.ToInt32(row.Cells["KolicinaBack"].Value);
                        kol += Convert.ToInt32(txtKolicina.Text);
                        row.Cells["KolicinaBack"].Value = kol;
                        txtKolicina.Text = "";
                        btnZakljuci.Enabled = true;
                        return;
                    }

                }

                int n = dgOpremaSkladiste.Rows.Add();
                dgOpremaSkladiste.Rows[n].Cells[0].Value = Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value);
                dgOpremaSkladiste.Rows[n].Cells[1].Value = Convert.ToString(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[1].Value);
                dgOpremaSkladiste.Rows[n].Cells[2].Value = Convert.ToString(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[2].Value);
                dgOpremaSkladiste.Rows[n].Cells[3].Value = Convert.ToString(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[3].Value);
                dgOpremaSkladiste.Rows[n].Cells[4].Value = Convert.ToInt32(txtKolicina.Text);
                txtKolicina.Text = "";
                btnZakljuci.Enabled = true;

            }
        }

        private void RadnikSkladiste_Load(object sender, EventArgs e)
        {
            BindRadnici();
            dgRadnici.ClearSelection();
            btnPrintaj.Enabled = false;

        }



        private void BindRadnici()
        {

            HttpResponseMessage response = radniciService.GetActionResponse("GetRadniciAndVozilo");

            if (response.IsSuccessStatusCode)
            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                vozilo = vozila[0].NazivVozila;
                dgRadnici.DataSource = vozila;
            }

            else
            {
                MessageBox.Show("Greska - Error Code:" + response.StatusCode + "-Message:" + response.ReasonPhrase);
            }
        }

        private void BindForm(VoziloDTO vozilo)
        {

            HttpResponseMessage zaduzenaresponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(vozilo.VoziloId));
            if (zaduzenaresponse.IsSuccessStatusCode)
            {
                dgZaduzenaOprema.DataSource = zaduzenaresponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
                dgZaduzenaOprema.ClearSelection();
            }
        }

        private void dgRadnici_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            voz = vozila[e.RowIndex];
            BindForm(voz);

        }

        private void btnZakljuci_Click(object sender, EventArgs e)
        {

            Ulaz u = new Ulaz();
            u.Datum = DateTime.Now;
            u.KorisnikId = Globall.prijavljeniKorisnik.Id;
            u.DobavljacId = 1;
            u.VoziloId = Convert.ToInt32(dgRadnici.Rows[dgRadnici.CurrentRow.Index].Cells[0].Value);
            u.BrojFakture = Convert.ToString(0000);

            if (u.UlazStavke == null)
            {
                u.UlazStavke = new List<UlazStavke>();

                for (int i = 0; i < dgOpremaSkladiste.RowCount; i++)
                {
                    UlazStavke s = new UlazStavke();
                    s.Kolicina = Convert.ToInt32(dgOpremaSkladiste.Rows[i].Cells[4].Value);
                    s.ArtikalId = Convert.ToInt32(dgOpremaSkladiste.Rows[i].Cells[0].Value);
                    u.UlazStavke.Add(s);
                }
                HttpResponseMessage response = ulazService.PostResponse(u);
                if (response.IsSuccessStatusCode)
                {
                    Ulaz ulaz = response.Content.ReadAsAsync<Ulaz>().Result;
                    noviUlazId = ulaz.Id;
                    datumUlaza = ulaz.Datum;

                    MessageBox.Show(Globall.GetMessage("ulaz_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnZakljuci.Enabled = false;
                    btnPrintaj.Enabled = true;
                    btnDodaj.Enabled = false;

                }
                else
                {
                    MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            VoziloSkladisteReport frm = new VoziloSkladisteReport(noviUlazId, vozilo, datumUlaza);
            frm.ShowDialog();
        }


        private void txtKolicina_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKolicina.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("kol_req"));

            }

            else if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*[1-9][0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

           else if (dgRadnici.SelectedRows.Count > 0 && Convert.ToInt32(txtKolicina.Text) > Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[4].Value))
            {
                e.Cancel = true;
                errorProvider1.SetError(dgZaduzenaOprema, Globall.GetMessage("nema_dovoljno"));
                errorProvider1.SetError(txtKolicina, "");
            }

            else
            {
                errorProvider1.SetError(txtKolicina, "");
                errorProvider1.SetError(dgZaduzenaOprema, "");

            }
        }

        private void dgRadnici_Validating(object sender, CancelEventArgs e)
        {
            if (dgRadnici.SelectedRows.Count == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(dgRadnici, Globall.GetMessage("voz_req"));
            }
            else
            {
                errorProvider1.SetError(dgRadnici, "");
            }
        }

       

        private void dgZaduzenaOprema_Validating(object sender, CancelEventArgs e)
        {

            int kol;
            if (txtKolicina.Text=="")
            {
                kol = 0;
            }
            else
            {
                kol = Convert.ToInt32(txtKolicina.Text);
            }
            foreach (DataGridViewRow row in dgOpremaSkladiste.Rows)
            {

                if (Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value) == Convert.ToInt32(row.Cells["IdArtikla"].Value) &&
                Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[4].Value) < Convert.ToInt32(row.Cells["KolicinaBack"].Value) + kol)
                {
                    e.Cancel = true;
                    errorProvider1.SetError(dgZaduzenaOprema, Globall.GetMessage("nema_dovoljno"));

                }



                else
                {
                    errorProvider1.SetError(dgZaduzenaOprema, "");
                }
            }

        }
        

    

        private void txtSifra_TextChanged_1(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*$"))
            {
                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtSifra, "");
            }


            TextBox ak = sender as TextBox;
            string ar = ak.Text as string;

            if (ak.Text == "")
            {
                HttpResponseMessage httpResponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(voz.VoziloId));
                if (httpResponse.IsSuccessStatusCode)
                {
                    dgZaduzenaOprema.DataSource = httpResponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
                    dgZaduzenaOprema.ClearSelection();
                }

            }

            List<ZaduzenaOpremaDTO> artikli = new List<ZaduzenaOpremaDTO>();
            List<ZaduzenaOpremaDTO> listaArtikala = new List<ZaduzenaOpremaDTO>();

            HttpResponseMessage zadresponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(voz.VoziloId));
            if (zadresponse.IsSuccessStatusCode)
            {
                artikli = zadresponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;

            }

            foreach (ZaduzenaOpremaDTO item in artikli)
            {
                if (Regex.IsMatch(item.SifraArtikla, ar, RegexOptions.IgnoreCase))
                {
                    listaArtikala.Add(item);
                }


            }

            dgZaduzenaOprema.DataSource = listaArtikala;
        }
    }
    }
    

