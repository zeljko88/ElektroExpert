﻿namespace ElectroExpert.Skladiste
{
    partial class RadnikRadnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbxVozila = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgVozila = new System.Windows.Forms.DataGridView();
            this.VoziloId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SefVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrTelefona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsSef = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgVozilaZaPrebacit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbxVozilaZaPrebacit = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgZaduzenaOprema = new System.Windows.Forms.DataGridView();
            this.ArtikalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SifraArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazivArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KolicinaZaduzenog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSifra = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtKolicina = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgOpremaZaPrebacit = new System.Windows.Forms.DataGridView();
            this.IdArtikla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JedinicaMjere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZaduzenaKolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnZakljuci = new System.Windows.Forms.Button();
            this.btnPrintaj = new System.Windows.Forms.Button();
            this.btnPrebacii = new MetroFramework.Controls.MetroTile();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVozila)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVozilaZaPrebacit)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgZaduzenaOprema)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOpremaZaPrebacit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbxVozila);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.dgVozila);
            this.groupBox3.Location = new System.Drawing.Point(13, 80);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(536, 250);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Vozila";
            // 
            // cbxVozila
            // 
            this.cbxVozila.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxVozila.FormattingEnabled = true;
            this.cbxVozila.Location = new System.Drawing.Point(10, 62);
            this.cbxVozila.Name = "cbxVozila";
            this.cbxVozila.Size = new System.Drawing.Size(216, 33);
            this.cbxVozila.TabIndex = 4;
            this.cbxVozila.SelectionChangeCommitted += new System.EventHandler(this.cbxVozila_SelectionChangeCommitted);
            this.cbxVozila.Validating += new System.ComponentModel.CancelEventHandler(this.cbxVozila_Validating);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 20);
            this.label12.TabIndex = 20;
            this.label12.Text = "Radnici:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 20);
            this.label6.TabIndex = 19;
            this.label6.Text = "Vozilo:";
            // 
            // dgVozila
            // 
            this.dgVozila.AllowUserToAddRows = false;
            this.dgVozila.AllowUserToDeleteRows = false;
            this.dgVozila.AllowUserToResizeColumns = false;
            this.dgVozila.AllowUserToResizeRows = false;
            this.dgVozila.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgVozila.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVozila.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VoziloId,
            this.SefVozila,
            this.BrTelefona,
            this.IsSef});
            this.dgVozila.Location = new System.Drawing.Point(6, 154);
            this.dgVozila.MultiSelect = false;
            this.dgVozila.Name = "dgVozila";
            this.dgVozila.ReadOnly = true;
            this.dgVozila.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgVozila.RowTemplate.Height = 24;
            this.dgVozila.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgVozila.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVozila.Size = new System.Drawing.Size(499, 85);
            this.dgVozila.TabIndex = 0;
            // 
            // VoziloId
            // 
            this.VoziloId.DataPropertyName = "VoziloId";
            this.VoziloId.HeaderText = "Id";
            this.VoziloId.Name = "VoziloId";
            this.VoziloId.ReadOnly = true;
            this.VoziloId.Visible = false;
            // 
            // SefVozila
            // 
            this.SefVozila.DataPropertyName = "SefVozila";
            this.SefVozila.FillWeight = 111.9289F;
            this.SefVozila.HeaderText = "Ime i prezime";
            this.SefVozila.Name = "SefVozila";
            this.SefVozila.ReadOnly = true;
            // 
            // BrTelefona
            // 
            this.BrTelefona.DataPropertyName = "BrTelefona";
            this.BrTelefona.FillWeight = 111.9289F;
            this.BrTelefona.HeaderText = "Telefon";
            this.BrTelefona.Name = "BrTelefona";
            this.BrTelefona.ReadOnly = true;
            // 
            // IsSef
            // 
            this.IsSef.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IsSef.DataPropertyName = "IsSef";
            this.IsSef.FillWeight = 76.14214F;
            this.IsSef.HeaderText = "Šef";
            this.IsSef.Name = "IsSef";
            this.IsSef.ReadOnly = true;
            this.IsSef.Width = 50;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgVozilaZaPrebacit);
            this.groupBox1.Controls.Add(this.cbxVozilaZaPrebacit);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(711, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(510, 250);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vozila";
            // 
            // dgVozilaZaPrebacit
            // 
            this.dgVozilaZaPrebacit.AllowUserToAddRows = false;
            this.dgVozilaZaPrebacit.AllowUserToDeleteRows = false;
            this.dgVozilaZaPrebacit.AllowUserToResizeColumns = false;
            this.dgVozilaZaPrebacit.AllowUserToResizeRows = false;
            this.dgVozilaZaPrebacit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgVozilaZaPrebacit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVozilaZaPrebacit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewCheckBoxColumn1});
            this.dgVozilaZaPrebacit.Location = new System.Drawing.Point(10, 154);
            this.dgVozilaZaPrebacit.MultiSelect = false;
            this.dgVozilaZaPrebacit.Name = "dgVozilaZaPrebacit";
            this.dgVozilaZaPrebacit.ReadOnly = true;
            this.dgVozilaZaPrebacit.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgVozilaZaPrebacit.RowTemplate.Height = 24;
            this.dgVozilaZaPrebacit.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgVozilaZaPrebacit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVozilaZaPrebacit.Size = new System.Drawing.Size(481, 85);
            this.dgVozilaZaPrebacit.TabIndex = 21;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VoziloId";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SefVozila";
            this.dataGridViewTextBoxColumn2.FillWeight = 111.9289F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Ime i prezime";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BrTelefona";
            this.dataGridViewTextBoxColumn3.FillWeight = 111.9289F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Telefon";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "IsSef";
            this.dataGridViewCheckBoxColumn1.FillWeight = 76.14214F;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Šef";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // cbxVozilaZaPrebacit
            // 
            this.cbxVozilaZaPrebacit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxVozilaZaPrebacit.FormattingEnabled = true;
            this.cbxVozilaZaPrebacit.Location = new System.Drawing.Point(10, 62);
            this.cbxVozilaZaPrebacit.Name = "cbxVozilaZaPrebacit";
            this.cbxVozilaZaPrebacit.Size = new System.Drawing.Size(216, 33);
            this.cbxVozilaZaPrebacit.TabIndex = 21;
            this.cbxVozilaZaPrebacit.SelectionChangeCommitted += new System.EventHandler(this.cbxVozilaZaPrebacit_SelectionChangeCommitted);
            this.cbxVozilaZaPrebacit.Validating += new System.ComponentModel.CancelEventHandler(this.cbxVozilaZaPrebacit_Validating_1);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 131);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 20);
            this.label13.TabIndex = 21;
            this.label13.Text = "Radnici:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Vozilo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 21;
            this.label2.Text = "Odaberite vozilo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(922, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Prijenos na vozilo";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgZaduzenaOprema);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtSifra);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(13, 375);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(536, 389);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Zadužena oprema ";
            // 
            // dgZaduzenaOprema
            // 
            this.dgZaduzenaOprema.AllowUserToAddRows = false;
            this.dgZaduzenaOprema.AllowUserToDeleteRows = false;
            this.dgZaduzenaOprema.AllowUserToOrderColumns = true;
            this.dgZaduzenaOprema.AllowUserToResizeColumns = false;
            this.dgZaduzenaOprema.AllowUserToResizeRows = false;
            this.dgZaduzenaOprema.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgZaduzenaOprema.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgZaduzenaOprema.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ArtikalId,
            this.SifraArtikla,
            this.NazivArtikla,
            this.JedMjere,
            this.KolicinaZaduzenog});
            this.dgZaduzenaOprema.Location = new System.Drawing.Point(6, 112);
            this.dgZaduzenaOprema.MultiSelect = false;
            this.dgZaduzenaOprema.Name = "dgZaduzenaOprema";
            this.dgZaduzenaOprema.ReadOnly = true;
            this.dgZaduzenaOprema.RowHeadersWidth = 40;
            this.dgZaduzenaOprema.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgZaduzenaOprema.RowTemplate.Height = 26;
            this.dgZaduzenaOprema.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgZaduzenaOprema.Size = new System.Drawing.Size(500, 271);
            this.dgZaduzenaOprema.TabIndex = 0;
            this.dgZaduzenaOprema.Validating += new System.ComponentModel.CancelEventHandler(this.dgZaduzenaOprema_Validating);
            // 
            // ArtikalId
            // 
            this.ArtikalId.DataPropertyName = "ArtikalId";
            this.ArtikalId.HeaderText = "ArtikalId";
            this.ArtikalId.Name = "ArtikalId";
            this.ArtikalId.ReadOnly = true;
            this.ArtikalId.Visible = false;
            // 
            // SifraArtikla
            // 
            this.SifraArtikla.DataPropertyName = "SifraArtikla";
            this.SifraArtikla.HeaderText = "Sifra";
            this.SifraArtikla.Name = "SifraArtikla";
            this.SifraArtikla.ReadOnly = true;
            // 
            // NazivArtikla
            // 
            this.NazivArtikla.DataPropertyName = "NazivArtikla";
            this.NazivArtikla.HeaderText = "Naziv";
            this.NazivArtikla.Name = "NazivArtikla";
            this.NazivArtikla.ReadOnly = true;
            // 
            // JedMjere
            // 
            this.JedMjere.DataPropertyName = "JedMjere";
            this.JedMjere.HeaderText = "Jed mjere";
            this.JedMjere.Name = "JedMjere";
            this.JedMjere.ReadOnly = true;
            // 
            // KolicinaZaduzenog
            // 
            this.KolicinaZaduzenog.DataPropertyName = "KolicinaZaduzenog";
            this.KolicinaZaduzenog.HeaderText = "Kolicina";
            this.KolicinaZaduzenog.Name = "KolicinaZaduzenog";
            this.KolicinaZaduzenog.ReadOnly = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "Šifra:";
            // 
            // txtSifra
            // 
            this.txtSifra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSifra.Location = new System.Drawing.Point(10, 67);
            this.txtSifra.Name = "txtSifra";
            this.txtSifra.Size = new System.Drawing.Size(152, 30);
            this.txtSifra.TabIndex = 0;
            this.txtSifra.TextChanged += new System.EventHandler(this.txtSifra_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(582, 366);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Količina:";
            // 
            // txtKolicina
            // 
            this.txtKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKolicina.Location = new System.Drawing.Point(586, 389);
            this.txtKolicina.Name = "txtKolicina";
            this.txtKolicina.Size = new System.Drawing.Size(87, 30);
            this.txtKolicina.TabIndex = 15;
            this.txtKolicina.Validating += new System.ComponentModel.CancelEventHandler(this.txtKolicina_Validating);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(161, 345);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(177, 17);
            this.label11.TabIndex = 24;
            this.label11.Text = "Odaberite robu za prijenos";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgOpremaZaPrebacit);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(711, 375);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(510, 389);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Oprema koja se prenosi";
            // 
            // dgOpremaZaPrebacit
            // 
            this.dgOpremaZaPrebacit.AllowUserToAddRows = false;
            this.dgOpremaZaPrebacit.AllowUserToDeleteRows = false;
            this.dgOpremaZaPrebacit.AllowUserToOrderColumns = true;
            this.dgOpremaZaPrebacit.AllowUserToResizeColumns = false;
            this.dgOpremaZaPrebacit.AllowUserToResizeRows = false;
            this.dgOpremaZaPrebacit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgOpremaZaPrebacit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOpremaZaPrebacit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdArtikla,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.JedinicaMjere,
            this.ZaduzenaKolicina});
            this.dgOpremaZaPrebacit.Location = new System.Drawing.Point(6, 112);
            this.dgOpremaZaPrebacit.MultiSelect = false;
            this.dgOpremaZaPrebacit.Name = "dgOpremaZaPrebacit";
            this.dgOpremaZaPrebacit.ReadOnly = true;
            this.dgOpremaZaPrebacit.RowHeadersWidth = 40;
            this.dgOpremaZaPrebacit.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgOpremaZaPrebacit.RowTemplate.Height = 26;
            this.dgOpremaZaPrebacit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOpremaZaPrebacit.Size = new System.Drawing.Size(485, 271);
            this.dgOpremaZaPrebacit.TabIndex = 0;
            this.dgOpremaZaPrebacit.Validating += new System.ComponentModel.CancelEventHandler(this.dgOpremaZaPrebacit_Validating);
            // 
            // IdArtikla
            // 
            this.IdArtikla.DataPropertyName = "IdArtikla";
            this.IdArtikla.HeaderText = "ArtikalId";
            this.IdArtikla.Name = "IdArtikla";
            this.IdArtikla.ReadOnly = true;
            this.IdArtikla.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "SifraArtikla";
            this.dataGridViewTextBoxColumn6.HeaderText = "Sifra";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NazivArtikla";
            this.dataGridViewTextBoxColumn7.HeaderText = "Naziv";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // JedinicaMjere
            // 
            this.JedinicaMjere.DataPropertyName = "JedinicaMjere";
            this.JedinicaMjere.HeaderText = "Jed mjere";
            this.JedinicaMjere.Name = "JedinicaMjere";
            this.JedinicaMjere.ReadOnly = true;
            // 
            // ZaduzenaKolicina
            // 
            this.ZaduzenaKolicina.DataPropertyName = "ZaduzenaKolicina";
            this.ZaduzenaKolicina.HeaderText = "Kolicina";
            this.ZaduzenaKolicina.Name = "ZaduzenaKolicina";
            this.ZaduzenaKolicina.ReadOnly = true;
            // 
            // btnZakljuci
            // 
            this.btnZakljuci.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnZakljuci.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZakljuci.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnZakljuci.Location = new System.Drawing.Point(1076, 770);
            this.btnZakljuci.Name = "btnZakljuci";
            this.btnZakljuci.Size = new System.Drawing.Size(136, 45);
            this.btnZakljuci.TabIndex = 25;
            this.btnZakljuci.Text = "Zaključi";
            this.btnZakljuci.UseVisualStyleBackColor = false;
            this.btnZakljuci.Click += new System.EventHandler(this.btnZakljuci_Click);
            // 
            // btnPrintaj
            // 
            this.btnPrintaj.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPrintaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintaj.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPrintaj.Location = new System.Drawing.Point(925, 770);
            this.btnPrintaj.Name = "btnPrintaj";
            this.btnPrintaj.Size = new System.Drawing.Size(136, 45);
            this.btnPrintaj.TabIndex = 26;
            this.btnPrintaj.Text = "Printaj";
            this.btnPrintaj.UseVisualStyleBackColor = false;
            this.btnPrintaj.Click += new System.EventHandler(this.btnPrintaj_Click);
            // 
            // btnPrebacii
            // 
            this.btnPrebacii.ActiveControl = null;
            this.btnPrebacii.Location = new System.Drawing.Point(555, 442);
            this.btnPrebacii.Name = "btnPrebacii";
            this.btnPrebacii.Size = new System.Drawing.Size(150, 44);
            this.btnPrebacii.TabIndex = 27;
            this.btnPrebacii.Text = "Prebaci";
            this.btnPrebacii.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPrebacii.TileImage = global::ElectroExpert.Properties.Resources.if_go_next_118773;
            this.btnPrebacii.TileImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrebacii.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.btnPrebacii.UseSelectable = true;
            this.btnPrebacii.UseTileImage = true;
            this.btnPrebacii.Click += new System.EventHandler(this.btnPrebacii_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // RadnikRadnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(1226, 824);
            this.Controls.Add(this.btnPrebacii);
            this.Controls.Add(this.btnPrintaj);
            this.Controls.Add(this.btnZakljuci);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtKolicina);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Movable = false;
            this.Name = "RadnikRadnik";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vozilo --> Vozilo";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.RadnikRadnik_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVozila)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVozilaZaPrebacit)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgZaduzenaOprema)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgOpremaZaPrebacit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgVozila;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgZaduzenaOprema;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtKolicina;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSifra;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgOpremaZaPrebacit;
        private System.Windows.Forms.Button btnZakljuci;
        private System.Windows.Forms.ComboBox cbxVozila;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbxVozilaZaPrebacit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dgVozilaZaPrebacit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoziloId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SefVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrTelefona;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsSef;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArtikalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SifraArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazivArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn KolicinaZaduzenog;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdArtikla;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn JedinicaMjere;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZaduzenaKolicina;
        private System.Windows.Forms.Button btnPrintaj;
        private MetroFramework.Controls.MetroTile btnPrebacii;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}