﻿using ElectroExpert.Artikli;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Skladiste
{
    public partial class PrijemRobe : MetroFramework.Forms.MetroForm
    {
        WebApiHelper artikliService = new WebApiHelper("http://localhost:63632/", "api/Artikal");
        WebApiHelper dobavljaciService = new WebApiHelper("http://localhost:63632/", "api/Dobavljac");
        WebApiHelper ulazService = new WebApiHelper("http://localhost:63632/", "api/Ulaz");

        public PrijemRobe()
        {
            InitializeComponent();            
            this.AutoValidate = AutoValidate.Disable;
        }

        private void PrijemRobe_Load(object sender, EventArgs e)
        {
            BindForm();
        }

        private void BindForm()
        {
            HttpResponseMessage responseDobavljac = dobavljaciService.GetResponse();
            if (responseDobavljac.IsSuccessStatusCode)
            {
                List<Dobavljac> dobavljac = responseDobavljac.Content.ReadAsAsync<List<Dobavljac>>().Result;
                dobavljac.Insert(0, new Dobavljac());
                cbxDobavljaci.DataSource = dobavljac;
                cbxDobavljaci.DisplayMember = "Naziv";
                cbxDobavljaci.ValueMember = "Id";

            }
        }

        private void btnDodajStavku_Click(object sender, EventArgs e)
        {
            using (ListaArtikalaAdd frm = new ListaArtikalaAdd() { Artikal = new ArtikalPrijemDTO() })
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    bindingSource1.Add(frm.Artikal);
                    
                }
            }

            //int n = dgStavkeUlaza.Rows.Add();
            //dgStavkeUlaza.Rows[n].Cells[0].Value = Convert.ToInt32(dgArtikliBaza.Rows[dgArtikliBaza.CurrentRow.Index].Cells[0].Value);
            //dgStavkeUlaza.Rows[n].Cells[1].Value = Convert.ToString(dgArtikliBaza.Rows[dgArtikliBaza.CurrentRow.Index].Cells[1].Value);
            //dgStavkeUlaza.Rows[n].Cells[2].Value = Convert.ToString(dgArtikliBaza.Rows[dgArtikliBaza.CurrentRow.Index].Cells[2].Value);
            //dgStavkeUlaza.Rows[n].Cells[3].Value = Convert.ToString(dgArtikliBaza.Rows[dgArtikliBaza.CurrentRow.Index].Cells[3].Value);
            //dgStavkeUlaza.Rows[n].Cells[4].Value = Convert.ToInt32(txtKolicina.Text);
        }

        private void btnZakljuci_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                Ulaz u = new Ulaz();
                u.Datum = DateTime.Now;
                u.KorisnikId = Globall.prijavljeniKorisnik.Id;                
                u.DobavljacId = Convert.ToInt32(cbxDobavljaci.SelectedValue);
                u.BrojFakture = Convert.ToString(txtBrojFakture.Text);

                if (u.UlazStavke == null)
                {                  
                    u.UlazStavke = new List<UlazStavke>();
                    for (int i = 0; i < dgStavkeUlaza.RowCount; i++)
                    {
                        UlazStavke s = new UlazStavke();
                        s.Kolicina = Convert.ToInt32(dgStavkeUlaza.Rows[i].Cells[5].Value);                        
                        s.ArtikalId = Convert.ToInt32(dgStavkeUlaza.Rows[i].Cells[0].Value);
                        u.UlazStavke.Add(s);
                    }

                    HttpResponseMessage response = ulazService.PostResponse(u);
                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show(Globall.GetMessage("ulaz_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            using (ListaArtikalaAdd frm = new ListaArtikalaAdd() { Artikal = new ArtikalPrijemDTO() })
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    if (frm.ValidateChildren())
                    {
                        int kol;
                        foreach (DataGridViewRow row in dgStavkeUlaza.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[0].Value) == frm.Artikal.Id)
                            {
                                kol = Convert.ToInt32(row.Cells["Kolicina"].Value);
                                kol += frm.Artikal.Kolicina;
                                row.Cells["Kolicina"].Value = kol;
                                return;
                            }
                        }
                        bindingSource1.Add(frm.Artikal);
                        dgStavkeUlaza.ClearSelection();
                    }
                }
            }
        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            if (dgStavkeUlaza.SelectedRows.Count > 0)
            {
                int row = dgStavkeUlaza.CurrentCell.RowIndex;
                dgStavkeUlaza.Rows.RemoveAt(row);
            }
        }
        

        #region Validacija
        private void txtBrojFakture_Validating(object sender, CancelEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtBrojFakture.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtBrojFakture, Globall.GetMessage("empty"));
            }



            else if (!Regex.IsMatch(txtBrojFakture.Text, "^[0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtBrojFakture, Globall.GetMessage("num_only"));
            }

            else
            {

                errorProvider1.SetError(txtBrojFakture, "");
            }
        }

        private void cbxDobavljaci_Validating(object sender, CancelEventArgs e)
        {
            if (cbxDobavljaci.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxDobavljaci, Globall.GetMessage("empty"));
            }
            else
            {
                errorProvider1.SetError(cbxDobavljaci, "");
            }
        }

        private void dgStavkeUlaza_Validating(object sender, CancelEventArgs e)
        {
            if (dgStavkeUlaza.Rows.Count == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(dgStavkeUlaza, Globall.GetMessage("empty"));
            }
            else
            {
                errorProvider1.SetError(dgStavkeUlaza, "");
            }
        } 
        #endregion


    }
}

        
