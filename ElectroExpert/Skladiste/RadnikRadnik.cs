﻿using ElectroExpert.Izvjestaji;
using ElectroExpert_API.DTO;
using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert.Skladiste
{
    public partial class RadnikRadnik : MetroFramework.Forms.MetroForm
    {
       
        WebApiHelper zaduzenaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaUlaz");
        WebApiHelper radniciService = new WebApiHelper("http://localhost:63632/", "api/Radnik");
        WebApiHelper voziloService = new WebApiHelper("http://localhost:63632/", "api/Vozilo");
        WebApiHelper zaduzenaStavkeService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOpremaStavke");
        WebApiHelper zaduzenaOpremaService = new WebApiHelper("http://localhost:63632/", "api/ZaduzenaOprema");

        private List<VoziloDTO> vozila { get; set; }
        private List<VoziloDTO> voziladva { get; set; }
        private int noviUlazId;
        public RadnikRadnik()
        {
            InitializeComponent();
            dgVozila.AutoGenerateColumns = false;
            dgOpremaZaPrebacit.AutoGenerateColumns = false;
            dgZaduzenaOprema.AutoGenerateColumns = false;
            dgVozilaZaPrebacit.AutoGenerateColumns = false;
            dgOpremaZaPrebacit.Columns[4].CellTemplate.ValueType = typeof(int);
        }

        private void RadnikRadnik_Load(object sender, EventArgs e)
        {

            BindVozila();
            btnPrintaj.Enabled = false;
            btnZakljuci.Enabled = false;
        }

        private void BindVozila()
        {
            HttpResponseMessage response = voziloService.GetResponse();
            if (response.IsSuccessStatusCode)
            {

                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                vozila.Insert(0, new Vozilo());
                cbxVozila.DataSource = vozila;
                cbxVozila.DisplayMember = "Naziv";
                cbxVozila.ValueMember = "Id";

            }
        }

        private void BindZaduzenaOprema(int id)
        {
            HttpResponseMessage zaduzenaStavkeresponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(id));
            if (zaduzenaStavkeresponse.IsSuccessStatusCode)
            {
                dgZaduzenaOprema.DataSource = zaduzenaStavkeresponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
                dgZaduzenaOprema.ClearSelection();
            }
        }

        private void BindRadnici()
        {
            HttpResponseMessage response = radniciService.GetResponse();

            if (response.IsSuccessStatusCode)
            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                dgVozila.DataSource = vozila;
                dgVozila.ClearSelection();
            }

            else
            {
                MessageBox.Show("Greska - Error Code:" + response.StatusCode + "-Message:" + response.ReasonPhrase);
            }
        }




        private void BindVozilaDva(int id)
        {
            HttpResponseMessage response = voziloService.GetActionResponse("GetVozilaExceptId", id.ToString());
            if (response.IsSuccessStatusCode)
            {


                List<Vozilo> vozila = response.Content.ReadAsAsync<List<Vozilo>>().Result;
                vozila.Insert(0, new Vozilo());
                cbxVozilaZaPrebacit.DataSource = vozila;
                cbxVozilaZaPrebacit.DisplayMember = "Naziv";
                cbxVozilaZaPrebacit.ValueMember = "Id";

            }
        }

        private void btnZakljuci_Click(object sender, EventArgs e)
        {
            int brojStavki = dgOpremaZaPrebacit.RowCount;

            ZaduzenaUlaz zaduzena = new ZaduzenaUlaz();
            zaduzena.KorisnikId = 3;
            zaduzena.VoziloId = (Convert.ToInt32(cbxVozilaZaPrebacit.SelectedValue));
            zaduzena.Datum = DateTime.Now;
            zaduzena.SaVozila = (Convert.ToInt32(cbxVozila.SelectedValue)); ;


            if (zaduzena.ZaduzenaUlazStavke == null)
            {
                zaduzena.ZaduzenaUlazStavke = new List<ZaduzenaUlazStavke>();

                foreach (DataGridViewRow row in dgOpremaZaPrebacit.Rows)
                {


                    ZaduzenaUlazStavke stavka = new ZaduzenaUlazStavke();

                    stavka.ArtikalId = Convert.ToInt32(row.Cells["IdArtikla"].Value);
                    stavka.Kolicina = Convert.ToInt32(row.Cells["ZaduzenaKolicina"].Value);
                    
                    zaduzena.ZaduzenaUlazStavke.Add(stavka);

                }
            }

            HttpResponseMessage response = zaduzenaService.PostResponse(zaduzena);

            if (response.IsSuccessStatusCode)
            {
                ZaduzenaUlaz zad = response.Content.ReadAsAsync<ZaduzenaUlaz>().Result;
                noviUlazId = zad.Id;


                MessageBox.Show(Globall.GetMessage("zo_succ"), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnPrintaj.Enabled = true;
                btnZakljuci.Enabled = false;
                btnPrebacii.Enabled = false;
                txtKolicina.Enabled = false;
                cbxVozila.Enabled = false;
                cbxVozilaZaPrebacit.Enabled = false;
                dgOpremaZaPrebacit.Rows.Clear();
                dgOpremaZaPrebacit.Refresh();
            }
            else
            {
                MessageBox.Show(Globall.GetMessage(response.ReasonPhrase), Globall.GetMessage("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cbxVozila_SelectionChangeCommitted(object sender, EventArgs e)
        {
            BindRadnici(Convert.ToInt32(cbxVozila.SelectedValue));
            BindZaduzenaOprema(Convert.ToInt32(cbxVozila.SelectedValue));
            BindVozilaDva(Convert.ToInt32(cbxVozila.SelectedValue));
        }

        private void BindRadnici(int v)
        {
            HttpResponseMessage response = radniciService.GetActionResponse("GetRadniciByVozilo", v.ToString());

            if (response.IsSuccessStatusCode)
            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                dgVozila.DataSource = vozila;
                dgVozila.ClearSelection();
            }
        }

        private void cbxVozilaZaPrebacit_SelectionChangeCommitted(object sender, EventArgs e)
        {
            BindRadniciDva(Convert.ToInt32(cbxVozilaZaPrebacit.SelectedValue));
        }

        private void BindRadniciDva(int v)
        {
            HttpResponseMessage response = radniciService.GetActionResponse("GetRadniciByVozilo", v.ToString());

            if (response.IsSuccessStatusCode)
            {
                vozila = response.Content.ReadAsAsync<List<VoziloDTO>>().Result;
                dgVozilaZaPrebacit.DataSource = vozila;
                dgVozilaZaPrebacit.ClearSelection();
            }
        }

        private void btnPrintaj_Click(object sender, EventArgs e)
        {
            VoziloVoziloReport frm = new VoziloVoziloReport(noviUlazId, cbxVozila.Text, cbxVozilaZaPrebacit.Text);
            frm.ShowDialog();
        }

        private void btnPrebacii_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                int kol;
                foreach (DataGridViewRow row in dgOpremaZaPrebacit.Rows)
                {
                    if (Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value) == Convert.ToInt32(row.Cells["IdArtikla"].Value))
                    {
                        kol = Convert.ToInt32(row.Cells["ZaduzenaKolicina"].Value);
                        kol += Convert.ToInt32(txtKolicina.Text);
                        row.Cells["ZaduzenaKolicina"].Value = kol;
                        btnZakljuci.Enabled = true;
                        txtKolicina.Text = "";
                        return;
                    }

                }

                int n = dgOpremaZaPrebacit.Rows.Add();
                dgOpremaZaPrebacit.Rows[n].Cells[0].Value = Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value);
                dgOpremaZaPrebacit.Rows[n].Cells[1].Value = Convert.ToString(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[1].Value);
                dgOpremaZaPrebacit.Rows[n].Cells[2].Value = Convert.ToString(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[2].Value);
                dgOpremaZaPrebacit.Rows[n].Cells[3].Value = Convert.ToString(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[3].Value);
                dgOpremaZaPrebacit.Rows[n].Cells[4].Value = Convert.ToInt32(txtKolicina.Text);
                dgOpremaZaPrebacit.ClearSelection();
                btnZakljuci.Enabled = true;
                txtKolicina.Text = "";

            }
        }



        #region Validacija
        private void cbxVozila_Validating(object sender, CancelEventArgs e)
        {

            if (cbxVozila.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxVozila, Globall.GetMessage("voz_req"));
            }
            else
            {
                errorProvider1.SetError(cbxVozila, "");
            }


        }




        private void txtKolicina_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKolicina.Text))
            {

                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("kol_req"));

            }

            else if (!Regex.IsMatch(txtKolicina.Text, "^[0-9]*[1-9][0-9]*$"))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtKolicina, Globall.GetMessage("num_only"));
            }

            else if (Convert.ToInt32(cbxVozilaZaPrebacit.SelectedValue) != 0 && Convert.ToInt32(txtKolicina.Text) > Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[4].Value))
            {
                e.Cancel = true;
                errorProvider1.SetError(dgZaduzenaOprema, Globall.GetMessage("nema_dovoljno"));
                cbxVozilaZaPrebacit.Visible = true;

            }

            else
            {
                errorProvider1.SetError(txtKolicina, "");
                errorProvider1.SetError(dgZaduzenaOprema, "");

            }
        }

        private void cbxVozilaZaPrebacit_Validating_1(object sender, CancelEventArgs e)
        {
            if (cbxVozilaZaPrebacit.SelectedIndex == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(cbxVozilaZaPrebacit, Globall.GetMessage("voz_req"));
            }
            else
            {
                errorProvider1.SetError(cbxVozilaZaPrebacit, "");
            }
        }

        private void dgOpremaZaPrebacit_Validating(object sender, CancelEventArgs e)
        {



            foreach (DataGridViewRow row in dgOpremaZaPrebacit.Rows)
            {
                if (Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[0].Value) == Convert.ToInt32(row.Cells["IdArtikla"].Value))
                {
                    if (Convert.ToInt32(dgZaduzenaOprema.Rows[dgZaduzenaOprema.CurrentRow.Index].Cells[4].Value) < Convert.ToInt32(row.Cells["ZaduzenaKolicina"].Value) + Convert.ToInt32(txtKolicina.Text))
                    {
                        e.Cancel = true;
                        errorProvider1.SetError(dgZaduzenaOprema, Globall.GetMessage("nema_dovoljno"));
                        dgZaduzenaOprema.Visible = true;
                    }


                    else
                    {
                        errorProvider1.SetError(dgZaduzenaOprema, "");
                    }
                }
            }
        }


        private void dgZaduzenaOprema_Validating(object sender, CancelEventArgs e)
        {
            if (dgZaduzenaOprema.Rows.Count == 0)
            {
                e.Cancel = true;
                errorProvider1.SetError(dgZaduzenaOprema, Globall.GetMessage("voz_nema"));

            }
            else
            {
                errorProvider1.SetError(dgZaduzenaOprema, "");
            }

        }



        #endregion

        private void txtSifra_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(txtSifra.Text, "^[0-9]*$"))
            {
                errorProvider1.SetError(txtSifra, Globall.GetMessage("num_only"));
            }

            else
            {
                errorProvider1.SetError(txtSifra, "");
            }
            
            TextBox ak = sender as TextBox;
            string ar = ak.Text as string;

            if (ak.Text == "")
            {
                HttpResponseMessage httpResponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(cbxVozila.SelectedValue));
                if (httpResponse.IsSuccessStatusCode)
                {
                    dgZaduzenaOprema.DataSource = httpResponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
                    dgZaduzenaOprema.ClearSelection();
                }
            }

            List<ZaduzenaOpremaDTO> artikli = new List<ZaduzenaOpremaDTO>();
            List<ZaduzenaOpremaDTO> listaArtikala = new List<ZaduzenaOpremaDTO>();

            HttpResponseMessage zadresponse = zaduzenaStavkeService.GetActionResponse("GetZaduzenaOpremaByVozilo", Convert.ToString(cbxVozila.SelectedValue));
            if (zadresponse.IsSuccessStatusCode)
            {
                artikli = zadresponse.Content.ReadAsAsync<List<ZaduzenaOpremaDTO>>().Result;
            }

            foreach (ZaduzenaOpremaDTO item in artikli)
            {
                if (Regex.IsMatch(item.SifraArtikla, ar, RegexOptions.IgnoreCase))
                {
                    listaArtikala.Add(item);
                }
            }

            dgZaduzenaOprema.DataSource = listaArtikala;
        }    
    }
}
