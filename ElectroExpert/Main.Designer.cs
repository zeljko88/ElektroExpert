﻿namespace ElectroExpert
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtDashboard = new MetroFramework.Controls.MetroTile();
            this.mtIzlaz = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // mtDashboard
            // 
            this.mtDashboard.ActiveControl = null;
            this.mtDashboard.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtDashboard.Location = new System.Drawing.Point(497, 137);
            this.mtDashboard.MinimumSize = new System.Drawing.Size(335, 188);
            this.mtDashboard.Name = "mtDashboard";
            this.mtDashboard.Size = new System.Drawing.Size(335, 188);
            this.mtDashboard.TabIndex = 6;
            this.mtDashboard.Text = "Menu";
            this.mtDashboard.TileImage = global::ElectroExpert.Properties.Resources.if_website_103800;
            this.mtDashboard.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtDashboard.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtDashboard.UseSelectable = true;
            this.mtDashboard.UseTileImage = true;
            this.mtDashboard.Click += new System.EventHandler(this.mtDashboard_Click);
            // 
            // mtIzlaz
            // 
            this.mtIzlaz.ActiveControl = null;
            this.mtIzlaz.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mtIzlaz.BackColor = System.Drawing.Color.Red;
            this.mtIzlaz.Location = new System.Drawing.Point(497, 347);
            this.mtIzlaz.MaximumSize = new System.Drawing.Size(335, 163);
            this.mtIzlaz.Name = "mtIzlaz";
            this.mtIzlaz.Size = new System.Drawing.Size(335, 160);
            this.mtIzlaz.TabIndex = 7;
            this.mtIzlaz.Text = "Izlaz";
            this.mtIzlaz.TileImage = global::ElectroExpert.Properties.Resources.if_exit_32261;
            this.mtIzlaz.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtIzlaz.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.mtIzlaz.UseSelectable = true;
            this.mtIzlaz.UseTileImage = true;
            this.mtIzlaz.Click += new System.EventHandler(this.mtIzlaz_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1231, 665);
            this.Controls.Add(this.mtIzlaz);
            this.Controls.Add(this.mtDashboard);
            this.IsMdiContainer = true;
            this.Name = "Main";
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Elektro Expert d.o.o.";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTile mtDashboard;
        private MetroFramework.Controls.MetroTile mtIzlaz;
    }
}