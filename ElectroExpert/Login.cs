﻿using ElectroExpert_API.Models;
using ElectroExpert_API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectroExpert
{
    public partial class Login : MetroFramework.Forms.MetroForm
    {
        private WebApiHelper korisniciService = new WebApiHelper("http://localhost:63632/", "api/Korisnik");
        public Login()
        {
            InitializeComponent();
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrijava_Click(object sender, EventArgs e)
        {

            if (this.ValidateChildren())
            {
                HttpResponseMessage response = korisniciService.GetResponse(textBoxKorisnickoIme.Text);

                if (response.IsSuccessStatusCode)
                {
                    Korisnik k = response.Content.ReadAsAsync<Korisnik>().Result;
                    if (UIHelper.GenerateHash(textBoxLozinka.Text, k.LozinkaSalt) == k.LozinkaHash)
                    {
                        Globall.prijavljeniKorisnik = k;
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                        MessageBox.Show(Globall.GetMessage("login_pass_err"), Globall.GetMessage("warning"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Greska - Error Code: " + response.StatusCode + "- Message:" + response.ReasonPhrase);
                }
            }
        }

        private void textBoxKorisnickoIme_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxKorisnickoIme.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(textBoxKorisnickoIme, Globall.GetMessage("empty"));

            }
            

            else
            {
                errorProvider1.SetError(textBoxKorisnickoIme, "");
            }
        }

        private void textBoxLozinka_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxLozinka.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(textBoxLozinka, Globall.GetMessage("empty"));

            }
            
            else
            {
                errorProvider1.SetError(textBoxLozinka, "");
            }
        }
    }
}
