﻿using ElectroExpert_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


using System.Linq;
using System.Web;

namespace ElectroExpert_API.DAL
{
    public class DataContext:DbContext
    {

        public DataContext() : base("name=spojiSe")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Nalog>()
               .HasMany<Usluga>(s => s.Usluge)
               .WithMany(c => c.Nalozi)
               .Map(cs =>
               {
                   cs.MapLeftKey("NalogId");
                   cs.MapRightKey("UslugaId");
                   cs.ToTable("NalogUsluga");
               });

        }
        public DbSet<Artikal> Artikli { get; set; }
        public DbSet<Vrsta> Vrste { get; set; }
        public DbSet<Korisnik> Korisnici { get; set; }

        public DbSet<Ulaz> Ulazi { get; set; }
        public DbSet<Radnik> Radnici { get; set; }
        
        public DbSet<UgradjenaOprema> UgradjenaOprema { get; set; }

        public DbSet<UgradjenaOpremaStavke> UgradjenaOpremaStavke { get; set; }
        public DbSet<Usluga> Usluge { get; set; }

        public DbSet<JedinicaMjere> JedinicaMjere { get; set; }
        public DbSet<VrstaUsluge> VrstaUsluge { get; set; }
        public DbSet<Kupac> Kupac { get; set; }

        public DbSet<Predracun> Predracun { get; set; }
        public DbSet<PredracunStavke> PredracunStavke { get; set; }
        public DbSet<ZaduzenaOprema> ZaduzenaOprema { get; set; }
       

        public DbSet<UlazStavke> UlazStavke { get; set; }
        public DbSet<Dobavljac> Dobavljaci { get; set; }
        public DbSet<StanjeSkladista> StanjeSkladista { get; set; }
        public DbSet<ZaduzenaUlaz> ZaduzenaUlaz { get; set; }
        public DbSet<ZaduzenaUlazStavke> ZaduzenaUlazStavke { get; set; }

        public DbSet<KorisniciUloge> KorisniciUloge { get; set; }

        public DbSet<Uloga> Uloga { get; set; }

        public System.Data.Entity.DbSet<ElectroExpert_API.Models.Nalog> Nalogs { get; set; }

        public System.Data.Entity.DbSet<ElectroExpert_API.Models.Skladiste> Skladistes { get; set; }

        public System.Data.Entity.DbSet<ElectroExpert_API.Models.Vozilo> Voziloes { get; set; }
    }
}