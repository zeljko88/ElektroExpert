﻿using ElectroExpert_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ElectroExpert_API.Util
{
    public class WebApiHelper
    {
        private HttpClient client { get; set; }

        private string route { get; set; }

        public WebApiHelper(string uri, string route)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            this.route = route;

        }

        public HttpResponseMessage GetResponse()
        {
            return client.GetAsync(route).Result;
        }
        public HttpResponseMessage PostResponse(Object newObject)
        {
            return client.PostAsJsonAsync(route, newObject).Result;
        }
        public HttpResponseMessage PostResponse(List<Object> newObject)
        {
            return client.PostAsJsonAsync(route, newObject).Result;
        }

        public HttpResponseMessage PostResponse(UgradjenaOprema newObject, List<Usluga> lista)
        {
            return client.PostAsJsonAsync(route, newObject).Result;
        }

        public HttpResponseMessage GetActionResponse(string action, Object newObject)
        {
            return client.PostAsJsonAsync(route, newObject).Result;
        }

        public HttpResponseMessage GetResponse(int id)
        {
            return client.GetAsync(route + "/" + id).Result;
        }
        public HttpResponseMessage GetResponse(string username)
        {
            return client.GetAsync(route + "/" + username).Result;
        }

        public HttpResponseMessage GetActionResponse(string action, string parameter = "")
        {
            return client.GetAsync(route + "/" + action + "/" + parameter).Result;
        }

        public HttpResponseMessage GetActionResponse(string action, string parameter = "", string v2="", string v3 ="")
        {
            return client.GetAsync(route + "/" + action + "/" + parameter +"/"+v2 + "/" + v3).Result;
        }


        public HttpResponseMessage GetActionResponse(string action)
        {
            return client.GetAsync(route + "/" + action).Result;
        }

        public HttpResponseMessage PostActionResponse(string action, Object newObject)
        {
            return client.PostAsJsonAsync(route + "/" + action, newObject).Result;
        }

        public HttpResponseMessage PostActionResponse(string action, string parameter, Object newObject)
        {
            return client.PostAsJsonAsync(route + "/" + action + "/" + parameter, newObject).Result;
        }

        public HttpResponseMessage GetActionResponse(string action, string v2, string v3)
        {
            return client.GetAsync(route + "/" + action + "/" + v2 + "/" + v3).Result;
        }

    
    }
}