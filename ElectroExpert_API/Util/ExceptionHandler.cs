﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Util
{
    public class ExceptionHandler
    {

        public static string HandleException(Exception ex)
        {
            SqlException error = ex.InnerException as SqlException;

            switch (error.Number)
            {
                case 2601:
                    return getConstraintExceptionMessage(error);
                default:
                    return error.Message + " (" + error.Number + ")";
            }

        }
        /* "Cannot insert duplicate key row in object 'dbo.Korisnik' with unique index 'IX_KorisnickoIme'. 
            The duplicate key value is (goran).\r\nThe statement has been terminated."	string*/



        private static string getConstraintExceptionMessage(SqlException error)
        {
            string newMessage = error.Message;

            int startIndex = newMessage.IndexOf("'I");
            int endIndex = newMessage.IndexOf("'", startIndex + 1);

            if (startIndex > 0 && endIndex > 0)
            {
                string constraintName = newMessage.Substring(startIndex + 1, endIndex - startIndex - 1);

                if (constraintName == "IX_Sifra")
                    newMessage = "sifra_con";
                else if (constraintName == "IX_Email")
                    newMessage = "email_con";
                else if (constraintName == "IX_Sifra")
                    newMessage = "sifra_con";
                else if (constraintName == "IX_Naziv")
                    newMessage = "naziv_con";
                else if (constraintName == "IX_BrojNaloga")
                    newMessage = "brNal_con";

            }

            return newMessage;
        }


    }
}