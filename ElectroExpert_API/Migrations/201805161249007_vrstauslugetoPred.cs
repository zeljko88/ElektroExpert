namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vrstauslugetoPred : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Predracun", "VrstaUslugeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Predracun", "VrstaUslugeId");
            AddForeignKey("dbo.Predracun", "VrstaUslugeId", "dbo.VrstaUsluge", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Predracun", "VrstaUslugeId", "dbo.VrstaUsluge");
            DropIndex("dbo.Predracun", new[] { "VrstaUslugeId" });
            DropColumn("dbo.Predracun", "VrstaUslugeId");
        }
    }
}
