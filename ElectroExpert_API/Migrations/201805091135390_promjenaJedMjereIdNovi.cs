namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class promjenaJedMjereIdNovi : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Usluga", new[] { "JedinicaMjere_Id" });
            RenameColumn(table: "dbo.Usluga", name: "JedinicaMjere_Id", newName: "JedinicaMjereId");
            AlterColumn("dbo.Usluga", "JedinicaMjereId", c => c.Int(nullable: false));
            CreateIndex("dbo.Usluga", "JedinicaMjereId");
            DropColumn("dbo.Usluga", "JedMjereId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Usluga", "JedMjereId", c => c.Int(nullable: false));
            DropIndex("dbo.Usluga", new[] { "JedinicaMjereId" });
            AlterColumn("dbo.Usluga", "JedinicaMjereId", c => c.Int());
            RenameColumn(table: "dbo.Usluga", name: "JedinicaMjereId", newName: "JedinicaMjere_Id");
            CreateIndex("dbo.Usluga", "JedinicaMjere_Id");
        }
    }
}
