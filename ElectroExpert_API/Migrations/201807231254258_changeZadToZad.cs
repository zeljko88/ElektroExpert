namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeZadToZad : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ZaduzenaUlazStavke", new[] { "Artikal_Id" });
            RenameColumn(table: "dbo.ZaduzenaUlazStavke", name: "Artikal_Id", newName: "ArtikalId");
            AlterColumn("dbo.ZaduzenaUlazStavke", "ArtikalId", c => c.Int(nullable: false));
            CreateIndex("dbo.ZaduzenaUlazStavke", "ArtikalId");
            DropColumn("dbo.ZaduzenaUlazStavke", "UlazId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ZaduzenaUlazStavke", "UlazId", c => c.Int(nullable: false));
            DropIndex("dbo.ZaduzenaUlazStavke", new[] { "ArtikalId" });
            AlterColumn("dbo.ZaduzenaUlazStavke", "ArtikalId", c => c.Int());
            RenameColumn(table: "dbo.ZaduzenaUlazStavke", name: "ArtikalId", newName: "Artikal_Id");
            CreateIndex("dbo.ZaduzenaUlazStavke", "Artikal_Id");
        }
    }
}
