namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addZaduzenaUlaz : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ZaduzenaUlaz",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VoziloId = c.Int(nullable: false),
                        Datum = c.DateTime(nullable: false),
                        KorisnikId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Korisnik", t => t.KorisnikId)
                .ForeignKey("dbo.Vozilo", t => t.VoziloId)
                .Index(t => t.VoziloId)
                .Index(t => t.KorisnikId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZaduzenaUlaz", "VoziloId", "dbo.Vozilo");
            DropForeignKey("dbo.ZaduzenaUlaz", "KorisnikId", "dbo.Korisnik");
            DropIndex("dbo.ZaduzenaUlaz", new[] { "KorisnikId" });
            DropIndex("dbo.ZaduzenaUlaz", new[] { "VoziloId" });
            DropTable("dbo.ZaduzenaUlaz");
        }
    }
}
