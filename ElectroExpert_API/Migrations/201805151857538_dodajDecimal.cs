namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodajDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Predracun", "IznosBezPdv", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Predracun", "Pdv", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Predracun", "IznosSaPdv", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Predracun", "IznosSaPdv", c => c.Single(nullable: false));
            AlterColumn("dbo.Predracun", "Pdv", c => c.Single(nullable: false));
            AlterColumn("dbo.Predracun", "IznosBezPdv", c => c.Single(nullable: false));
        }
    }
}
