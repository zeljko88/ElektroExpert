namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeVrstaUsluge : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Usluga", "VrstaId", "dbo.Vrsta");
            DropIndex("dbo.Usluga", new[] { "VrstaId" });
            AddColumn("dbo.Usluga", "VrstaUslugeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Usluga", "VrstaUslugeId");
            AddForeignKey("dbo.Usluga", "VrstaUslugeId", "dbo.VrstaUsluge", "Id");
            DropColumn("dbo.Usluga", "VrstaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Usluga", "VrstaId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Usluga", "VrstaUslugeId", "dbo.VrstaUsluge");
            DropIndex("dbo.Usluga", new[] { "VrstaUslugeId" });
            DropColumn("dbo.Usluga", "VrstaUslugeId");
            CreateIndex("dbo.Usluga", "VrstaId");
            AddForeignKey("dbo.Usluga", "VrstaId", "dbo.Vrsta", "Id");
        }
    }
}
