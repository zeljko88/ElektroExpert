namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addKupacTOpredracun : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Predracun", "KupacId", c => c.Int(nullable: false));
            CreateIndex("dbo.Predracun", "KupacId");
            AddForeignKey("dbo.Predracun", "KupacId", "dbo.Kupac", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Predracun", "KupacId", "dbo.Kupac");
            DropIndex("dbo.Predracun", new[] { "KupacId" });
            DropColumn("dbo.Predracun", "KupacId");
        }
    }
}
