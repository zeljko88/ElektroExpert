namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUnique : DbMigration
    {
        public override void Up()
        {
           
            CreateIndex("dbo.Usluga", "Sifra", unique: true);
          
        }
        
        public override void Down()
        {
           
            DropIndex("dbo.Usluga", new[] { "Sifra" });
           
        }
    }
}
