namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmigrationzaduzenaStavke : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ZaduzenaOpremaStavke",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ZaduzenaOpremaId = c.Int(nullable: false),
                        ArtikalId = c.Int(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        KolicinaNaStanju = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikal", t => t.ArtikalId)
                .ForeignKey("dbo.ZaduzenaOprema", t => t.ZaduzenaOpremaId)
                .Index(t => t.ZaduzenaOpremaId)
                .Index(t => t.ArtikalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZaduzenaOpremaStavke", "ZaduzenaOpremaId", "dbo.ZaduzenaOprema");
            DropForeignKey("dbo.ZaduzenaOpremaStavke", "ArtikalId", "dbo.Artikal");
            DropIndex("dbo.ZaduzenaOpremaStavke", new[] { "ArtikalId" });
            DropIndex("dbo.ZaduzenaOpremaStavke", new[] { "ZaduzenaOpremaId" });
            DropTable("dbo.ZaduzenaOpremaStavke");
        }
    }
}
