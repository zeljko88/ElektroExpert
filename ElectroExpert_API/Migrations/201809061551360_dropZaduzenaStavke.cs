namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropZaduzenaStavke : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ZaduzenaOpremaStavke", "ArtikalId", "dbo.Artikal");
            DropForeignKey("dbo.ZaduzenaOpremaStavke", "ZaduzenaOpremaId", "dbo.ZaduzenaOprema");
            DropIndex("dbo.ZaduzenaOpremaStavke", new[] { "ZaduzenaOpremaId" });
            DropIndex("dbo.ZaduzenaOpremaStavke", new[] { "ArtikalId" });
            DropTable("dbo.ZaduzenaOpremaStavke");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ZaduzenaOpremaStavke",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ZaduzenaOpremaId = c.Int(nullable: false),
                        ArtikalId = c.Int(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        KolicinaNaStanju = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ZaduzenaOpremaStavke", "ArtikalId");
            CreateIndex("dbo.ZaduzenaOpremaStavke", "ZaduzenaOpremaId");
            AddForeignKey("dbo.ZaduzenaOpremaStavke", "ZaduzenaOpremaId", "dbo.ZaduzenaOprema", "Id");
            AddForeignKey("dbo.ZaduzenaOpremaStavke", "ArtikalId", "dbo.Artikal", "Id");
        }
    }
}
