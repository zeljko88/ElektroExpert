namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeZaduzena : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ZaduzenaOprema", "ArtikalId", c => c.Int(nullable: false));
            AddColumn("dbo.ZaduzenaOprema", "Kolicina", c => c.Int(nullable: false));
            CreateIndex("dbo.ZaduzenaOprema", "ArtikalId");
            AddForeignKey("dbo.ZaduzenaOprema", "ArtikalId", "dbo.Artikal", "Id");
            DropColumn("dbo.ZaduzenaOprema", "PrijenosSaVozila");
            DropColumn("dbo.ZaduzenaOprema", "DatumZaduzenja");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ZaduzenaOprema", "DatumZaduzenja", c => c.DateTime(nullable: false));
            AddColumn("dbo.ZaduzenaOprema", "PrijenosSaVozila", c => c.Boolean());
            DropForeignKey("dbo.ZaduzenaOprema", "ArtikalId", "dbo.Artikal");
            DropIndex("dbo.ZaduzenaOprema", new[] { "ArtikalId" });
            DropColumn("dbo.ZaduzenaOprema", "Kolicina");
            DropColumn("dbo.ZaduzenaOprema", "ArtikalId");
        }
    }
}
