namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stanjeskladista : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StanjeSkladista",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SkladisteId = c.Int(nullable: false),
                        ArtikalId = c.Int(nullable: false),
                        Kolicina = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikal", t => t.ArtikalId)
                .ForeignKey("dbo.Skladiste", t => t.SkladisteId)
                .Index(t => t.SkladisteId)
                .Index(t => t.ArtikalId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StanjeSkladista", "SkladisteId", "dbo.Skladiste");
            DropForeignKey("dbo.StanjeSkladista", "ArtikalId", "dbo.Artikal");
            DropIndex("dbo.StanjeSkladista", new[] { "ArtikalId" });
            DropIndex("dbo.StanjeSkladista", new[] { "SkladisteId" });
            DropTable("dbo.StanjeSkladista");
        }
    }
}
