namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodavanjeuUgradjenaStavke : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UgradjenaOpremaStavke",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArtikalId = c.Int(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        UgradjenaOpremaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikal", t => t.ArtikalId)
                .ForeignKey("dbo.UgradjenaOprema", t => t.UgradjenaOpremaId)
                .Index(t => t.ArtikalId)
                .Index(t => t.UgradjenaOpremaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UgradjenaOpremaStavke", "UgradjenaOpremaId", "dbo.UgradjenaOprema");
            DropForeignKey("dbo.UgradjenaOpremaStavke", "ArtikalId", "dbo.Artikal");
            DropIndex("dbo.UgradjenaOpremaStavke", new[] { "UgradjenaOpremaId" });
            DropIndex("dbo.UgradjenaOpremaStavke", new[] { "ArtikalId" });
            DropTable("dbo.UgradjenaOpremaStavke");
        }
    }
}
