namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropFaktura : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Faktura");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Faktura",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Datum = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
