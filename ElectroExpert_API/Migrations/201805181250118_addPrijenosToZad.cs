namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPrijenosToZad : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ZaduzenaOprema", "PrijenosSaVozila", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ZaduzenaOprema", "PrijenosSaVozila");
        }
    }
}
