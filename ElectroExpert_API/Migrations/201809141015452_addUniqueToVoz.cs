namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUniqueToVoz : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Vozilo", "Naziv", c => c.String(maxLength: 50));
            
            CreateIndex("dbo.Vozilo", "Naziv", unique: true);
          
        }
        
        public override void Down()
        {
         
            DropIndex("dbo.Vozilo", new[] { "Naziv" });
        
            AlterColumn("dbo.Vozilo", "Naziv", c => c.String());
        }
    }
}
