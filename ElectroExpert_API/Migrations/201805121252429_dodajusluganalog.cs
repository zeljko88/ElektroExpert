namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodajusluganalog : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UslugaNalog", newName: "NalogUsluga");
            RenameColumn(table: "dbo.NalogUsluga", name: "Usluga_Id", newName: "UslugaId");
            RenameColumn(table: "dbo.NalogUsluga", name: "Nalog_Id", newName: "NalogId");
            RenameIndex(table: "dbo.NalogUsluga", name: "IX_Nalog_Id", newName: "IX_NalogId");
            RenameIndex(table: "dbo.NalogUsluga", name: "IX_Usluga_Id", newName: "IX_UslugaId");
            DropPrimaryKey("dbo.NalogUsluga");
            AddPrimaryKey("dbo.NalogUsluga", new[] { "NalogId", "UslugaId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.NalogUsluga");
            AddPrimaryKey("dbo.NalogUsluga", new[] { "Usluga_Id", "Nalog_Id" });
            RenameIndex(table: "dbo.NalogUsluga", name: "IX_UslugaId", newName: "IX_Usluga_Id");
            RenameIndex(table: "dbo.NalogUsluga", name: "IX_NalogId", newName: "IX_Nalog_Id");
            RenameColumn(table: "dbo.NalogUsluga", name: "NalogId", newName: "Nalog_Id");
            RenameColumn(table: "dbo.NalogUsluga", name: "UslugaId", newName: "Usluga_Id");
            RenameTable(name: "dbo.NalogUsluga", newName: "UslugaNalog");
        }
    }
}
