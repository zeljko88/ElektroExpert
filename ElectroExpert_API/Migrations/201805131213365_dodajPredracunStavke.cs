namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodajPredracunStavke : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PredracunStavke",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UslugaId = c.Int(nullable: false),
                        JedinicaMjereId = c.Int(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        CijenaBezPdv = c.Double(nullable: false),
                        CijenaSaPdv = c.Double(nullable: false),
                        Pdv = c.Double(nullable: false),
                        IznosBezPdv = c.Double(nullable: false),
                        IznosSaPdv = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JedinicaMjere", t => t.JedinicaMjereId)
                .ForeignKey("dbo.Usluga", t => t.UslugaId)
                .Index(t => t.UslugaId)
                .Index(t => t.JedinicaMjereId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PredracunStavke", "UslugaId", "dbo.Usluga");
            DropForeignKey("dbo.PredracunStavke", "JedinicaMjereId", "dbo.JedinicaMjere");
            DropIndex("dbo.PredracunStavke", new[] { "JedinicaMjereId" });
            DropIndex("dbo.PredracunStavke", new[] { "UslugaId" });
            DropTable("dbo.PredracunStavke");
        }
    }
}
