namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUnique1 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Nalog", "BrojNaloga", unique: true);
         
        }
        
        public override void Down()
        {
         
            DropIndex("dbo.Nalog", new[] { "BrojNaloga" });
        }
    }
}
