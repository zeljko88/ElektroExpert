namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nesto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Predracun", "IznosBezPdv", c => c.Single(nullable: false));
            AddColumn("dbo.Predracun", "Pdv", c => c.Single(nullable: false));
            AddColumn("dbo.Predracun", "IznosSaPdv", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Predracun", "IznosSaPdv");
            DropColumn("dbo.Predracun", "Pdv");
            DropColumn("dbo.Predracun", "IznosBezPdv");
        }
    }
}
