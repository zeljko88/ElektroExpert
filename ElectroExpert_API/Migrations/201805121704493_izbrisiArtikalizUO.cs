namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class izbrisiArtikalizUO : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UgradjenaOprema", "ArtikalId", "dbo.Artikal");
            DropIndex("dbo.UgradjenaOprema", new[] { "ArtikalId" });
            DropColumn("dbo.UgradjenaOprema", "ArtikalId");
            DropColumn("dbo.UgradjenaOprema", "Kolicina");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UgradjenaOprema", "Kolicina", c => c.Int(nullable: false));
            AddColumn("dbo.UgradjenaOprema", "ArtikalId", c => c.Int(nullable: false));
            CreateIndex("dbo.UgradjenaOprema", "ArtikalId");
            AddForeignKey("dbo.UgradjenaOprema", "ArtikalId", "dbo.Artikal", "Id");
        }
    }
}
