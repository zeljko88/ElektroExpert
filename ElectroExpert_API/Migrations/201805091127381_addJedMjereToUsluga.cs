namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addJedMjereToUsluga : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usluga", "JedMjereId", c => c.Int(nullable: false));
            AddColumn("dbo.Usluga", "JedinicaMjere_Id", c => c.Int());
            CreateIndex("dbo.Usluga", "JedinicaMjere_Id");
            AddForeignKey("dbo.Usluga", "JedinicaMjere_Id", "dbo.JedinicaMjere", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Usluga", "JedinicaMjere_Id", "dbo.JedinicaMjere");
            DropIndex("dbo.Usluga", new[] { "JedinicaMjere_Id" });
            DropColumn("dbo.Usluga", "JedinicaMjere_Id");
            DropColumn("dbo.Usluga", "JedMjereId");
        }
    }
}
