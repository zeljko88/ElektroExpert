namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeboolToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ZaduzenaUlaz", "SaVozila", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ZaduzenaUlaz", "SaVozila", c => c.Boolean(nullable: false));
        }
    }
}
