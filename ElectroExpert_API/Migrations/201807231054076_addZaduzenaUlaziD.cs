namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addZaduzenaUlaziD : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ZaduzenaOprema", new[] { "ZaduzenaUlaz_Id" });
            RenameColumn(table: "dbo.ZaduzenaOprema", name: "ZaduzenaUlaz_Id", newName: "ZaduzenaUlazId");
            AlterColumn("dbo.ZaduzenaOprema", "ZaduzenaUlazId", c => c.Int(nullable: false));
            CreateIndex("dbo.ZaduzenaOprema", "ZaduzenaUlazId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ZaduzenaOprema", new[] { "ZaduzenaUlazId" });
            AlterColumn("dbo.ZaduzenaOprema", "ZaduzenaUlazId", c => c.Int());
            RenameColumn(table: "dbo.ZaduzenaOprema", name: "ZaduzenaUlazId", newName: "ZaduzenaUlaz_Id");
            CreateIndex("dbo.ZaduzenaOprema", "ZaduzenaUlaz_Id");
        }
    }
}
