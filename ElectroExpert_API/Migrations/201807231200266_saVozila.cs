namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class saVozila : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ZaduzenaUlaz", "SaVozila", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ZaduzenaUlaz", "SaVozila");
        }
    }
}
