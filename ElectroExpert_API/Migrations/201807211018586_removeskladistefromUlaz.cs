namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeskladistefromUlaz : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Ulaz", "SkladisteId", "dbo.Skladiste");
            DropIndex("dbo.Ulaz", new[] { "SkladisteId" });
            DropColumn("dbo.Ulaz", "SkladisteId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Ulaz", "SkladisteId", c => c.Int(nullable: false));
            CreateIndex("dbo.Ulaz", "SkladisteId");
            AddForeignKey("dbo.Ulaz", "SkladisteId", "dbo.Skladiste", "Id");
        }
    }
}
