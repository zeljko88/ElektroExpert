namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPredracunUsluga : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kupac",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ime = c.String(),
                        Adresa = c.String(),
                        Grad = c.String(),
                        IdBroj = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VrstaUsluge",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Predracun", "Broj", c => c.String());
            AddColumn("dbo.Predracun", "DatumOd", c => c.DateTime(nullable: false));
            AddColumn("dbo.Predracun", "DatumDo", c => c.DateTime(nullable: false));
            AddColumn("dbo.Usluga", "VrstaId", c => c.Int(nullable: false));
            CreateIndex("dbo.Usluga", "VrstaId");
            AddForeignKey("dbo.Usluga", "VrstaId", "dbo.Vrsta", "Id");
            DropColumn("dbo.Predracun", "Datum");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Predracun", "Datum", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.Usluga", "VrstaId", "dbo.Vrsta");
            DropIndex("dbo.Usluga", new[] { "VrstaId" });
            DropColumn("dbo.Usluga", "VrstaId");
            DropColumn("dbo.Predracun", "DatumDo");
            DropColumn("dbo.Predracun", "DatumOd");
            DropColumn("dbo.Predracun", "Broj");
            DropTable("dbo.VrstaUsluge");
            DropTable("dbo.Kupac");
        }
    }
}
