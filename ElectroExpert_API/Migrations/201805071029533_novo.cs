namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class novo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ZaduzenaOprema", "ArtikalId", "dbo.Artikal");
            DropIndex("dbo.ZaduzenaOprema", new[] { "ArtikalId" });
            DropColumn("dbo.ZaduzenaOprema", "ArtikalId");
            DropColumn("dbo.ZaduzenaOprema", "Kolicina");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ZaduzenaOprema", "Kolicina", c => c.Int(nullable: false));
            AddColumn("dbo.ZaduzenaOprema", "ArtikalId", c => c.Int(nullable: false));
            CreateIndex("dbo.ZaduzenaOprema", "ArtikalId");
            AddForeignKey("dbo.ZaduzenaOprema", "ArtikalId", "dbo.Artikal", "Id");
        }
    }
}
