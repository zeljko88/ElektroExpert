namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addZaduzena : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ZaduzenaOprema", "ZaduzenaUlaz_Id", c => c.Int());
            CreateIndex("dbo.ZaduzenaOprema", "ZaduzenaUlaz_Id");
            AddForeignKey("dbo.ZaduzenaOprema", "ZaduzenaUlaz_Id", "dbo.ZaduzenaUlaz", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZaduzenaOprema", "ZaduzenaUlaz_Id", "dbo.ZaduzenaUlaz");
            DropIndex("dbo.ZaduzenaOprema", new[] { "ZaduzenaUlaz_Id" });
            DropColumn("dbo.ZaduzenaOprema", "ZaduzenaUlaz_Id");
        }
    }
}
