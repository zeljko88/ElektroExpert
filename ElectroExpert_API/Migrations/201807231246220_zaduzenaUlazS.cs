namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zaduzenaUlazS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ZaduzenaUlazStavke",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UlazId = c.Int(nullable: false),
                        ZaduzenaUlazId = c.Int(nullable: false),
                        Kolicina = c.Int(nullable: false),
                        Artikal_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikal", t => t.Artikal_Id)
                .ForeignKey("dbo.ZaduzenaUlaz", t => t.ZaduzenaUlazId)
                .Index(t => t.ZaduzenaUlazId)
                .Index(t => t.Artikal_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZaduzenaUlazStavke", "ZaduzenaUlazId", "dbo.ZaduzenaUlaz");
            DropForeignKey("dbo.ZaduzenaUlazStavke", "Artikal_Id", "dbo.Artikal");
            DropIndex("dbo.ZaduzenaUlazStavke", new[] { "Artikal_Id" });
            DropIndex("dbo.ZaduzenaUlazStavke", new[] { "ZaduzenaUlazId" });
            DropTable("dbo.ZaduzenaUlazStavke");
        }
    }
}
