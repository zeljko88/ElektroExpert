namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSifraToVrstaUsluge : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VrstaUsluge", "Sifra", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.VrstaUsluge", "Sifra");
        }
    }
}
