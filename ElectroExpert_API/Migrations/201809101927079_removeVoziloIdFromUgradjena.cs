namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeVoziloIdFromUgradjena : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UgradjenaOprema", "VoziloId", "dbo.Vozilo");
            DropIndex("dbo.UgradjenaOprema", new[] { "VoziloId" });
            DropColumn("dbo.UgradjenaOprema", "VoziloId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UgradjenaOprema", "VoziloId", c => c.Int(nullable: false));
            CreateIndex("dbo.UgradjenaOprema", "VoziloId");
            AddForeignKey("dbo.UgradjenaOprema", "VoziloId", "dbo.Vozilo", "Id");
        }
    }
}
