namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class novo1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UlazStavke", "KolicinaNaStanju");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UlazStavke", "KolicinaNaStanju", c => c.Int(nullable: false));
        }
    }
}
