namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeZadUlazIDfromZaduzena : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ZaduzenaOprema", "ZaduzenaUlazId", "dbo.ZaduzenaUlaz");
            DropIndex("dbo.ZaduzenaOprema", new[] { "ZaduzenaUlazId" });
            DropColumn("dbo.ZaduzenaOprema", "ZaduzenaUlazId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ZaduzenaOprema", "ZaduzenaUlazId", c => c.Int(nullable: false));
            CreateIndex("dbo.ZaduzenaOprema", "ZaduzenaUlazId");
            AddForeignKey("dbo.ZaduzenaOprema", "ZaduzenaUlazId", "dbo.ZaduzenaUlaz", "Id");
        }
    }
}
