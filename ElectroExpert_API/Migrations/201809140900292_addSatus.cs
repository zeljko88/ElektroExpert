namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vozilo", "DatumIstekaReg", c => c.DateTime(nullable: false));
            AddColumn("dbo.Vozilo", "RegOznaka", c => c.String());
            AddColumn("dbo.Radnik", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Radnik", "Status");
            DropColumn("dbo.Vozilo", "RegOznaka");
            DropColumn("dbo.Vozilo", "DatumIstekaReg");
        }
    }
}
