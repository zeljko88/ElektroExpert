namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUloge : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KorisniciUloge",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        KorisnikId = c.Int(nullable: false),
                        UlogaId = c.Int(nullable: false),
                        DatumIzmjene = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Korisnik", t => t.KorisnikId)
                .ForeignKey("dbo.Uloga", t => t.UlogaId)
                .Index(t => t.KorisnikId)
                .Index(t => t.UlogaId);
            
            CreateTable(
                "dbo.Uloga",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KorisniciUloge", "UlogaId", "dbo.Uloga");
            DropForeignKey("dbo.KorisniciUloge", "KorisnikId", "dbo.Korisnik");
            DropIndex("dbo.KorisniciUloge", new[] { "UlogaId" });
            DropIndex("dbo.KorisniciUloge", new[] { "KorisnikId" });
            DropTable("dbo.Uloga");
            DropTable("dbo.KorisniciUloge");
        }
    }
}
