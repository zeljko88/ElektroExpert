namespace ElectroExpert_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stavkepredracun : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PredracunStavke", "PredracunId", c => c.Int(nullable: false));
            CreateIndex("dbo.PredracunStavke", "PredracunId");
            AddForeignKey("dbo.PredracunStavke", "PredracunId", "dbo.Predracun", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PredracunStavke", "PredracunId", "dbo.Predracun");
            DropIndex("dbo.PredracunStavke", new[] { "PredracunId" });
            DropColumn("dbo.PredracunStavke", "PredracunId");
        }
    }
}
