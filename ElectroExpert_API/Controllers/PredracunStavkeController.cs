﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class PredracunStavkeController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<PredracunStavke, PredracunStavkeDTO>> AsPredracunStavkeDTO =
      x => new PredracunStavkeDTO
      {
          NazivUsluge=x.Usluga.Naziv,
          CijenaBezPdv=x.CijenaBezPdv,
          CijenaSaPdv=x.CijenaSaPdv,
          IznosSaPdv=x.IznosSaPdv,
          IznosBezPdv=x.IznosBezPdv,
          JedMjere=x.JedinicaMjere.Naziv,
          Kolicina=x.Kolicina,
          Pdv=x.Pdv




      };

        // GET: api/PredracunStavke
        public IQueryable<PredracunStavke> GetPredracunStavke()
        {
            return db.PredracunStavke;
        }

        [HttpGet]
        [Route("api/PredracunStavke/GetArtikliBaza")]
        public IQueryable<PredracunStavkeDTO> GetStavkePredracuna()
        {
            return db.PredracunStavke.Select(AsPredracunStavkeDTO);
        }



        // GET: api/PredracunStavke/5
        [ResponseType(typeof(PredracunStavke))]
        public IHttpActionResult GetPredracunStavke(int id)
        {
            PredracunStavke predracunStavke = db.PredracunStavke.Find(id);
            if (predracunStavke == null)
            {
                return NotFound();
            }

            return Ok(predracunStavke);
        }

        // PUT: api/PredracunStavke/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPredracunStavke(int id, PredracunStavke predracunStavke)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != predracunStavke.Id)
            {
                return BadRequest();
            }

            db.Entry(predracunStavke).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PredracunStavkeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PredracunStavke
        [ResponseType(typeof(PredracunStavke))]
        public IHttpActionResult PostPredracunStavke(PredracunStavke predracunStavke)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PredracunStavke.Add(predracunStavke);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = predracunStavke.Id }, predracunStavke);
        }

        // DELETE: api/PredracunStavke/5
        [ResponseType(typeof(PredracunStavke))]
        public IHttpActionResult DeletePredracunStavke(int id)
        {
            PredracunStavke predracunStavke = db.PredracunStavke.Find(id);
            if (predracunStavke == null)
            {
                return NotFound();
            }

            db.PredracunStavke.Remove(predracunStavke);
            db.SaveChanges();

            return Ok(predracunStavke);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PredracunStavkeExists(int id)
        {
            return db.PredracunStavke.Count(e => e.Id == id) > 0;
        }
    }
}