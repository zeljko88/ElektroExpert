﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;

namespace ElectroExpert_API.Controllers
{
    public class KupacController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Kupac
        public IQueryable<Kupac> GetKupac()
        {
            return db.Kupac;
        }

        // GET: api/Kupac/5
        [ResponseType(typeof(Kupac))]
        public IHttpActionResult GetKupac(int id)
        {
            Kupac kupac = db.Kupac.Find(id);
            if (kupac == null)
            {
                return NotFound();
            }

            return Ok(kupac);
        }

        // PUT: api/Kupac/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKupac(int id, Kupac kupac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kupac.Id)
            {
                return BadRequest();
            }

            db.Entry(kupac).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KupacExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Kupac
        [ResponseType(typeof(Kupac))]
        public IHttpActionResult PostKupac(Kupac kupac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Kupac.Add(kupac);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = kupac.Id }, kupac);
        }

        // DELETE: api/Kupac/5
        [ResponseType(typeof(Kupac))]
        public IHttpActionResult DeleteKupac(int id)
        {
            Kupac kupac = db.Kupac.Find(id);
            if (kupac == null)
            {
                return NotFound();
            }

            db.Kupac.Remove(kupac);
            db.SaveChanges();

            return Ok(kupac);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KupacExists(int id)
        {
            return db.Kupac.Count(e => e.Id == id) > 0;
        }
    }
}