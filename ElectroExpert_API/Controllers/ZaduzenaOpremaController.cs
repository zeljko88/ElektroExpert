﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using System.Linq.Expressions;
using ElectroExpert_API.DTO;

namespace ElectroExpert_API.Controllers
{
    public class ZaduzenaOpremaController : ApiController
    {
        private DataContext db = new DataContext();



        private static readonly Expression<Func<ZaduzenaOprema, ZaduzenaListaDTO>> AsZaduzenaListaDTO =
         x => new ZaduzenaListaDTO
         {
             ZaduzenaOpremaId = x.Id,
            
             Vozilo=x.Vozilo.Naziv
         };

        //GET: api/ZaduzenaOprema


        public IQueryable<ZaduzenaListaDTO> GetZaduzenaOprema()
        {
            return db.ZaduzenaOprema.Include(x=>x.Vozilo).Select(AsZaduzenaListaDTO);
        }

        // GET: api/ZaduzenaOprema

        //[HttpGet]
        //[Route("api/ZaduzenaOprema/GetZaduzenaOpremaByRadnik/{id}")]
        //public IQueryable<ZaduzenaOpremaDTO> GetZaduzenaOpremaByRadnik(int id)
        //{
        //    //return db.ZaduzenaOprema.Where(x=>x.RadnikId==id && x.Kolicina!=0).Select(AsZaduzenaOpremaDTO);


        //    return (from zo in db.ZaduzenaOprema
        //            join a in db.Artikli on zo.ArtikalId equals a.Id
        //            where zo.Kolicina > 0 && zo.VoziloId==id
        //            select new
        //            {
        //                artikal = a,
        //                kolicina = zo.Kolicina,
        //                naziv = a.Naziv,
        //                sifra = a.Sifra,
        //                datum = zo.DatumZaduzenja,
        //                jedMjere = a.JedinicaMjere.Naziv,

        //            } into artikli

        //            group artikli by artikli.artikal into art
        //            let kol = art.Sum(prod => prod.kolicina)
        //            select new ZaduzenaOpremaDTO
        //            {

        //                ArtikalId = art.Key.Id,
        //                SifraArtikla = art.Key.Sifra,
        //                NazivArtikla = art.Key.Naziv,
        //                KolicinaZaduzenog = kol,
        //                JedMjere=art.Key.JedinicaMjere.Naziv,
        //            });





        // GET: api/ZaduzenaOprema/5
        [ResponseType(typeof(ZaduzenaOprema))]
        public IHttpActionResult GetZaduzenaOprema(int id)
        {
            ZaduzenaOprema zaduzenaOprema = db.ZaduzenaOprema.Find(id);
            if (zaduzenaOprema == null)
            {
                return NotFound();
            }

            return Ok(zaduzenaOprema);
        }

        // PUT: api/ZaduzenaOprema/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutZaduzenaOprema(int id, ZaduzenaOprema zaduzenaOprema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zaduzenaOprema.Id)
            {
                return BadRequest();
            }

            db.Entry(zaduzenaOprema).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZaduzenaOpremaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ZaduzenaOprema
        [ResponseType(typeof(ZaduzenaOprema))]
        public IHttpActionResult PostZaduzenaOprema(List<ZaduzenaOprema> zaduzenaOprema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                    


            List<ZaduzenaOprema> zaduzenaList = db.ZaduzenaOprema.ToList();
            List<ZaduzenaOprema> novaLista = new List<ZaduzenaOprema>();

            foreach (ZaduzenaOprema ITEM in zaduzenaOprema)
            {
                int z = db.ZaduzenaOprema.Where(x => x.ArtikalId == ITEM.ArtikalId && x.VoziloId==ITEM.VoziloId).Count();
                if (z == 0)
                {

                    ZaduzenaOprema novoStanje = new ZaduzenaOprema();
                    novoStanje.ArtikalId = ITEM.ArtikalId;
                    novoStanje.VoziloId = ITEM.VoziloId;                    
                    novoStanje.Kolicina = ITEM.Kolicina;
                   
                    novaLista.Add(novoStanje);
                 
                }

            }

            if (novaLista != null)
            {
                db.ZaduzenaOprema.AddRange(novaLista);
                db.SaveChanges();
            }



            foreach (ZaduzenaOprema item in zaduzenaOprema)
            {
                foreach (ZaduzenaOprema ITEMM in zaduzenaList.Where(x=>x.ArtikalId==item.ArtikalId && x.VoziloId==item.VoziloId))
                {
                   
                        item.Kolicina += ITEMM.Kolicina;
                   
                }
                
            }

            db.SaveChanges();

            int brojac = 0;

            foreach (ZaduzenaOprema item in zaduzenaOprema)
            {
                brojac = item.Kolicina;
                foreach (StanjeSkladista ITEM in db.StanjeSkladista.Where(x => x.Kolicina > 0 && x.ArtikalId==item.ArtikalId))
                {                    

                    do
                        {
                            ITEM.Kolicina--;
                            brojac--;

                        } while (brojac > 0);


                }
                db.SaveChanges();
            }

            db.SaveChanges();


            return CreatedAtRoute("DefaultApi", new { novaLista[novaLista.Count()-1].Id },novaLista);
        }

        // DELETE: api/ZaduzenaOprema/5
        [ResponseType(typeof(ZaduzenaOprema))]
        public IHttpActionResult DeleteZaduzenaOprema(int id)
        {
            ZaduzenaOprema zaduzenaOprema = db.ZaduzenaOprema.Find(id);
            if (zaduzenaOprema == null)
            {
                return NotFound();
            }

            db.ZaduzenaOprema.Remove(zaduzenaOprema);
            db.SaveChanges();

            return Ok(zaduzenaOprema);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

      
       // [Route("api/ZaduzenaOprema/PostOprema/{idvoz}")]
       // [ResponseType(typeof(ZaduzenaOprema))]
       // public IHttpActionResult PostOprema(int idvoz, ZaduzenaOprema zo)
       // {
       //     if (!ModelState.IsValid)
       //     {
       //         return BadRequest(ModelState);
       //     }
       //     ZaduzenaOprema u = new ZaduzenaOprema();

       //     u.VoziloId = zo.VoziloId;
       //     u.PrijenosSaVozila = zo.PrijenosSaVozila;
       //     u.DatumZaduzenja = zo.DatumZaduzenja;
       //     db.ZaduzenaOprema.Add(u);
       //     db.SaveChanges();

            
        
       //     var stavke = new List<ZaduzenaOpremaStavke>();

       //     List<StanjeSkladista> ulazstavke = db.StanjeSkladista.Where(x => x.Kolicina > 0).ToList();

       //     foreach (ZaduzenaOpremaStavke item in zo.ZaduzenaOpremaStavke)
       //     {

       //         var stavka = new ZaduzenaOpremaStavke { ArtikalId = item.ArtikalId, Kolicina = item.Kolicina, ZaduzenaOpremaId = u.Id, KolicinaNaStanju = item.KolicinaNaStanju };
       //         stavke.Add(stavka);

       //     }
       //     db.ZaduzenaOpremaStavke.AddRange(stavke);

       //     db.SaveChanges();

       //     List<ZaduzenaOprema> zaduzenaOpremaPoVozilu = db.ZaduzenaOprema.Where(x => x.VoziloId == idvoz).ToList();

       //     List<ZaduzenaOpremaStavke> zos = db.ZaduzenaOpremaStavke.Where(x => x.KolicinaNaStanju > 0).ToList();
            


       //     int brojac = 0;
       //     foreach (ZaduzenaOpremaStavke item in stavke)
       //     {
       //         brojac = item.KolicinaNaStanju;
       //         foreach (ZaduzenaOprema ITEM in zaduzenaOpremaPoVozilu)
       //         {
                  
       //                 foreach (ZaduzenaOpremaStavke ITEMM in zos)
       //                 {
       //                 if (ITEMM.ZaduzenaOpremaId == ITEM.Id && ITEMM.ArtikalId == item.ArtikalId)
       //                 {
       //                     do
       //                     {
       //                         ITEMM.KolicinaNaStanju--;
       //                         brojac--;
       //                     } while (brojac > 0 && ITEMM.KolicinaNaStanju > 0);


       //                 }
       //             }


       //                 if (brojac <= 0)
       //                 {
       //                     break;
       //                 }
       //             }
                
       //     }



       //db.SaveChanges();


       //     return Ok(u);

       // }
    









        //[Route("api/ZaduzenaOprema/PostZaduzena/{id}")]
        //[ResponseType(typeof(List<ZaduzenaOprema>))]
        //public IHttpActionResult PostZaduzena(List<ZaduzenaOprema> zaduzenaOprema, int id)
        //{
        //    List<ZaduzenaOprema> lista = new List<ZaduzenaOprema>();
        //    int br = zaduzenaOprema.Count;
        //    int radnikId = zaduzenaOprema[0].VoziloId;
        //    foreach (ZaduzenaOprema item in zaduzenaOprema)
        //    {


        //        ZaduzenaOprema zo = new ZaduzenaOprema();

        //        zo.ArtikalId = item.ArtikalId;
        //        zo.Kolicina = item.Kolicina;
        //        zo.VoziloId = item.VoziloId;
        //        zo.DatumZaduzenja = DateTime.Now;
        //        lista.Add(zo);

        //    }

        //    db.ZaduzenaOprema.AddRange(lista);
        //    db.SaveChanges();



        //    List<ZaduzenaOprema> zaduzenaStavke = db.ZaduzenaOprema.Where(x => x.VoziloId == id && x.Kolicina > 0).ToList();
        //    int brojac = 0;
        //    foreach (ZaduzenaOprema item in lista)
        //    {
        //        brojac = item.Kolicina;
        //        foreach (ZaduzenaOprema ITEM in zaduzenaStavke)
        //        {
        //            if (item.ArtikalId == ITEM.ArtikalId)
        //            {
        //                do
        //                {
        //                    ITEM.Kolicina--;
        //                    brojac--;
        //                } while (brojac > 0 && ITEM.Kolicina > 0);
        //            }
        //        }
        //    }

        //    db.SaveChanges();

        //    return Ok();
        //}


            private bool ZaduzenaOpremaExists(int id)
        {
            return db.ZaduzenaOprema.Count(e => e.Id == id) > 0;
        }
    }
}