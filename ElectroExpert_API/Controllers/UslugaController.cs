﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using System.Linq.Expressions;
using ElectroExpert_API.DTO;

namespace ElectroExpert_API.Controllers
{
    public class UslugaController : ApiController
    {
        private DataContext db = new DataContext();



        private static readonly Expression<Func<Usluga, UslugaDTO>> AsUslugaDTO =
       x => new UslugaDTO
       {

           Id=x.Id,
           Cijena=x.Cijena,
           SifraUsluge=x.Sifra,
           JedMjereId=x.JedinicaMjereId,
           VrstaUslugeId=x.VrstaUslugeId,
           NazivUsluge=x.Naziv,
           VrstaUsluge=x.VrstaUsluge.Naziv,
           JedMjere=x.JedinicaMjere.Naziv
           
       };


        // GET: api/Usluga
        public IQueryable<UslugaDTO> GetUsluge()
        {
            return db.Usluge.Select(AsUslugaDTO);
        }


        [HttpGet]
        [Route("api/Usluga/GetUslugeByNalogId/{idNalog}")]
        public IQueryable<UslugaNalogDTO> GetUslugeByNalogId(int idNalog)
        {
            return (
                       from usluga in db.Usluge
                       from nalozi in usluga.Nalozi
                       where nalozi.Id == idNalog
                    

                       select new
                       {
                           usluge = usluga,
                           kolicina = usluga.Id
                       }
                       into usluge

                       group usluge by usluge.usluge into usl
                       let kol = usl.Count()

                       select new UslugaNalogDTO
                       {

                           NazivUsluge = usl.Key.Naziv,
                           Cijena=usl.Key.Cijena,
                           Kolicina=kol,
                           JedMjere=usl.Key.JedinicaMjere.Naziv,
                           SifraUsluge=usl.Key.Sifra,
                           VrstaUsluge=usl.Key.VrstaUsluge.Naziv
                          
                       });
        }


        // GET: api/Usluga/5
        [ResponseType(typeof(Usluga))]
        public IHttpActionResult GetUsluga(int id)
        {
            Usluga usluga = db.Usluge.Find(id);
            if (usluga == null)
            {
                return NotFound();
            }

            return Ok(usluga);
        }

        // PUT: api/Usluga/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUsluga(int id, Usluga usluga)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usluga.Id)
            {
                return BadRequest();
            }

            db.Entry(usluga).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UslugaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Usluga
        [ResponseType(typeof(Usluga))]
        public IHttpActionResult PostUsluga(Usluga usluga)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Usluge.Add(usluga);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = usluga.Id }, usluga);
        }

        // DELETE: api/Usluga/5
        [ResponseType(typeof(Usluga))]
        public IHttpActionResult DeleteUsluga(int id)
        {
            Usluga usluga = db.Usluge.Find(id);
            if (usluga == null)
            {
                return NotFound();
            }

            db.Usluge.Remove(usluga);
            db.SaveChanges();

            return Ok(usluga);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UslugaExists(int id)
        {
            return db.Usluge.Count(e => e.Id == id) > 0;
        }
    }
}