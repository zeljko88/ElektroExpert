﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using System.Linq.Expressions;
using ElectroExpert_API.DTO;

namespace ElectroExpert_API.Controllers
{
    public class VoziloController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<Vozilo, ListaVozilaDTO>> AsListaVozilaDTO =
          x => new ListaVozilaDTO
 {
    VoziloId=x.Id,
    DatIstekaReg=x.DatumIstekaReg,
    NazivVozila=x.Naziv,
    RegOznaka=x.RegOznaka,
    

};

        // GET: api/Vozilo
        public IQueryable<Vozilo> GetVoziloes()
        {
            return db.Voziloes;
        }

        // GET: api/Vozilo/5
        [ResponseType(typeof(Vozilo))]
        public IHttpActionResult GetVozilo(int id)
        {
            Vozilo vozilo = db.Voziloes.Find(id);
            if (vozilo == null)
            {
                return NotFound();
            }

            return Ok(vozilo);
        }

        [HttpGet]
        [Route("api/Vozilo/GetVozilaExceptId/{Id}")]
        public IQueryable<Vozilo> GetVozilaExceptId(int Id)
        {
            return db.Voziloes.Where(x => x.Id != Id);
        }


        [HttpGet]
        [Route("api/Vozilo/GetNazivVozila/{Id}")]
        public IQueryable<string> GetNazivVozila(int Id)
        {
            return db.Voziloes.Where(x => x.Id == Id).Select(x=>x.Naziv);
        }
        [HttpGet]
        [Route("api/Vozilo/GetVozila")]
        public IQueryable<ListaVozilaDTO> GetVozila()
        {
            return db.Voziloes.Select(AsListaVozilaDTO);
            
            
        }

        // PUT: api/Vozilo/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVozilo(int id, Vozilo vozilo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vozilo.Id)
            {
                return BadRequest();
            }

            db.Entry(vozilo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VoziloExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vozilo
        [ResponseType(typeof(Vozilo))]
        public IHttpActionResult PostVozilo(Vozilo vozilo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Vozilo v = db.Voziloes.Find(vozilo.Id);

            if (v != null)
            {
                v.Naziv = vozilo.Naziv;
                v.RegOznaka = vozilo.RegOznaka;
                v.DatumIstekaReg = vozilo.DatumIstekaReg;
                
                db.SaveChanges();
            }
            else
            {

                db.Voziloes.Add(vozilo);
                db.SaveChanges();
            }
            return CreatedAtRoute("DefaultApi", new { id = vozilo.Id }, vozilo);
        }

        // DELETE: api/Vozilo/5
        [ResponseType(typeof(Vozilo))]
        public IHttpActionResult DeleteVozilo(int id)
        {
            Vozilo vozilo = db.Voziloes.Find(id);
            if (vozilo == null)
            {
                return NotFound();
            }

            db.Voziloes.Remove(vozilo);
            db.SaveChanges();

            return Ok(vozilo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VoziloExists(int id)
        {
            return db.Voziloes.Count(e => e.Id == id) > 0;
        }
    }
}