﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using System.Linq.Expressions;
using ElectroExpert_API.DTO;

namespace ElectroExpert_API.Controllers
{
    public class UlazStavkeController : ApiController
    {
        private DataContext db = new DataContext();


        private static readonly Expression<Func<UlazStavke, UlazStavkeDTO>> AsUlazStavkeDTO =
           x => new UlazStavkeDTO
           {
               ArtikalId = x.ArtikalId,
               Naziv = x.Artikal.Naziv,
               
               JedMjereId = x.Artikal.JedinicaMjereId,
               JedMjere = x.Artikal.JedinicaMjere.Naziv,
               Sifra=x.Artikal.Sifra
               
           };



        // GET: api/UlazStavke
        //public IQueryable<UlazStavkeDTO> GetUlazStavke()
        //{
        //    return (from us in db.UlazStavke
        //            join a in db.Artikli on us.ArtikalId equals a.Id
        //            where us.KolicinaNaStanju>0
        //            select new
        //            {
        //                artikal = a,
        //                kolicina = us.KolicinaNaStanju,
        //                naziv = a.Naziv,
        //                sifra = a.Sifra,
        //                jedMjere=a.JedinicaMjere.Naziv,

        //            } into artikli

        //            group artikli by artikli.artikal into art
        //            let kol = art.Sum(prod => prod.kolicina)
        //            select new UlazStavkeDTO {
        //                JedMjere=art.Key.JedinicaMjere.Naziv,
        //                ArtikalId=art.Key.Id,                        
        //                Sifra=art.Key.Sifra,
        //                Naziv=art.Key.Naziv,
        //                Kolicina=kol });
        
        //}

        // GET: api/UlazStavke/5
        [ResponseType(typeof(UlazStavke))]
        public IHttpActionResult GetUlazStavke(int id)
        {
            UlazStavke ulazStavke = db.UlazStavke.Find(id);
            if (ulazStavke == null)
            {
                return NotFound();
            }

            return Ok(ulazStavke);
        }

        // PUT: api/UlazStavke/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUlazStavke(int id, UlazStavke ulazStavke)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ulazStavke.Id)
            {
                return BadRequest();
            }

            db.Entry(ulazStavke).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UlazStavkeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UlazStavke
        [ResponseType(typeof(UlazStavke))]
        public IHttpActionResult PostUlazStavke(UlazStavke ulazStavke)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UlazStavke.Add(ulazStavke);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = ulazStavke.Id }, ulazStavke);
        }

        // DELETE: api/UlazStavke/5
        [ResponseType(typeof(UlazStavke))]
        public IHttpActionResult DeleteUlazStavke(int id)
        {
            UlazStavke ulazStavke = db.UlazStavke.Find(id);
            if (ulazStavke == null)
            {
                return NotFound();
            }

            db.UlazStavke.Remove(ulazStavke);
            db.SaveChanges();

            return Ok(ulazStavke);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UlazStavkeExists(int id)
        {
            return db.UlazStavke.Count(e => e.Id == id) > 0;
        }
    }
}