﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;

namespace ElectroExpert_API.Controllers
{
    public class DobavljacController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Dobavljac
        public IQueryable<Dobavljac> GetDobavljaci()
        {
            return db.Dobavljaci;
        }

        // GET: api/Dobavljac/5
        [ResponseType(typeof(Dobavljac))]
        public IHttpActionResult GetDobavljac(int id)
        {
            Dobavljac dobavljac = db.Dobavljaci.Find(id);
            if (dobavljac == null)
            {
                return NotFound();
            }

            return Ok(dobavljac);
        }

        // PUT: api/Dobavljac/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDobavljac(int id, Dobavljac dobavljac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dobavljac.Id)
            {
                return BadRequest();
            }

            db.Entry(dobavljac).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DobavljacExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Dobavljac
        [ResponseType(typeof(Dobavljac))]
        public IHttpActionResult PostDobavljac(Dobavljac dobavljac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Dobavljaci.Add(dobavljac);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = dobavljac.Id }, dobavljac);
        }

        // DELETE: api/Dobavljac/5
        [ResponseType(typeof(Dobavljac))]
        public IHttpActionResult DeleteDobavljac(int id)
        {
            Dobavljac dobavljac = db.Dobavljaci.Find(id);
            if (dobavljac == null)
            {
                return NotFound();
            }

            db.Dobavljaci.Remove(dobavljac);
            db.SaveChanges();

            return Ok(dobavljac);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DobavljacExists(int id)
        {
            return db.Dobavljaci.Count(e => e.Id == id) > 0;
        }
    }
}