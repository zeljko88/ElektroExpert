﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class RadnikController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Radnik

        private static readonly Expression<Func<Radnik, RadnikDTO>> AsRadnikDTO =
         x => new RadnikDTO
            {
             Id = x.Id,
             Ime=x.Ime,
             Prezime=x.Prezime,
             Email=x.Email,
             Adresa=x.Adresa,
             Telefon = x.Telefon
             };

        private static readonly Expression<Func<Radnik, VoziloDTO>> AsVoziloDTO =
       x => new VoziloDTO
       {
           VoziloId = x.VoziloId,
           SefVozila = x.Ime + " " + x.Prezime,
           BrTelefona=x.Telefon,
           Email=x.Email,
           IsSef=x.SefVozila,
           NazivVozila=x.Vozilo.Naziv,
           
       };


        public IQueryable<RadnikDTO> GetRadnici()
        {
            return db.Radnici.Select(AsRadnikDTO);
        }
        [HttpGet]
        [Route("api/Radnik/GetSviRadnici/")]
        public IQueryable<RadnikDTO> GetSviRadnici()
        {
            return db.Radnici.Select(AsRadnikDTO);
        }

        [HttpGet]
        [Route("api/Radnik/GetRadniciAndVozilo/")]
        public IQueryable<VoziloDTO> GetRadniciAndVozilo()
        {
            return db.Radnici.Where(x=>x.SefVozila==true).Select(AsVoziloDTO);
        }

     

        [HttpGet]
        [Route("api/Radnik/GetRadnikById/{id}")]
        public IHttpActionResult GetRadnikById(int id)
        {
            Radnik radnik = db.Radnici
                       .Where(x => x.Id == id).SingleOrDefault();

            return Ok(radnik);
        }



        [HttpPost]
        [Route("api/Radnik/UrediRadnika/")]
        public IHttpActionResult UrediRadnika(Radnik r)
        {
            Radnik radnik = db.Radnici                
                .Where(x => x.Id == r.Id).SingleOrDefault();

            radnik.Ime = r.Ime;
            radnik.Prezime = r.Prezime;
            radnik.Email = r.Email;
            radnik.Adresa = r.Adresa;
            radnik.Telefon = r.Telefon;

            db.SaveChanges();

            return Ok(radnik); ;
        }





        [HttpGet]
        [Route("api/Radnik/GetRadniciAndVozilo/{id}")]
        public IQueryable<VoziloDTO> GetRadniciAndVozilo(int id)
        {
            return db.Radnici.Where(x => x.VoziloId==id && x.SefVozila==true).Select(AsVoziloDTO);
        }



        [HttpGet]
        [Route("api/Radnik/GetRadniciByVozilo/{id}")]
        public IQueryable<VoziloDTO> GetRadniciByVozilo(int id)
        {
            return db.Radnici.Where(x => x.VoziloId == id).Select(AsVoziloDTO);
        }


        [HttpGet]
        [Route("api/Radnik/GetRadniciExceptId/{Id}")]
        public IQueryable<RadnikDTO> GetRadniciExceptId(int Id)
        {
            return db.Radnici.Where(x => x.Id != Id).Select(AsRadnikDTO);
        }

       
        [HttpGet]
        [Route("api/Radnik/ProvjeriSef/{Id}")]
        public IHttpActionResult ProvjeriSef(int Id)
        {

            foreach (Radnik item in db.Radnici)
            {
                if (item.SefVozila == true && item.VoziloId == Id)
                {
                    return Ok();
                  
                }
            }

            return NotFound();
        }



        // GET: api/Radnik/5
        [ResponseType(typeof(Radnik))]
        public IHttpActionResult GetRadnik(int id)
        {
            Radnik radnik = db.Radnici.Find(id);
            if (radnik == null)
            {
                return NotFound();
            }

            return Ok(radnik);
        }

        // PUT: api/Radnik/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRadnik(int id, Radnik radnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != radnik.Id)
            {
                return BadRequest();
            }

            db.Entry(radnik).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RadnikExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Radnik
        [ResponseType(typeof(Radnik))]
        public IHttpActionResult PostRadnik(Radnik radnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {

                db.Radnici.Add(radnik);
                db.SaveChanges();
            }

            catch (DbUpdateException ex)
            {

                throw CreateHttpResponseException(Util.ExceptionHandler.HandleException(ex.InnerException), HttpStatusCode.Conflict);
            }



            return CreatedAtRoute("DefaultApi", new { id = radnik.Id }, radnik);
        }

        private Exception CreateHttpResponseException(string reason, HttpStatusCode code)
        {
            var response = new HttpResponseMessage
            {
                StatusCode = code,
                ReasonPhrase = reason,
                Content = new StringContent(reason)
            };

            return new HttpResponseException(response);
        }


        // DELETE: api/Radnik/5
        [ResponseType(typeof(Radnik))]
        public IHttpActionResult DeleteRadnik(int id)
        {
            Radnik radnik = db.Radnici.Find(id);
            if (radnik == null)
            {
                return NotFound();
            }

            db.Radnici.Remove(radnik);
            db.SaveChanges();

            return Ok(radnik);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RadnikExists(int id)
        {
            return db.Radnici.Count(e => e.Id == id) > 0;
        }
    }
}