﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;

namespace ElectroExpert_API.Controllers
{
    public class KorisnikController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Korisnik
        public IQueryable<Korisnik> GetKorisnici()
        {
            return db.Korisnici;
        }

        // GET: api/Korisnik/5
        [ResponseType(typeof(Korisnik))]
        public IHttpActionResult GetKorisnik(int id)
        {
            Korisnik korisnik = db.Korisnici.Find(id);
            if (korisnik == null)
            {
                return NotFound();
            }

            return Ok(korisnik);
        }
        [ResponseType(typeof(Korisnik))]
        [Route("api/Korisnik/{username}")]
        public IHttpActionResult GetKorisnik(string username)
        {
            Korisnik korisnik = db.Korisnici.Where(x => x.KorisnickoIme == username).FirstOrDefault();
            if (korisnik == null)
            {
                return NotFound();
            }

            return Ok(korisnik);
        }
        // PUT: api/Korisnik/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKorisnik(int id, Korisnik korisnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != korisnik.Id)
            {
                return BadRequest();
            }

            db.Entry(korisnik).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KorisnikExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Korisnik
        [ResponseType(typeof(Korisnik))]
        public IHttpActionResult PostKorisnik(Korisnik korisnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Korisnik k = new Korisnik();
            k.Ime = korisnik.Ime;
            k.Prezime = korisnik.Prezime;
            k.Adresa = korisnik.Adresa;
            k.Email = korisnik.Email;
            k.KorisnickoIme = korisnik.KorisnickoIme;
            k.Lozinka = korisnik.Lozinka;
            k.LozinkaHash = korisnik.LozinkaHash;
            k.LozinkaSalt = korisnik.LozinkaSalt;
            k.Status = korisnik.Status;
            k.Telefon = korisnik.Telefon;


            try
            {    

                db.Korisnici.Add(k);
                db.SaveChanges();
                
            }
            catch (DbUpdateException ex)
            {

                throw CreateHttpResponseException(Util.ExceptionHandler.HandleException(ex.InnerException), HttpStatusCode.Conflict);
            }
           
                       

        
            return CreatedAtRoute("DefaultApi", new { id = k.Id }, k);
        }

        private HttpResponseException CreateHttpResponseException(string reason, HttpStatusCode code)
        {
            var response = new HttpResponseMessage
            {
                StatusCode = code,
                ReasonPhrase = reason,
                Content = new StringContent(reason)
            };

            return new HttpResponseException(response);
        }

        // DELETE: api/Korisnik/5
        [ResponseType(typeof(Korisnik))]
        public IHttpActionResult DeleteKorisnik(int id)
        {
            Korisnik korisnik = db.Korisnici.Find(id);
            if (korisnik == null)
            {
                return NotFound();
            }

            db.Korisnici.Remove(korisnik);
            db.SaveChanges();

            return Ok(korisnik);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KorisnikExists(int id)
        {
            return db.Korisnici.Count(e => e.Id == id) > 0;
        }
    }
}