﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;

namespace ElectroExpert_API.Controllers
{
    public class JedinicaMjereController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/JedinicaMjere
        public IQueryable<JedinicaMjere> GetJedinicaMjere()
        {
            return db.JedinicaMjere;
        }

        // GET: api/JedinicaMjere/5
        [ResponseType(typeof(JedinicaMjere))]
        public IHttpActionResult GetJedinicaMjere(int id)
        {
            JedinicaMjere jedinicaMjere = db.JedinicaMjere.Find(id);
            if (jedinicaMjere == null)
            {
                return NotFound();
            }

            return Ok(jedinicaMjere);
        }

        // PUT: api/JedinicaMjere/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJedinicaMjere(int id, JedinicaMjere jedinicaMjere)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != jedinicaMjere.Id)
            {
                return BadRequest();
            }

            db.Entry(jedinicaMjere).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JedinicaMjereExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/JedinicaMjere
        [ResponseType(typeof(JedinicaMjere))]
        public IHttpActionResult PostJedinicaMjere(JedinicaMjere jedinicaMjere)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JedinicaMjere.Add(jedinicaMjere);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = jedinicaMjere.Id }, jedinicaMjere);
        }

        // DELETE: api/JedinicaMjere/5
        [ResponseType(typeof(JedinicaMjere))]
        public IHttpActionResult DeleteJedinicaMjere(int id)
        {
            JedinicaMjere jedinicaMjere = db.JedinicaMjere.Find(id);
            if (jedinicaMjere == null)
            {
                return NotFound();
            }

            db.JedinicaMjere.Remove(jedinicaMjere);
            db.SaveChanges();

            return Ok(jedinicaMjere);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JedinicaMjereExists(int id)
        {
            return db.JedinicaMjere.Count(e => e.Id == id) > 0;
        }
    }
}