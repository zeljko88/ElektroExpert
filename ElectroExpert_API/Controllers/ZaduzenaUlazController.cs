﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class ZaduzenaUlazController : ApiController
    {
        private DataContext db = new DataContext();


        private static readonly Expression<Func<ZaduzenaUlaz, ZaduzenaListaDTO>> AsZaduzenaListaDTO =
        x => new ZaduzenaListaDTO
        {
            ZaduzenaOpremaId = x.Id,
            DatumZaduzenja=x.Datum,
            Vozilo = x.Vozilo.Naziv
        };

        // GET: api/ZaduzenaUlaz
        public IQueryable<ZaduzenaListaDTO> GetZaduzenaUlaz()
        {
            return db.ZaduzenaUlaz.Include(x => x.Vozilo).Select(AsZaduzenaListaDTO);
        }



        [HttpGet]
        [Route("api/ZaduzenaUlaz/GetZaduzenaByVozilo/{id}")]
        public IQueryable<ZaduzenaListaDTO> GetZaduzenaByVozilo(int id)
        {
            return db.ZaduzenaUlaz.Where(x => x.VoziloId == id).Select(AsZaduzenaListaDTO);
        }




        // GET: api/ZaduzenaUlaz/5
        [ResponseType(typeof(ZaduzenaUlaz))]
        public IHttpActionResult GetZaduzenaUlaz(int id)
        {
            ZaduzenaUlaz zaduzenaUlaz = db.ZaduzenaUlaz.Find(id);
            if (zaduzenaUlaz == null)
            {
                return NotFound();
            }

            return Ok(zaduzenaUlaz);
        }

        // PUT: api/ZaduzenaUlaz/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutZaduzenaUlaz(int id, ZaduzenaUlaz zaduzenaUlaz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zaduzenaUlaz.Id)
            {
                return BadRequest();
            }

            db.Entry(zaduzenaUlaz).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZaduzenaUlazExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ZaduzenaUlaz
        [ResponseType(typeof(ZaduzenaUlaz))]
        public IHttpActionResult PostZaduzenaUlaz(ZaduzenaUlaz zaduzenaUlaz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            ZaduzenaUlaz u = new ZaduzenaUlaz();
            u.KorisnikId = zaduzenaUlaz.KorisnikId;
            u.VoziloId = zaduzenaUlaz.VoziloId;
            u.Datum = zaduzenaUlaz.Datum;
            u.SaVozila = zaduzenaUlaz.SaVozila;
            db.ZaduzenaUlaz.Add(u);
            db.SaveChanges();

            List<ZaduzenaUlazStavke> stavke = new List<ZaduzenaUlazStavke>();

            foreach (ZaduzenaUlazStavke item in zaduzenaUlaz.ZaduzenaUlazStavke)
            {

                var stavka = new ZaduzenaUlazStavke { ArtikalId = item.ArtikalId, Kolicina = item.Kolicina, ZaduzenaUlazId = u.Id };
                stavke.Add(stavka);

            }
            db.ZaduzenaUlazStavke.AddRange(stavke);
            db.SaveChanges();



            foreach (ZaduzenaUlazStavke item in zaduzenaUlaz.ZaduzenaUlazStavke)
            {
                foreach (ZaduzenaOprema ITEMM in db.ZaduzenaOprema.Where(x => x.ArtikalId == item.ArtikalId && x.VoziloId == zaduzenaUlaz.VoziloId))
                {

                    ITEMM.Kolicina += item.Kolicina;

                }
                db.SaveChanges();
            }





            List<ZaduzenaOprema> novaLista = new List<ZaduzenaOprema>();
                    
                foreach (ZaduzenaUlazStavke item in zaduzenaUlaz.ZaduzenaUlazStavke)
                {

                    int z = db.ZaduzenaOprema.Where(x => x.ArtikalId == item.ArtikalId && x.VoziloId == zaduzenaUlaz.VoziloId).Count();
                    if (z == 0)
                    {

                        ZaduzenaOprema novoStanje = new ZaduzenaOprema();
                        novoStanje.ArtikalId = item.ArtikalId;
                        novoStanje.VoziloId = Convert.ToInt32(zaduzenaUlaz.VoziloId);
                        novoStanje.Kolicina = item.Kolicina;
                        novaLista.Add(novoStanje);

                    }

                }
            

            if (novaLista != null)
            {
                db.ZaduzenaOprema.AddRange(novaLista);
                db.SaveChanges();
            }



       
           

            int brojac = 0;

            if (zaduzenaUlaz.SaVozila != 0)
            {                

                foreach (ZaduzenaUlazStavke item in zaduzenaUlaz.ZaduzenaUlazStavke)
                {
                    brojac = item.Kolicina;
                    foreach (ZaduzenaOprema ITEM in db.ZaduzenaOprema.Where(x => x.VoziloId == zaduzenaUlaz.SaVozila && x.ArtikalId==item.ArtikalId))
                    {

                        do
                        {
                            ITEM.Kolicina--;
                            brojac--;

                        } while (brojac > 0);


                    }
                    db.SaveChanges();
                }
            }

            else
            {

                foreach (ZaduzenaUlazStavke item in zaduzenaUlaz.ZaduzenaUlazStavke)
                {
                    brojac = item.Kolicina;
                    foreach (StanjeSkladista ITEM in db.StanjeSkladista.Where(x => x.Kolicina > 0 && x.ArtikalId == item.ArtikalId))
                    {

                        do
                        {
                            ITEM.Kolicina--;
                            brojac--;

                        } while (brojac > 0);


                    }
                }
                db.SaveChanges();

            }
            return CreatedAtRoute("DefaultApi", new { id = u.Id }, u);
        }

        // DELETE: api/ZaduzenaUlaz/5
        [ResponseType(typeof(ZaduzenaUlaz))]
        public IHttpActionResult DeleteZaduzenaUlaz(int id)
        {
            ZaduzenaUlaz zaduzenaUlaz = db.ZaduzenaUlaz.Find(id);
            if (zaduzenaUlaz == null)
            {
                return NotFound();
            }

            db.ZaduzenaUlaz.Remove(zaduzenaUlaz);
            db.SaveChanges();

            return Ok(zaduzenaUlaz);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ZaduzenaUlazExists(int id)
        {
            return db.ZaduzenaUlaz.Count(e => e.Id == id) > 0;
        }
    }
}