﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class ArtikalController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<Artikal, ArtikalDTO>> AsArtikalDTO =
       x => new ArtikalDTO
       {
           Id = x.Id,
           Naziv=x.Naziv,
           Sifra=x.Sifra,         
           
        
       };

        private static readonly Expression<Func<Artikal, ArtikalPrijemDTO>> AsArtikalPrijemDTO =
       x => new ArtikalPrijemDTO
       {
           Id = x.Id,
           Vrsta=x.Vrsta.Naziv,
           Naziv = x.Naziv,
           Sifra = x.Sifra,
           JedMjere=x.JedinicaMjere.Naziv
          
       };

        // GET: api/Artikal
        public IQueryable<ArtikalDTO> GetArtikli()
        {
            return db.Artikli.Select(AsArtikalDTO);
        }


        [HttpGet]
        [Route("api/Artikal/GetArtikliBaza")]
        public IQueryable<ArtikalPrijemDTO> GetArtikliBaza()
        {
            return db.Artikli.Select(AsArtikalPrijemDTO);
        }
        [HttpGet]
        [Route("api/Artikal/GetArtikalById/{id}")]
        public IHttpActionResult GetArtikalById(int id)
        {
            Artikal artikal = db.Artikli
                .Include(x => x.JedinicaMjere)
                .Include(x=>x.Vrsta)
                .Where(x => x.Id == id).SingleOrDefault();

            return Ok(artikal);
        }

        [HttpPost]
        [Route("api/Artikal/UrediArtikal")]
        public IHttpActionResult UrediArtikal(Artikal a)
        {
            Artikal artikal = db.Artikli
                .Include(x => x.JedinicaMjere)
                .Include(x => x.Vrsta)
                .Where(x => x.Id == a.Id).SingleOrDefault();

            artikal.JedinicaMjereId = a.JedinicaMjereId;
            artikal.Naziv = a.Naziv;
            artikal.Opis = a.Opis;
            artikal.Sifra = a.Sifra;
            artikal.VrstaId = a.VrstaId;

            db.SaveChanges();

            return Ok(artikal); ;
        }


        [HttpGet]
        [Route("api/Artikal/GetArtikalByVrsta/{id}")]
        //public IQueryable<ArtikalDTO> GetArtikalByVrsta(int id)
        //{
        //    return db.Artikli.Where(x=>x.VrstaId==id).Select(AsArtikalDTO);
        //}

        // GET: api/Artikal/5
        [ResponseType(typeof(Artikal))]
        public IHttpActionResult GetArtikal(int id)
        {
            Artikal artikal = db.Artikli.Where(x=>x.Id==id).SingleOrDefault();
            if (artikal == null)
            {
                return NotFound();
            }

            return Ok(artikal);
        }

        // PUT: api/Artikal/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutArtikal(int id, Artikal artikal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artikal.Id)
            {
                return BadRequest();
            }

            db.Entry(artikal).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtikalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Artikal
        [ResponseType(typeof(Artikal))]
        public IHttpActionResult PostArtikal(Artikal artikal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            //try
            //{

                db.Artikli.Add(artikal);
            db.SaveChanges();

            //}
            //catch (DbUpdateException ex)
            //{

            //    throw CreateHttpResponseException(Util.ExceptionHandler.HandleException(ex.InnerException), HttpStatusCode.Conflict);
            //}





            return CreatedAtRoute("DefaultApi", new { id = artikal.Id }, artikal);
        }

        private Exception CreateHttpResponseException(string reason, HttpStatusCode code)
        {
            var response = new HttpResponseMessage
            {
                StatusCode = code,
                ReasonPhrase = reason,
                Content = new StringContent(reason)
            };

            return new HttpResponseException(response);
        }



        // DELETE: api/Artikal/5
        [ResponseType(typeof(Artikal))]
        public IHttpActionResult DeleteArtikal(int id)
        {
            Artikal artikal = db.Artikli.Find(id);
            if (artikal == null)
            {
                return NotFound();
            }

            db.Artikli.Remove(artikal);
            db.SaveChanges();

            return Ok(artikal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikalExists(int id)
        {
            return db.Artikli.Count(e => e.Id == id) > 0;
        }
    }
}