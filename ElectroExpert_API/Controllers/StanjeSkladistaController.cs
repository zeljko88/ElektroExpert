﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class StanjeSkladistaController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<StanjeSkladista, StanjeSkladistaDTO>> AsStanjeSkladistaDTO =
      x => new StanjeSkladistaDTO
      {
          Id = x.Artikal.Id,
          Naziv = x.Artikal.Naziv,
          Sifra = x.Artikal.Sifra,
          Kolicina=x.Kolicina,          
          JedMjere=x.Artikal.JedinicaMjere.Naziv

      };

        // GET: api/StanjeSkladista
        public IQueryable<StanjeSkladistaDTO> GetStanjeSkladista()
        {
            return db.StanjeSkladista.Where(x=>x.Kolicina!=0).Select(AsStanjeSkladistaDTO);
        }

        [HttpGet]
        [Route("api/StanjeSkladista/GetArtikalByVrsta/{id}")]
        public IQueryable<StanjeSkladistaDTO> GetArtikalByVrsta(int id)
        {
            return db.StanjeSkladista.Where(x => x.Artikal.VrstaId == id).Select(AsStanjeSkladistaDTO);
        }

        [HttpGet]
        [Route("api/StanjeSkladista/GetStanjeArtikla/{id}")]
        public IQueryable<StanjeSkladistaDTO> GetStanjeArtikla(int id)
        {
            return db.StanjeSkladista.Where(x => x.ArtikalId == id).Select(AsStanjeSkladistaDTO);
        }

        [HttpGet]
        [Route("api/StanjeSkladista/UrediArtikal/{id}/{novostanje}")]
        public IHttpActionResult UrediArtikal(int id, int novostanje)
        {
            StanjeSkladista s = db.StanjeSkladista.Where(x => x.ArtikalId == id).SingleOrDefault();

            s.Kolicina = novostanje;

            db.SaveChanges();

            return Ok();
        }



        [HttpGet]
        [Route("api/StanjeSkladista/DeleteArtikal/{id}")]
        public IQueryable<StanjeSkladistaDTO> DeleteArtikal(int id)
        {

            StanjeSkladista s = db.StanjeSkladista.Where(x => x.ArtikalId == id).SingleOrDefault();

            db.StanjeSkladista.Remove(s);

            db.SaveChanges();

            return db.StanjeSkladista.Select(AsStanjeSkladistaDTO);
        }

        // GET: api/StanjeSkladista/5
        [ResponseType(typeof(StanjeSkladista))]
        public IHttpActionResult GetStanjeSkladista(int id)
        {
            StanjeSkladista stanjeSkladista = db.StanjeSkladista.Find(id);
            if (stanjeSkladista == null)
            {
                return NotFound();
            }

            return Ok(stanjeSkladista);
        }

        // PUT: api/StanjeSkladista/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStanjeSkladista(int id, StanjeSkladista stanjeSkladista)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != stanjeSkladista.Id)
            {
                return BadRequest();
            }

            db.Entry(stanjeSkladista).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StanjeSkladistaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/StanjeSkladista
        [ResponseType(typeof(StanjeSkladista))]
        public IHttpActionResult PostStanjeSkladista(StanjeSkladista stanjeSkladista)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.StanjeSkladista.Add(stanjeSkladista);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = stanjeSkladista.Id }, stanjeSkladista);
        }

        // DELETE: api/StanjeSkladista/5
        [ResponseType(typeof(StanjeSkladista))]
        public IHttpActionResult DeleteStanjeSkladista(int id)
        {
            StanjeSkladista stanjeSkladista = db.StanjeSkladista.Find(id);
            if (stanjeSkladista == null)
            {
                return NotFound();
            }

            db.StanjeSkladista.Remove(stanjeSkladista);
            db.SaveChanges();

            return Ok(stanjeSkladista);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StanjeSkladistaExists(int id)
        {
            return db.StanjeSkladista.Count(e => e.Id == id) > 0;
        }
    }
}