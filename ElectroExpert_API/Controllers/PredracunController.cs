﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using System.Linq.Expressions;
using ElectroExpert_API.DTO;

namespace ElectroExpert_API.Controllers
{
    public class PredracunController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<Predracun, PredracunDTO>> AsPredracunDTO =
     x => new PredracunDTO
     {
         Id = x.Id,
         Datum = x.DatumDo,
         IznosBezPdv = x.IznosBezPdv,
         BrojPredracuna=x.Broj,
         IznosSaPdv = x.IznosSaPdv,
         NazivKupca = x.Kupac.Ime,
         VrstaUsluge=x.VrstaUsluge.Naziv+"-"+x.VrstaUsluge.Sifra


     };




        // GET: api/Predracun
        public IQueryable<PredracunDTO> GetPredracun()
        {
            return db.Predracun.Select(AsPredracunDTO);
        }


        [HttpGet]
        [Route("api/Predracun/GetPredracunBroj/")]
        public IHttpActionResult GetPredracunBroj()
        {
            string broj = "";

            List<Predracun> pred = db.Predracun.ToList();

            if (pred.Count != 0)
            {

                 broj = db.Predracun.OrderByDescending(x => x.Broj).Select(x => x.Broj).First();

                int crtica = broj.IndexOf('/');
                string brSledeci = broj;

                brSledeci = broj.Substring(0, crtica);
                
                
                return Ok(brSledeci);
            }
            else
                return Ok(broj);
        }

        // GET: api/Predracun/5
        [ResponseType(typeof(Predracun))]
        public IHttpActionResult GetPredracun(int id)
        {
            Predracun predracun = db.Predracun.Find(id);
            if (predracun == null)
            {
                return NotFound();
            }

            return Ok(predracun);
        }

        // PUT: api/Predracun/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPredracun(int id, Predracun predracun)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != predracun.Id)
            {
                return BadRequest();
            }

            db.Entry(predracun).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PredracunExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Predracun
        [ResponseType(typeof(Predracun))]
        public IHttpActionResult PostPredracun(Predracun predracun)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

             Predracun p = new Predracun();



            p.Broj = predracun.Broj;
            p.DatumDo = predracun.DatumDo;
            p.VrstaUslugeId = predracun.VrstaUslugeId;
            p.DatumOd = predracun.DatumOd;
            p.Zakljucen = predracun.Zakljucen;
            p.IznosBezPdv = predracun.IznosBezPdv;
            p.IznosSaPdv = predracun.IznosSaPdv;
            p.Pdv = predracun.Pdv;
            p.KupacId = predracun.KupacId;
            db.Predracun.Add(p);
            db.SaveChanges();



            var stavke = new List<PredracunStavke>();
           



            foreach (PredracunStavke item in predracun.PredracunStavke)
            {

                var stavka = new PredracunStavke {

                    CijenaSaPdv = item.CijenaSaPdv,
                    CijenaBezPdv=item.CijenaBezPdv,
                    IznosBezPdv=item.IznosBezPdv,
                    IznosSaPdv=item.IznosSaPdv,
                    Kolicina=item.Kolicina,
                    PredracunId=p.Id,
                    JedinicaMjereId=item.JedinicaMjereId,
                    Pdv=item.Pdv,
                    UslugaId=item.UslugaId,
                                                         

                };
                stavke.Add(stavka);

            }
            db.PredracunStavke.AddRange(stavke);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = p.Id }, p);
        }

        // DELETE: api/Predracun/5
        [ResponseType(typeof(Predracun))]
        public IHttpActionResult DeletePredracun(int id)
        {
            Predracun predracun = db.Predracun.Find(id);
            if (predracun == null)
            {
                return NotFound();
            }

            db.Predracun.Remove(predracun);
            db.SaveChanges();

            return Ok(predracun);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PredracunExists(int id)
        {
            return db.Predracun.Count(e => e.Id == id) > 0;
        }

        [HttpGet]
        [Route("api/Predracun/ObrisiPredracun/{id}")]
        public IQueryable<PredracunDTO> ObrisiPredracun(int id)
        {

            Predracun p = db.Predracun.Where(x => x.Id == id).SingleOrDefault();

            List<PredracunStavke> ps = db.PredracunStavke.Where(x => x.PredracunId == id).ToList();

            db.PredracunStavke.RemoveRange(ps);


            db.Predracun.Remove(p);

            db.SaveChanges();

            return db.Predracun.Select(AsPredracunDTO);
        }
    }
}