﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class NalogController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<Nalog, NalogDTO>> AsNalogDTO =
    x => new NalogDTO
    {
        Id = x.Id,
        BrojNaloga = x.BrojNaloga,
        Vozilo = x.Vozilo.Naziv,        
        DatumOtvaranja = x.DatumOtvorenja.ToString(),
        DatumZavrsetka = x.DatumZavrsetka.ToString(),
        AdresaInstalacije = x.AdresaInstalacije,
        Opis=x.Opis,
        VoziloId=x.VoziloId,
        Pretplatnik=x.Pretplatnik,
        Status = x.Zavrsen == true ? "Završen" : "Otvoren",
    };

       
        private static readonly Expression<Func<Nalog, ZavrseniNaloziDTO>> AsZavrseniNaloziDTO =
  x => new ZavrseniNaloziDTO
  {
      NalogId = x.Id,
      BrojNaloga = x.BrojNaloga,
      Radnik = x.Vozilo.Naziv,  
      DatumZavrsetka = x.DatumZavrsetka.ToString(),
      AdresaInstalacije = x.AdresaInstalacije,      
      Pretplatnik = x.Pretplatnik,
      
  };



        public IQueryable<NalogDTO> GetNalogs()
        {
            return db.Nalogs.Select(AsNalogDTO);
        }


        [HttpGet]
        [Route("api/Nalog/IzmjeniStatus/{id}")]
        public IHttpActionResult IzmjeniStatus(int id)
        {
            Nalog nalog = db.Nalogs.Find(id);
            if (nalog == null)
            {
                return NotFound();
            }

            else

            nalog.Zavrsen = true;
            nalog.DatumZavrsetka = DateTime.Now;

            db.SaveChanges();
            return Ok(nalog);
        }


        [HttpGet]
        [Route("api/Nalog/GetZavrseniNalozi")]
        public IQueryable<ZavrseniNaloziDTO> GetZavrseniNalozi()
        {
            return db.Nalogs.Where(x => x.Zavrsen == true).Select(AsZavrseniNaloziDTO);
        }


        [HttpGet]
        [Route("api/Nalog/GetZavrseniNaloziByRadnik/{id}")]
        public IQueryable<ZavrseniNaloziDTO> GetZavrseniNaloziByRadnik(int id)
        {
            return db.Nalogs.Where(x => x.Zavrsen == true && x.VoziloId == id).Select(AsZavrseniNaloziDTO);
        }

        [HttpGet]
        [Route("api/Nalog/GetZavrseniNaloziByVoziloAndDatum/{id}/{datOd}/{datDo}")]
        public IQueryable<ZavrseniNaloziDTO> GetZavrseniNaloziByVoziloAndDatum(int id, DateTime datOd, DateTime datDo)
        {
            DateTime newDateDo = datDo.AddDays(1);
            return (
                       from @nalozi in db.Nalogs
                       where @nalozi.Zavrsen == true
                       where @nalozi.VoziloId == id
                       where @nalozi.DatumZavrsetka.Value >= datOd
                       where @nalozi.DatumZavrsetka.Value <= newDateDo


                       select new ZavrseniNaloziDTO
                       {
                           AdresaInstalacije= nalozi.AdresaInstalacije,
                           BrojNaloga=nalozi.BrojNaloga,
                           DatumZavrsetka=nalozi.DatumZavrsetka.ToString(),
                           NalogId=nalozi.Id,
                           Pretplatnik=nalozi.Pretplatnik,
                           Radnik=nalozi.Vozilo.Naziv
                         
                       });
        }



        // GET: api/Nalog/5
        [ResponseType(typeof(Nalog))]
        public IHttpActionResult GetNalog(int id)
        {
            Nalog nalog = db.Nalogs.Find(id);
            if (nalog == null)
            {
                return NotFound();
            }

            return Ok(nalog);
        }

        // PUT: api/Nalog/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNalog(int id, Nalog nalog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nalog.Id)
            {
                return BadRequest();
            }

            db.Entry(nalog).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NalogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Nalog
        [ResponseType(typeof(Nalog))]
        public IHttpActionResult PostNalog(Nalog nalog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Nalogs.Add(nalog);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nalog.Id }, nalog);
        }



        [HttpPost]
        [Route("api/Nalog/Updateuj")]
        public IHttpActionResult Updateuj(Nalog nalog)
        {
            Nalog n = db.Nalogs.Find(nalog.Id);
            n.Opis = nalog.Opis;
            n.Zavrsen = true;
            n.DatumZavrsetka = DateTime.Now;
           
            foreach(Usluga item in nalog.Usluge)
            {
                Usluga usluga = db.Usluge.FirstOrDefault(x=>x.Id == item.Id);


                db.Nalogs.Include("Usluge").FirstOrDefault(x => x.Id == nalog.Id).Usluge.Add(usluga);
                
            }
            
            db.SaveChanges();


            return Ok();
        }















        [HttpGet]
        [Route("api/Nalog/DeleteNalog/{id}")]
        // DELETE: api/Nalog/5
        [ResponseType(typeof(Nalog))]
        public IHttpActionResult DeleteNalog(int id)
        {
            Nalog nalog = db.Nalogs.Find(id);
            if (nalog == null)
            {
                return NotFound();
            }

            db.Nalogs.Remove(nalog);
            db.SaveChanges();

            return Ok(nalog);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NalogExists(int id)
        {
            return db.Nalogs.Count(e => e.Id == id) > 0;
        }
    }
}