﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class ZaduzenaOpremaStavkeController : ApiController
    {
        private DataContext db = new DataContext();



        //private static readonly Expression<Func<ZaduzenaOpremaStavke, ZaduzenaOpremaDTO>> AsZaduzenaOpremaDTO =
        //   x => new ZaduzenaOpremaDTO
        //   {
        //       ArtikalId = x.ArtikalId,
        //       NazivArtikla = x.Artikal.Naziv,
        //       KolicinaZaduzenog = x.KolicinaNaStanju,              
        //       JedMjere = x.Artikal.JedinicaMjere.Naziv,
        //       SifraArtikla = x.Artikal.Sifra              
               

        //   };

        [HttpGet]
        [Route("api/ZaduzenaOpremaStavke/GetZaduzenaOpremaByVozilo/{id}")]
        public IQueryable<ZaduzenaOpremaDTO> GetZaduzenaOpremaByVozilo(int id)
        {
            return (from zo in db.ZaduzenaOprema                    
                    join a in db.Artikli on zo.ArtikalId equals a.Id
                    where zo.Kolicina > 0 && zo.VoziloId == id
                    select new
                    {
                        artikal = a,
                        kolicina = zo.Kolicina,
                        naziv = a.Naziv,
                        sifra = a.Sifra,
                        jedMjere = a.JedinicaMjere.Naziv,

                    } into artikli

                    group artikli by artikli.artikal into art
                    let kol = art.Sum(prod => prod.kolicina)
                    select new ZaduzenaOpremaDTO
                    {
                        JedMjere = art.Key.JedinicaMjere.Naziv,
                        ArtikalId = art.Key.Id,
                        SifraArtikla = art.Key.Sifra,
                        NazivArtikla = art.Key.Naziv,
                        KolicinaZaduzenog = kol
                    });

        }



        // GET: api/ZaduzenaOpremaStavke
        //public IQueryable<ZaduzenaOpremaStavke> GetZaduzenaOpremaStavke()
        //{
        //    return db.ZaduzenaOpremaStavke;
        //}

        //// GET: api/ZaduzenaOpremaStavke/5
        //[ResponseType(typeof(ZaduzenaOpremaStavke))]
        //public IHttpActionResult GetZaduzenaOpremaStavke(int id)
        //{
        //    ZaduzenaOpremaStavke zaduzenaOpremaStavke = db.ZaduzenaOpremaStavke.Find(id);
        //    if (zaduzenaOpremaStavke == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(zaduzenaOpremaStavke);
        //}

        //// PUT: api/ZaduzenaOpremaStavke/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutZaduzenaOpremaStavke(int id, ZaduzenaOpremaStavke zaduzenaOpremaStavke)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != zaduzenaOpremaStavke.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(zaduzenaOpremaStavke).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ZaduzenaOpremaStavkeExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/ZaduzenaOpremaStavke
        //[ResponseType(typeof(ZaduzenaOpremaStavke))]
        //public IHttpActionResult PostZaduzenaOpremaStavke(ZaduzenaOpremaStavke zaduzenaOpremaStavke)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.ZaduzenaOpremaStavke.Add(zaduzenaOpremaStavke);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = zaduzenaOpremaStavke.Id }, zaduzenaOpremaStavke);
        //}

        //// DELETE: api/ZaduzenaOpremaStavke/5
        //[ResponseType(typeof(ZaduzenaOpremaStavke))]
        //public IHttpActionResult DeleteZaduzenaOpremaStavke(int id)
        //{
        //    ZaduzenaOpremaStavke zaduzenaOpremaStavke = db.ZaduzenaOpremaStavke.Find(id);
        //    if (zaduzenaOpremaStavke == null)
        //    {
        //        return NotFound();
        //    }

        //    db.ZaduzenaOpremaStavke.Remove(zaduzenaOpremaStavke);
        //    db.SaveChanges();

        //    return Ok(zaduzenaOpremaStavke);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool ZaduzenaOpremaStavkeExists(int id)
        //{
        //    return db.ZaduzenaOpremaStavke.Count(e => e.Id == id) > 0;
        //}
    }
}