﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class VrstaUslugeController : ApiController
    {
        private DataContext db = new DataContext();


        private static readonly Expression<Func<Usluga, UslugaDTO>> AsUslugaDTO =
      x => new UslugaDTO
      {

          Id = x.Id,
          Cijena = x.Cijena,
          SifraUsluge = x.Sifra,
          JedMjereId = x.JedinicaMjereId,
          VrstaUslugeId = x.VrstaUslugeId,
          NazivUsluge = x.Naziv,
          VrstaUsluge = x.VrstaUsluge.Naziv,
          JedMjere = x.JedinicaMjere.Naziv

      };


        // GET: api/VrstaUsluge
        public IQueryable<VrstaUsluge> GetVrstaUsluge()
        {
            return db.VrstaUsluge;
        }



        [HttpGet]
        [Route("api/VrstaUsluge/GetUslugeByDateAndVrsta/{idUsluge}/{datOd}/{datDo}")]
        public IQueryable<PredracunStavkeDTO> GetUslugeByDateAndVrsta(int idUsluge,DateTime datOd, DateTime datDo)
        {
            //DateTime od= Convert.ToDateTime(dtpOd);
            //DateTime doD= Convert.ToDateTime(dtpDo);
            DateTime newDateDo = datDo.AddDays(1);
            return (
                        from @nalozi in db.Nalogs
                        from usluga in @nalozi.Usluge
                        where usluga.VrstaUslugeId == idUsluge
                        where @nalozi.DatumZavrsetka.Value >= datOd
                        where @nalozi.DatumZavrsetka.Value <= newDateDo

                        select new
                        {
                            usluge = usluga,
                            kolicina = usluga.Id
                        }
                        into usluge

                        group usluge by usluge.usluge into usl
                        let kol = usl.Count()
                     
                    select new PredracunStavkeDTO
                    {

                        NazivUsluge = usl.Key.Naziv,
                        UslugaId = usl.Key.Id,
                        Kolicina = kol,
                        JedMjereId=usl.Key.JedinicaMjereId,
                        CijenaBezPdv=usl.Key.Cijena,
                        CijenaSaPdv= usl.Key.Cijena+(usl.Key.Cijena/100)*17,
                        IznosBezPdv=kol* usl.Key.Cijena,
                        
                        PdvIznos= kol * (usl.Key.Cijena / 100) * 17,
                        IznosSaPdv = kol*usl.Key.Cijena+((kol * usl.Key.Cijena / 100) * 17),
                        JedMjere=usl.Key.JedinicaMjere.Naziv,
                        Pdv=17
                    });
        }


        // GET: api/VrstaUsluge/5
        [ResponseType(typeof(VrstaUsluge))]
        public IHttpActionResult GetVrstaUsluge(int id)
        {
            VrstaUsluge vrstaUsluge = db.VrstaUsluge.Find(id);
            if (vrstaUsluge == null)
            {
                return NotFound();
            }

            return Ok(vrstaUsluge);
        }

        // PUT: api/VrstaUsluge/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVrstaUsluge(int id, VrstaUsluge vrstaUsluge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vrstaUsluge.Id)
            {
                return BadRequest();
            }

            db.Entry(vrstaUsluge).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VrstaUslugeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/VrstaUsluge
        [ResponseType(typeof(VrstaUsluge))]
        public IHttpActionResult PostVrstaUsluge(VrstaUsluge vrstaUsluge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VrstaUsluge.Add(vrstaUsluge);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vrstaUsluge.Id }, vrstaUsluge);
        }

        // DELETE: api/VrstaUsluge/5
        [ResponseType(typeof(VrstaUsluge))]
        public IHttpActionResult DeleteVrstaUsluge(int id)
        {
            VrstaUsluge vrstaUsluge = db.VrstaUsluge.Find(id);
            if (vrstaUsluge == null)
            {
                return NotFound();
            }

            db.VrstaUsluge.Remove(vrstaUsluge);
            db.SaveChanges();

            return Ok(vrstaUsluge);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VrstaUslugeExists(int id)
        {
            return db.VrstaUsluge.Count(e => e.Id == id) > 0;
        }
    }
}