﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using ElectroExpert_API.DTO;
using System.Linq.Expressions;

namespace ElectroExpert_API.Controllers
{
    public class UlazController : ApiController
    {
        private DataContext db = new DataContext();

        private static readonly Expression<Func<Ulaz, ListaUlazaDTO>> AsListaUlazaDTO =
x => new ListaUlazaDTO
{
 UlazId = x.Id,
 Datum = x.Datum, 

 
 Vozilo=x.Vozilo.Naziv


};


        // GET: api/Ulaz
        public IQueryable<ListaUlazaDTO> GetUlazi()
        {
            return db.Ulazi.Where(x=>x.VoziloId!=null).Select(AsListaUlazaDTO);
        }

        // GET: api/Ulaz/5
        [ResponseType(typeof(Ulaz))]
        public IHttpActionResult GetUlaz(int id)
        {
            Ulaz ulaz = db.Ulazi.Find(id);
            if (ulaz == null)
            {
                return NotFound();
            }

            return Ok(ulaz);
        }

        // PUT: api/Ulaz/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUlaz(int id, Ulaz ulaz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ulaz.Id)
            {
                return BadRequest();
            }

            db.Entry(ulaz).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UlazExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ulaz
        [ResponseType(typeof(Ulaz))]
        public IHttpActionResult PostUlaz(Ulaz ulaz)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Ulaz u = new Ulaz();

            u.KorisnikId = ulaz.KorisnikId;
            u.VoziloId = ulaz.VoziloId;            
            u.DobavljacId = ulaz.DobavljacId;
            if (string.IsNullOrEmpty(ulaz.BrojFakture))
            {
                u.BrojFakture = null;
            }
            else
            {
                u.BrojFakture = ulaz.BrojFakture;
            }
            u.Datum = ulaz.Datum;
             db.Ulazi.Add(u);
            db.SaveChanges();
            

            var stavke = new List<UlazStavke>();

            foreach (UlazStavke item in ulaz.UlazStavke)
            {

                var stavka = new UlazStavke { ArtikalId = item.ArtikalId, Kolicina = item.Kolicina, UlazId = u.Id };
                stavke.Add(stavka);

            }
            db.UlazStavke.AddRange(stavke);
            db.SaveChanges();



            if (ulaz.VoziloId != null)
            {


                int brojac = 0;

                foreach (UlazStavke item in ulaz.UlazStavke)
                {
                    brojac = item.Kolicina;
                    foreach (ZaduzenaOprema ITEM in db.ZaduzenaOprema.Where(x => x.ArtikalId==item.ArtikalId && x.VoziloId == ulaz.VoziloId))
                    {

                        do
                        {
                            ITEM.Kolicina--;
                            brojac--;

                        } while (brojac > 0);


                    }
                    db.SaveChanges();
                }
            }


                     

                StanjeSkladista stanje = new StanjeSkladista();

                List<StanjeSkladista> listastanja = db.StanjeSkladista.ToList();

                if (listastanja.Count == 0)
                {
                    foreach (UlazStavke ITEM in ulaz.UlazStavke)
                    {
                        StanjeSkladista novoStanje = new StanjeSkladista();
                        novoStanje.ArtikalId = ITEM.ArtikalId;
                        novoStanje.SkladisteId = 1;
                        novoStanje.Kolicina = ITEM.Kolicina;
                        db.StanjeSkladista.Add(novoStanje);
                        db.SaveChanges();
                    }
                }


                foreach (UlazStavke ITEM in ulaz.UlazStavke)
                {
                    int z = db.StanjeSkladista.Where(x => x.ArtikalId == ITEM.ArtikalId).Count();
                    if (z == 0)
                    {

                        StanjeSkladista novoStanje = new StanjeSkladista();
                        novoStanje.ArtikalId = ITEM.ArtikalId;
                        novoStanje.SkladisteId = 1;
                        novoStanje.Kolicina = ITEM.Kolicina;
                        db.StanjeSkladista.Add(novoStanje);
                        db.SaveChanges();
                    }

                }

                foreach (UlazStavke ITEM in ulaz.UlazStavke)
                {

                    foreach (StanjeSkladista item in listastanja.Where(x => x.ArtikalId == ITEM.ArtikalId))
                    {

                        item.Kolicina = item.Kolicina + ITEM.Kolicina;

                    }

               }

          


            
                db.SaveChanges();
            
                return CreatedAtRoute("DefaultApi", new { id = u.Id }, u);
            }
        

        // DELETE: api/Ulaz/5
        [ResponseType(typeof(Ulaz))]
        public IHttpActionResult DeleteUlaz(int id)
        {
            Ulaz ulaz = db.Ulazi.Find(id);
            if (ulaz == null)
            {
                return NotFound();
            }

            db.Ulazi.Remove(ulaz);
            db.SaveChanges();

            return Ok(ulaz);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UlazExists(int id)
        {
            return db.Ulazi.Count(e => e.Id == id) > 0;
        }
    }
}