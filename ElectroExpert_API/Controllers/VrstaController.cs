﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;

namespace ElectroExpert_API.Controllers
{
    public class VrstaController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Vrsta
        public IQueryable<Vrsta> GetVrste()
        {
            return db.Vrste;
        }

        // GET: api/Vrsta/5
        [ResponseType(typeof(Vrsta))]
        public IHttpActionResult GetVrsta(int id)
        {
            Vrsta vrsta = db.Vrste.Find(id);
            if (vrsta == null)
            {
                return NotFound();
            }

            return Ok(vrsta);
        }

        // PUT: api/Vrsta/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVrsta(int id, Vrsta vrsta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vrsta.Id)
            {
                return BadRequest();
            }

            db.Entry(vrsta).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VrstaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vrsta
        [ResponseType(typeof(Vrsta))]
        public IHttpActionResult PostVrsta(Vrsta vrsta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vrste.Add(vrsta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vrsta.Id }, vrsta);
        }

        // DELETE: api/Vrsta/5
        [ResponseType(typeof(Vrsta))]
        public IHttpActionResult DeleteVrsta(int id)
        {
            Vrsta vrsta = db.Vrste.Find(id);
            if (vrsta == null)
            {
                return NotFound();
            }

            db.Vrste.Remove(vrsta);
            db.SaveChanges();

            return Ok(vrsta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VrstaExists(int id)
        {
            return db.Vrste.Count(e => e.Id == id) > 0;
        }
    }
}