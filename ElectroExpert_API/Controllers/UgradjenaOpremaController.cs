﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ElectroExpert_API.DAL;
using ElectroExpert_API.Models;
using System.Linq.Expressions;
using ElectroExpert_API.DTO;

namespace ElectroExpert_API.Controllers
{
    public class UgradjenaOpremaController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/UgradjenaOprema
        public IQueryable<UgradjenaOprema> GetUgradjenaOprema()
        {
            return db.UgradjenaOprema;
        }

        private static readonly Expression<Func<UgradjenaOprema, UgradjenaOpremaDTO>> AsUgradjenaOpremaDTO =
       x => new UgradjenaOpremaDTO
       {
           
           DatumUgradnje = x.DatumUgradnje,
           
           //VoziloId = x.VoziloId,
           
           NalogId = x.NalogId,
          

       };


        [HttpGet]
        [Route("api/UgradjenaOprema/GetUgradjenaOpremaByNalog/{id}")]
        public IQueryable<UgradjenaOpremaDTO> GetUgradjenaOpremaByNalog(int id)
        {
            return (
                      from oprema in db.UgradjenaOprema join opStavke in db.UgradjenaOpremaStavke
                      on oprema.Id equals opStavke.UgradjenaOpremaId join artikal in db.Artikli
                      on opStavke.ArtikalId equals artikal.Id join jm in db.JedinicaMjere 
                      on artikal.JedinicaMjereId equals jm.Id
                      where oprema.NalogId == id
                       

                     

                      select new UgradjenaOpremaDTO
                      {

                          NazivArtikla=artikal.Naziv,
                          SifraArtikla=artikal.Sifra,
                          JedinicaMjere=jm.Naziv,
                          KolicinaUgradjenog=opStavke.Kolicina,
                          DatumUgradnje=oprema.DatumUgradnje

                      });


            
        }



        // GET: api/UgradjenaOprema/5
        [ResponseType(typeof(UgradjenaOprema))]
        public IHttpActionResult GetUgradjenaOprema(int id)
        {
            UgradjenaOprema ugradjenaOprema = db.UgradjenaOprema.Find(id);
            if (ugradjenaOprema == null)
            {
                return NotFound();
            }

            return Ok(ugradjenaOprema);
        }

        // PUT: api/UgradjenaOprema/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUgradjenaOprema(int id, UgradjenaOprema ugradjenaOprema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ugradjenaOprema.Id)
            {
                return BadRequest();
            }

            db.Entry(ugradjenaOprema).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UgradjenaOpremaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //POST: api/UgradjenaOprema
       [ResponseType(typeof(UgradjenaOprema))]
        public IHttpActionResult PostUgradjenaOprema(UgradjenaOprema ugradjenaOprema)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Nalog nalog = db.Nalogs.Find(ugradjenaOprema.NalogId);
                      

            db.UgradjenaOprema.Add(ugradjenaOprema);

            foreach (UgradjenaOpremaStavke item in ugradjenaOprema.UgradjenaOpremaStavke)
            {
                {
                    db.UgradjenaOpremaStavke.Add(item);
                }
            }

            List<ZaduzenaOprema> z = db.ZaduzenaOprema.Where(x => x.VoziloId == nalog.VoziloId && x.Kolicina>0).ToList();

                  


            foreach (UgradjenaOpremaStavke ITEM in ugradjenaOprema.UgradjenaOpremaStavke)
            {
                foreach (ZaduzenaOprema item in db.ZaduzenaOprema.Where(x => x.VoziloId == nalog.VoziloId))
                {
                    if (item.ArtikalId == ITEM.ArtikalId)
                    {
                        item.Kolicina -= ITEM.Kolicina;
                    }
                }
            }

            nalog.DatumZavrsetka = nalog.DatumZavrsetka;
            nalog.Zavrsen = true;
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = ugradjenaOprema.Id }, ugradjenaOprema);
        }

        // DELETE: api/UgradjenaOprema/5
        [ResponseType(typeof(UgradjenaOprema))]
        public IHttpActionResult DeleteUgradjenaOprema(int id)
        {
            UgradjenaOprema ugradjenaOprema = db.UgradjenaOprema.Find(id);
            if (ugradjenaOprema == null)
            {
                return NotFound();
            }

            db.UgradjenaOprema.Remove(ugradjenaOprema);
            db.SaveChanges();

            return Ok(ugradjenaOprema);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UgradjenaOpremaExists(int id)
        {
            return db.UgradjenaOprema.Count(e => e.Id == id) > 0;
        }
    }
}