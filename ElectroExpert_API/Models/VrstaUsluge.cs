﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class VrstaUsluge
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        public string Sifra { get;set; }
        public string Naziv { get; set; }
    }
}