﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Ulaz
    {
        public int Id { get; set; }        
        [Index(IsUnique = true)]
        public string BrojFakture { get; set; }
        public DateTime Datum { get; set; }
        public int DobavljacId { get; set; }
        public virtual Dobavljac Dobavljac { get; set; }
        public int KorisnikId { get; set; }
        public virtual Korisnik Korisnik { get; set; }
    
        public virtual List<UlazStavke> UlazStavke { get; set; }

        public int? VoziloId { get; set; }
        public virtual Vozilo Vozilo { get; set; }
    }
}