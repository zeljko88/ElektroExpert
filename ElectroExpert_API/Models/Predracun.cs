﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Predracun
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        public string Broj { get; set; }
        public  DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }

        public bool Zakljucen { get; set; }

        public decimal IznosBezPdv { get; set; }

        public decimal Pdv { get; set; }

        public decimal IznosSaPdv { get; set; }

        public int VrstaUslugeId { get; set; }

        public virtual VrstaUsluge VrstaUsluge { get; set; }



        public int KupacId { get; set; }

        public virtual Kupac Kupac { get; set; }
        public virtual List<PredracunStavke> PredracunStavke { get; set; }
    }
}