﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Artikal
    {
        public int Id { get; set; }
        [StringLength(30)]
        [Index(IsUnique = true)]
        public string Sifra { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
       

             
        public byte[] Slika { get; set; }
        public byte[] SlikaThumb { get; set; }
        public bool Dostupno { get; set; }

        public int VrstaId { get; set; }
        public virtual Vrsta Vrsta { get; set; }

        public int JedinicaMjereId { get; set; }
        public virtual JedinicaMjere JedinicaMjere { get; set; }



    }
}
 