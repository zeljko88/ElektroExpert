﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class UgradjenaOpremaStavke
    {
        public int Id { get; set; }

        public int ArtikalId { get; set; }
        public virtual Artikal Artikal { get; set; }

        public int Kolicina { get; set; }

        public int UgradjenaOpremaId { get; set; }

        public virtual UgradjenaOprema UgradjenaOprema { get; set; }
    }
}