﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class ZaduzenaOprema
    { 

        public int Id { get; set; }
       
        public int VoziloId { get; set; }
        public virtual Vozilo Vozilo { get; set; }

        public int ArtikalId { get; set; }
        public virtual Artikal Artikal { get; set; }

    


        public int Kolicina { get; set; }


    }
}