﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Vozilo
    {
        public int Id { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Naziv { get; set; }

        public DateTime DatumIstekaReg { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string RegOznaka { get; set; }

        public virtual ICollection<Radnik> Radnici { get; set; }
    }
}