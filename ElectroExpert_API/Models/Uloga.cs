﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Uloga
    {
        public Uloga()
        {
            this.KorisniciUloge = new HashSet<KorisniciUloge>();
        }
        public int Id { get; set; }
        public string Naziv { get; set; }


        public virtual ICollection<KorisniciUloge> KorisniciUloge { get; set; }
    }
}