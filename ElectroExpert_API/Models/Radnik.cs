﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Radnik
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        [StringLength(30)]
        [Index(IsUnique = true)]
        public string Email { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }

        public bool SefVozila { get; set; }
        public int VoziloId { get; set; }
        public bool Status { get; set; }

        public virtual Vozilo Vozilo { get; set; }
        public virtual ICollection<Nalog> Nalozi { get; set; }
    }
}