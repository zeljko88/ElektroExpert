﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Nalog
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        public int BrojNaloga { get; set; }

        public DateTime DatumOtvorenja { get; set; }
        public DateTime? DatumZavrsetka { get; set; }

        public string AdresaInstalacije { get; set; }
        public bool Zavrsen { get; set; }
        [StringLength(1000)]
        public string Opis { get; set; }
        public string Pretplatnik { get; set; }

        public int VoziloId { get; set; }
        public virtual Vozilo Vozilo { get; set; }

        public int KorisnikId { get; set; }

        public virtual Korisnik Korisnik { get; set; }

        public int? PredracunId { get; set; }

        public virtual Predracun Predracun { get; set; }

        public virtual ICollection<Usluga> Usluge { get; set; }
    }
}