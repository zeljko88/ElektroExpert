﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Usluga
    {
        public int Id { get; set; }
        [Index(IsUnique = true)]
        public int Sifra { get; set; }
        public string Naziv { get; set; }
        public double Cijena { get; set; }
        public int VrstaUslugeId { get; set; }
        public virtual VrstaUsluge VrstaUsluge { get; set; }
        public int JedinicaMjereId { get; set; }
        public virtual JedinicaMjere JedinicaMjere { get; set; }

        public virtual ICollection<Nalog> Nalozi { get; set; }
    }
}