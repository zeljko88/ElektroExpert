﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class ZaduzenaUlaz
    {
        public int Id { get; set; }

        public int VoziloId { get; set; }

        public virtual Vozilo Vozilo { get; set; }

        public DateTime Datum { get; set; }

        public int KorisnikId { get; set; }

        public virtual Korisnik Korisnik { get; set; }

        public virtual List<ZaduzenaUlazStavke> ZaduzenaUlazStavke { get; set; }
        public int SaVozila { get; set; }
    }
}