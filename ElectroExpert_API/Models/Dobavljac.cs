﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Dobavljac
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        
        public string Adresa { get; set; }

        public string BrojTelefona { get; set; }

        public string Email { get; set; }

        public int ZiroRacun { get; set; }
    }
}