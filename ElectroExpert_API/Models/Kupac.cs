﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Kupac
    {
        public int Id { get; set; }
        public string Ime { get; set; }

        public string Adresa { get; set; }

        public string Grad { get; set; }

        public float IdBroj { get; set; }
    }
}