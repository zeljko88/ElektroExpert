﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class StanjeSkladista
    {
        public int Id { get; set; }

        public int SkladisteId { get; set; }
        public virtual Skladiste Skladiste { get; set; }

        public int ArtikalId { get; set; }

        public virtual Artikal Artikal { get; set; }

        public int Kolicina { get; set; }
    }
}