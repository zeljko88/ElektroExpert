﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Vrsta
    {
        public int Id { get; set; }

        public string Naziv { get; set; }
    }
}