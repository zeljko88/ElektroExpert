﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class UgradjenaOprema
    {
        public int Id { get; set; }
        public DateTime DatumUgradnje { get; set; }        
             
        public int NalogId { get; set; }
        public virtual Nalog Nalog { get; set; }            

        public virtual List<UgradjenaOpremaStavke> UgradjenaOpremaStavke { get; set; }
    }
}