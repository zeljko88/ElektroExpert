﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class PredracunStavke
    {
        public int Id { get; set; }

        public int PredracunId { get; set; }

        public virtual Predracun Predracun { get; set; }

        public int UslugaId { get; set; }
        public virtual Usluga Usluga { get; set; }

        public int JedinicaMjereId { get; set; }
        public virtual JedinicaMjere JedinicaMjere { get; set; }

        public int Kolicina { get; set; }
        public double CijenaBezPdv { get; set; }
        public double CijenaSaPdv { get; set; }

        public double Pdv { get; set; }
        public double IznosBezPdv { get; set; }
        public double IznosSaPdv { get; set; }

    }
}