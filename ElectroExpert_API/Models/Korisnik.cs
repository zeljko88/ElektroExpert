﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class Korisnik
    {
        public Korisnik()
        {
                   
            this.KorisniciUloge = new HashSet<KorisniciUloge>();
        }
        public int Id { get; set; }
        [Required]
        public string Ime { get; set; }
        [Required]
        public string Prezime { get; set; }
        [Required]
        [StringLength(30)]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        public string Adresa { get; set; }
        public string Telefon { get; set; }
        [Required]
        [StringLength(30)]
        [Index(IsUnique = true)]
        public string KorisnickoIme { get; set; }

        public string Lozinka { get; set; }
        [Required]
        public string LozinkaHash { get; set; }
        [Required]
        public string LozinkaSalt { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<KorisniciUloge> KorisniciUloge { get; set; }


    }
}