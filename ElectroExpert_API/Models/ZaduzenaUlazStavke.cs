﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class ZaduzenaUlazStavke
    {
        public int Id { get; set; }
        

      

        public virtual ZaduzenaUlaz ZaduzenaUlaz { get; set; }
        public int ZaduzenaUlazId { get; set; }

        public int ArtikalId { get; set; }
        public virtual Artikal Artikal { get; set; }

        public int Kolicina { get; set; }
    }
}