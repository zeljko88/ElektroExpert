﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.Models
{
    public class UlazStavke
    {
        public int Id { get; set; }



        public int UlazId { get; set; }

        public virtual Ulaz Ulaz { get; set; }
        public int ArtikalId { get; set; }

        public virtual Artikal Artikal { get; set; }

        public int Kolicina { get; set; }
       
    }
}