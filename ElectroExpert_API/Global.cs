﻿using ElectroExpert_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace ElectroExpert
{
   public class Global
    {
        public static Korisnik prijavljeniKorisnik { get; set; }
        public static Ulaz aktivniUlaz { get; set; }

        public static string GetMessage(string key)
        {
            ResourceManager rm = new ResourceManager("ElectroExpert.Messages", Assembly.GetExecutingAssembly());

            return rm.GetString(key);
        }
    

}
}
