﻿using ElectroExpert_API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class NalogDTO
    {

        public int Id { get; set; }
        public int BrojNaloga { get; set; }
        public string Vozilo { get; set; }
        [StringLength(1000)]
        public string Opis { get; set; }
        public int VoziloId { get; set; }
        public string DatumOtvaranja { get; set; }

        public string DatumZavrsetka { get; set; }

        public string Pretplatnik { get; set; }

        public string Status { get; set; }
        public string AdresaInstalacije { get;  set; }

        public virtual ICollection<Usluga> Usluge { get; set; }
    }
}