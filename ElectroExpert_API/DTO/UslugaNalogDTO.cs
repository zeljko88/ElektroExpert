﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class UslugaNalogDTO
    {
      public string NazivUsluge { get; set; }

     public double  Cijena { get; set; }
        public string JedMjere { get; set; }
        public int Kolicina { get; set; }

        public int SifraUsluge { get; set; }

        public string VrstaUsluge { get; set; }
        
    }
}