﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ZaduzenaOpremaDTO
    {
        public int ArtikalId { get; set; }

        public string SifraArtikla { get; set; }
        public string NazivArtikla { get; set; }

        public int KolicinaZaduzenog { get; set; }

        public string JedMjere { get; set; }

    }
}