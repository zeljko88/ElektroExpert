﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class StanjeSkladistaDTO
    {
        public int Id { get; set; }

        public string Sifra { get; set; }
        public string Naziv { get; set; }
        
        public int Kolicina { get; set; }
        public string JedMjere { get;  set; }
        
    }
}