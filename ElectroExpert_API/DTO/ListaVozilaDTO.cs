﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ListaVozilaDTO
    {
        public int VoziloId { get; set; }

        public string NazivVozila { get; set; }

        public DateTime DatIstekaReg { get; set; }

        public string SefVozila { get; set; }
       
        public string RegOznaka { get; set; }
    }
}