﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class VoziloDTO
    {
        public int VoziloId { get; set; }

        public string NazivVozila { get; set; }                

        public string BrTelefona { get; set; }

        public string SefVozila { get; set; }

        public bool IsSef { get; set; }
        public string Email { get;  set; }
    }
}