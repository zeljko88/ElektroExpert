﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ZavrseniNaloziDTO
    {
        public int NalogId { get; set; }

        public int BrojNaloga { get; set; }

        public string Radnik { get; set; }

        public string DatumZavrsetka { get; set; }

        public string Pretplatnik { get; set; }

        public string AdresaInstalacije { get; set; }
    }
}