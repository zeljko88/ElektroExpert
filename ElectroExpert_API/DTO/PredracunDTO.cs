﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class PredracunDTO
    {
        public int Id { get; set; }
        public string VrstaUsluge { get; set; }

        public DateTime Datum { get; set; }

        public string BrojPredracuna { get; set; }

        public string NazivKupca { get; set; }

        public decimal IznosBezPdv { get; set; }

        public decimal IznosSaPdv { get; set; }
    }
}