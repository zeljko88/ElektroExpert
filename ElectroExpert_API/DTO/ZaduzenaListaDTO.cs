﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ZaduzenaListaDTO
    {
        public int ZaduzenaOpremaId { get; set; }

        public DateTime DatumZaduzenja { get; set; }

        public string Vozilo { get; set; }
    }
}