﻿using ElectroExpert_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class UlazStavkeDTO
    {
        public int ArtikalId { get; set; }
       
        public string Sifra { get; set; }
        public string Naziv { get; set; }        
        public int Kolicina { get; set; }

        public int JedMjereId { get; set; }
        public string JedMjere { get;  set; }
    }
}