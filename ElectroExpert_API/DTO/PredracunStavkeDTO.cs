﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class PredracunStavkeDTO
    {
        public string NazivUsluge { get; set; }
        public string JedMjere { get; set; }

        public int Kolicina { get; set; }

        public double CijenaBezPdv { get; set; }
        public double CijenaSaPdv { get; set; }
        
        public double PdvIznos { get; set; }
        public double Pdv { get; set; }
        public double IznosBezPdv { get; set; }
        public double IznosSaPdv { get; set; }
        public int JedMjereId { get;  set; }
        public int UslugaId { get;  set; }
    }
}