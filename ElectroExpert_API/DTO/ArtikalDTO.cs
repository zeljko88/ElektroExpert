﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ArtikalDTO
    {
        public int Id { get; set; }

        public string Sifra { get; set; }
        public string Naziv { get; set; }     
        public decimal Cijena { get; set; }
        public int Kolicina { get; set; }       
        
        
    }
}