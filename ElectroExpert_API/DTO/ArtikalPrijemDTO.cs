﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ArtikalPrijemDTO
    {
        public int Id { get; set; }

        public string Vrsta { get; set; }

        public string Sifra { get; set; }
       
        public string Naziv { get; set; }

        public string JedMjere { get; set; }

        public int Kolicina { get; set; }
    }
}