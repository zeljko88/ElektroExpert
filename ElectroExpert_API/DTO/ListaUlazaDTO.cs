﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class ListaUlazaDTO
    {
        public int UlazId { get;  set; }
        public DateTime Datum { get;  set; }
        public string Skladiste { get;  set; }
        public string Vozilo { get;  set; }
    }
}