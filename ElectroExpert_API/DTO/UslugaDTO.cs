﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class UslugaDTO
    {
        public int Id { get; set; }

        public string VrstaUsluge { get; set; }
        public int VrstaUslugeId { get; set; }
        public int SifraUsluge { get; set; }
        public string NazivUsluge { get; set; }
        public string JedMjere { get; set; }
        public int JedMjereId { get; set; }
        public double Cijena { get; set; }

    }
}