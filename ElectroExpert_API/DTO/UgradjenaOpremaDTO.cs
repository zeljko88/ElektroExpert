﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroExpert_API.DTO
{
    public class UgradjenaOpremaDTO
    {

        public int ArtikalId { get; set; }

        public string SifraArtikla { get; set; }
        public string NazivArtikla { get; set; }

        public int KolicinaUgradjenog { get; set; }

        public string JedinicaMjere { get; set; }
        public DateTime DatumUgradnje { get; set; }
        public int VoziloId { get; internal set; }
        public int NalogId { get; internal set; }
    }
}